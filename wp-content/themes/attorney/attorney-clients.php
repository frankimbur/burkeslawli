<?php
/**
 * Template Name: Attorney Clients
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); ?>
        <?php do_action('attorney_format_blog_banner', get_the_id()); ?>
<!--content Section Start Here -->
<div id="content" class="clients-page">
    <section class="clients-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php $PageTitle = get_post_meta(get_the_id(),'page_title', true); ?>
                    <?php if(!empty($PageTitle)): ?>
                    <h2><?php echo esc_html($PageTitle); ?></h2>
                    <?php endif; ?>
                    <?php $TitleDescription = get_post_meta(get_the_id(),'title_description', true); ?>
                    <?php if(!empty($TitleDescription)): ?>
                        <span class="heading-details"><?php echo esc_html($TitleDescription); ?></span>
                    <?php endif; ?>
                </div>
            </div> <!-- row ends here -->
            <div class="row">
            <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
                <?php $attorney_client = new WP_Query(array('post_type' => 'attorney_client', 'posts_per_page' => '18', 'paged' => $paged)); ?>
                  <?php if ($attorney_client->have_posts()): ?>
                    <ul class="clients-listing clearfix">
                        <?php while ($attorney_client->have_posts()): $attorney_client->the_post(); ?>
                            <?php if (has_post_thumbnail()) { ?>
                                <li class="col-xs-12 col-sm-3 col-md-2 animate-effect">
                                    <span><?php the_post_thumbnail(); ?></span>
                                </li>
                            <?php } ?>
                        <?php endwhile; ?>
                    </ul>
            <?php attorney_pagenavi($attorney_client);
            else :
                get_template_part('content/none');
            endif;
            ?>
           <?php wp_reset_postdata(); ?> 
            </div><!-- Row ends here -->
        </div>
    </section><!-- client contents ends here -->

    <section class="happy-clients">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php $clientTitle = get_post_meta(get_the_id(), 'clients_title', true); ?>
                    <?php if(!empty($clientTitle)): ?>
                    <h2><?php echo esc_html($clientTitle); ?></h2>
                    <?php endif; ?>
                    
                    <?php $clientsTitleDescripton = get_post_meta(get_the_id(), 'clients_title_descripton', true); ?>
                    <?php if(!empty($clientsTitleDescripton)): ?>
                    <span class="heading-details underline-label"><?php echo esc_html($clientsTitleDescripton); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <?php $the_query = new WP_Query( array('post_type' => 'testimonial', 'posts_per_page' => '2') ); ?>
                <?php if ($the_query->have_posts()): ?>
                    <?php while ($the_query->have_posts()): $the_query->the_post(); ?>
                         <div class="happy-client-list clearfix">
                        <?php if (has_post_thumbnail()): ?>
                            <figure class="col-xs-12 col-sm-3 col-md-3  animate-effect">
                                <?php the_post_thumbnail(); ?>
                            </figure>
                        <?php endif; ?>
                            <div class="col-xs-12 col-sm-9 col-md-9 happy-client-state animate-effect">
                                <p class="about-us-paragraph">
                                       <?php echo get_the_content(); ?>
                                </p>
                                <span class="taging-client">
                                    <span class="posting-by"><?php _e('By', 'attorney'); ?></span>
                                    <a href="#"><?php the_title(); ?></a>
                                </span>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
                <?php $testimonialsPageLink = get_post_meta(get_the_id(), 'testimonials_page_link', true); ?>
                <?php if(!empty($testimonialsPageLink)): ?>
                <div class="col-xs-12">
                    <a class="detail-submit view-btn animate-effect" href="<?php echo get_permalink($testimonialsPageLink); ?>"><?php _e('View all','attorney'); ?></a>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section><!-- happy clients ends here -->
</div>
 <?php endwhile; ?>
<?php endif; ?>
<?php
get_footer();
