<?php

/**
 * Attorney - sidebar
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */

if ( is_active_sidebar( 'blog-sidebar' ) ) {
    dynamic_sidebar("blog-sidebar");
} elseif (is_active_sidebar( 'attorney-default-sidebar' )) {
    dynamic_sidebar("attorney-default-sidebar");
}