/*!
 * Attorney 
 * Metabox control according page template
 * 
 *  @scince attorney 1.0 
 */

(function($){
    
    var AttorneyMetaboxControl={
       id: [],
       default: '',
       init: function(){
           this.setID();
           this.event();
       },
       event: function(){
        var self=this;
        
         self.reset();
         self.setShow($("#page_template").val());
        
        $(document).on("change", "#page_template", function(){
            self.reset();
            var $id=$(this).val();
            if($id){
                self.setShow($id);
            }
        });
       },
       setID: function(){
       this.default='#home_page_three_boxes, #home_page_two_boxes, #home_page_four_boxes, #history_page_boxes, #common_page_boxes, #page_boxes, #about_boxes, #client_boxes, #home_page_one_boxes, #mymetabox_revslider_0, #faq_page_boxes';
        this.id={
            'attorney-about-us.php' : '#common_page_boxes, #about_boxes',
            'attorney-clients.php' : '#common_page_boxes, #client_boxes',
            'attorney-faq.php'      : '#common_page_boxes, #faq_page_boxes',
            'attorney-practice-areas.php': '#common_page_boxes',
            'attornies-two.php' : '#common_page_boxes',
            'attornies-one.php': '#common_page_boxes',
            'attorney-testimonials.php' : '#common_page_boxes',
            'attorney-history.php'  : '#history_page_boxes, #common_page_boxes',
            'attorney-contact.php'  : '#common_page_boxes, #page_boxes',
            'attorney-home-one.php' : '#home_page_one_boxes',
            'attorney-home-two.php'     : '#home_page_two_boxes',
            'attorney-home-three.php'   : '#home_page_three_boxes',
            'attorney-home-four.php': '#home_page_four_boxes',
        };
       },
       reset: function(){
            var self=this;
            $(self.default).hide();
       },
       setShow: function($id){
            var selection=this.id[$id];
            $(selection).show();
       }        
    };
    
    if($("#page_template").length> 0){
        AttorneyMetaboxControl.init();
    }
    
})(jQuery);
