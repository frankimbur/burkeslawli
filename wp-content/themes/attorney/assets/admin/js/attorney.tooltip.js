/* 
 * AirDev Tooltip
 * @package: airdev
 * @subpackage: airdev/assets/admin/
 * @version: airdev 1.1
 */
(function($) {
    var AirdevTooltip={
        init: function(){
                this.events();
        },
        events: function(){
            var self=this;
            $(".attorney-hint").bind({
                mousemove : function(e){ self.changeTooltipPosition(e, this); },
                mouseenter : function(e){ self.showTooltip(e, this); },
                mouseleave: function(e){ self.hideTooltip(e, this); }
            });
        },
        changeTooltipPosition: function(event , $this) {
            var tooltipX = event.pageX - 8;
            var tooltipY = event.pageY + 8;
            
            $('div.attorney-tooltip').css({
                top: tooltipY, 
                left: tooltipX
            });
        },
        showTooltip: function(event, $this) {
            
            $('div.attorney-tooltip').remove();
           // alert(event.toSource());
           //console.log($(event).data("title"));
            $('<div class="attorney-tooltip">'+ $($this).data("title")+'</div>').appendTo('body');
            
            this.changeTooltipPosition(event, $this);
        },
            
 
 
        hideTooltip: function() {
            $('div.attorney-tooltip').remove();
        }
    };
    
    
   //$("#setting_attorney_4header img").addClass("airdev-hint");
    
    if($(".attorney-hint").length> 0){
        AirdevTooltip.init();
    }
    

    
        
})(jQuery);

