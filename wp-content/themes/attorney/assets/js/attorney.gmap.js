/*! 
 * Attorney - GMap
 * @version 1.0.0
 */
(function($) {
    //Custom Map
    if ($('#map').length) {
        var map = new GMaps({
            div: '#map',
            lat: ATTORNEY_GMAP.latitude,
            lng: ATTORNEY_GMAP.longitude,
            disableDefaultUI: true,
            zoom: 17,
            scrollwheel: false
        });
        map.drawOverlay({
            lat: map.getCenter().lat(),
            lng: map.getCenter().lng(),
            content: '<a href="#" class="fonttag mapmarker">marker<i class="fa fa-map-marker"></i></a>',
            verticalAlign: 'top',
            horizontalAlign: 'center'
        });

        /*if ($(window).width() >= 1200) {
            map.setOptions({
                styles: Site.styles,
                center: new google.maps.LatLng(41.401836, -74.329801),
            });
        } else if ($(window).width() >= 992) {
            map.setOptions({
                styles: Site.styles,
                center: new google.maps.LatLng(41.401836, -74.331801),
            });
        } else if ($(window).width() >= 768) {
            map.setOptions({
                styles: Site.styles,
                center: new google.maps.LatLng(41.401836, -74.329801),
            });
        } else {
            map.setOptions({
                styles: Site.styles,
                center: new google.maps.LatLng(41.400136, -74.332562),
            });
        }*/
    }

})(jQuery);

