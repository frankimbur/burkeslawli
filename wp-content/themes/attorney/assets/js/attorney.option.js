/*Attorney - Theme Option
 * 
 *@version 1.0.0  
 */
jQuery(function($) {
    var AttorneySwitcher = {
        init: function() {
            this.loadSkin();
            this.loadLess();
            this.stickyHeader();
            this.layoutStyle();
            this.transitionlayerfun()
        },
        loadSkin: function() {
            $("head").append($('<link rel="stylesheet/less">').attr("href", ATTORNEY_OPTION.themeURL + "/assets/css/skin.less"));
        },
        loadLess: function() {
            var self = this;
            $.getScript(ATTORNEY_OPTION.themeURL + "/assets/js/less.js", function(data, textStatus, jqxhr) {
                self.setLessValue();
            });
        },
        setLessValue: function() {
            less.modifyVars({
                skinColor: ATTORNEY_OPTION.color,
                fontFamily: ATTORNEY_OPTION.global_font
            });
        },
        stickyHeader: function() {
            var self = this;
            var sticky = ATTORNEY_OPTION.sticky_header;
            var menuType = ATTORNEY_OPTION.menu_type;
            self.fixedNav(sticky, menuType);
            self.navPosition(menuType);
        },
        layoutStyle: function() {
            var layout = ATTORNEY_OPTION.layout;
            if (layout == 'full-width') {
                $('#wrapper').removeClass('boxed')
                $('#layout .layout-column').removeClass('radio-active')
                $('#full-width').addClass('radio-active')
            } else {
                $('#wrapper').addClass('boxed')
                $('#layout .layout-column').removeClass('radio-active')
                $('#boxed').addClass('radio-active')
            }
        },
        fixedNav: function($sticky, $navPosition) {
            // Fixed Header JS
            var headerHeight = $('#header').height()
            var st = $(window).scrollTop();
            var fixedNav = function() {
                if ($sticky == "intelligent") {
                    $('#header').removeClass('normal');
                    $('#slider').addClass('top');
                    $('#header').addClass('intelligent');
                    var pos = $(window).scrollTop();
                    if (pos > headerHeight) {
                        if (pos > st) {
                            $('#header').addClass('simple')
                            $('#header.simple').removeClass('down')
                            $('#header.simple').addClass('fixed up')
                        } else {
                            $('#header.simple').removeClass('up')
                            $('#header.simple').addClass('fixed down')
                        }
                        st = pos;
                    } else {
                        $('#header.simple').removeClass('fixed down up simple')
                    }
                    if (pos == $(document).height() - $(window).height()) {
                        $('#header.simple').removeClass('up')
                        $('#header.simple').addClass('fixed down')
                    }
                }
                else if ($sticky == "fixed") {
                    $('#slider').addClass('top')
                    $('#header').addClass('simple fixed')
                    $('#header').removeClass('down up')
                    $('#header').removeClass('intelligent');
                    $('#wrapper').css({
                        paddingTop: 0
                    })
                } else {
                    $('#header.simple').removeClass('fixed down up simple')
                    $('#header').addClass('normal');
                    $('#slider').removeClass('top');
                    $('#header').removeClass('intelligent');
                    $('#wrapper').css({
                        paddingTop: 0
                    })
                }
            }
            if ($('#header').length) {
                fixedNav();
            }
            $(window).scroll(function() {
                if ($('#header').length) {
                    fixedNav();
                }
            });
        },
        navPosition: function(navPosition) {
            if (navPosition == "top") {
                $('#header').find('.navbar').addClass('navbar-fixed-top')
                $('#header-position .layout-column').removeClass('radio-active')
                $('#header-top').addClass('radio-active')
            } else if (navPosition == "bottom") {
                $('#header').find('.navbar').removeClass('navbar-fixed-top')
                $('#header-position .layout-column').removeClass('radio-active')
                $('#header-bottom').addClass('radio-active')
            } else {
                $('#header').find('.navbar').removeClass('navbar-fixed-top')
                $('#header-position .layout-column').removeClass('radio-active')
                $('#header-default').addClass('radio-active')
            }
        },
        transitionlayerfun: function(transitionVal) {
            var themeEffect = ATTORNEY_OPTION.effect
            if (themeEffect == 'transition') {
                $('html,body').scrollTop(0)
                $('#theme_effect .layout-column').removeClass('radio-active');
                $('#theme-transition.layout-column').addClass('radio-active');
                $('.animate-effect').each(function() {
                    $(this).addClass('anim-section');
                });
            } else {
                $('#theme_effect .layout-column').removeClass('radio-active');
                $('.animate-effect').removeClass('anim-section');
                $('#theme-translate.layout-column').addClass('radio-active');
            }
        },
    };
    AttorneySwitcher.init();
})
