<?php
/**
 * Attorney - archive page
 * The template for displaying archive pages
 * 
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<!--content Section Start Here -->
<div id="content">
    <!--practice-listings-container Section Start Here -->
    <section class="practice-listings-container">
        <div class="container">
            <div class="practice-area-lists">
                <div class="row">
                <?php if (have_posts()): ?>
                    <?php while (have_posts()): the_post(); ?>
                    <div class="col-xs-12 col-sm-4 area-list-collection zoom">
                        <?php if (has_post_thumbnail()) { ?>
                                <figure><?php the_post_thumbnail(); ?> </figure>
                            <?php } ?>
                        <h3><?php the_title(); ?></h3>
                                <?php the_content(); ?>
                        <a class="more-btn more" href="<?php the_permalink(); ?>"><?php _e('Read more', 'attorney'); ?></a>
                    </div>
                    <?php endwhile;  
                    attorney_pagenavi();
            else :
                get_template_part('content/none');
            endif;
            wp_reset_postdata(); ?>
                </div>
            </div>

        </div>

    </section>

    <!--practice-detail-container Section End Here -->

</div>
<!--content Section ends Here -->
<?php
get_footer();
