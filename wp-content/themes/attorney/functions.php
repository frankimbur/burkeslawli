<?php

/**
 * Attorney - Core files
 *
 * @package AttorneyTheme
 * @since attorney 1.0 
 */
define('ATTORNEY_THEME_DIR', get_template_directory());
define('ATTORNEY_THEME_URL', get_template_directory_uri());

//setup
attorney_inclusion( "inc/add-action-setup.php" );
attorney_inclusion( "inc/add-action-layout.php" );

//functions
attorney_inclusion( "inc/blog-functions.php" );
attorney_inclusion( "inc/comment-list-callback.php" );
attorney_inclusion( "inc/comment-extra-fields.php" );

//lib
attorney_inclusion( "inc/lib/pagination.php" );

//widgets
attorney_inclusion( "inc/widgets/register-sidebars.php" );
//metabox
attorney_inclusion( "inc/metabox/metabox.php" );

//admin menu
attorney_inclusion( "inc/menu/menu.php" );

//user 
attorney_inclusion( "inc/user/user.php" );

//Helper Function
attorney_inclusion( "inc/helper-functions.php" );

//Add widget
attorney_inclusion( "inc/add-action-widget.php" );

//Image Resize
attorney_inclusion( "inc/lib/image-resize.php" );

attorney_inclusion( "vendor/vendor.php" );

//visual composer
if (defined('WPB_VC_VERSION')) {
attorney_inclusion( "vc_templates/vc-map/vc-map.php" );
}
/**
 * Attorney - file inclusion
 *
 * @param string $path
 * @param boolean $include_once
 * @return void
 *
 * @since attorney 1.0.0
 */
function attorney_inclusion($path, $require = true, $once = true, $return = false) {
    //default: require_once
    if ($once) {
        if ($require) {
            require_once( trailingslashit(get_template_directory()) . $path );
        } else {
            include_once( trailingslashit(get_template_directory()) . $path );
        }
    } else {
        if ($require) {
            require( trailingslashit(get_template_directory()) . $path );
        } else {
            if ($return) {
                return( include trailingslashit(get_template_directory()) . $path );
            } else {
                include( trailingslashit(get_template_directory()) . $path );
            }
        }
    }
}
