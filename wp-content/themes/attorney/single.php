<?php
/**
 * Attorney - single
 * The template for displaying all single posts.
 * 
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<section class="banner-one" id="slider">
    <?php $url = ot_get_option('blog_detail_banner_image'); ?>
    <?php if(!empty($url)): ?>
    <div class="about-banner" style="background: url(<?php echo esc_url($url); ?>);">
                <div class="container banner-text">
                        <div class="row">
                                <div class="col-xs-12">
                                        <h1><?php the_title(); ?></h1>
                                </div>
                        </div>
                </div>

        </div>
    <?php endif; ?>
</section>
<!--content Section Start Here -->
<div id="content">
    <!--blog-content Section Start Here -->
    <section class="blog-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-9">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) : the_post();
                        attorney_wpb_set_post_views(get_the_id());
                           get_template_part('content/format/standard'); 
                            // If comments are open or we have at least one comment, load up the comment template.
                            if (comments_open() || get_comments_number()) :
                                comments_template();
                            endif;
                        endwhile;
                    else :
                        get_template_part('content/none');
                    endif;
                    ?>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-3 blog-page blog-sidebar">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
    <!--blog-content Section End Here -->
</div>
<!--content Section End Here -->
<?php
get_footer();
