<?php
/**
 * Attorney - page
 * The template for displaying all pages.
 * 
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<!--content Section Start Here -->
<div id="content">
    <!--blog-content Section Start Here -->
    <section class="blog-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-9">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) : the_post();
                            ?>
                            <div id="post-<?php the_ID(); ?>" <?php post_class('blog-listing blog-details-page clearfix'); ?>>
                                <?php if (has_post_thumbnail() && !post_password_required() && !is_attachment()) : ?>
                                    <div class="blog-listing-pics">
                                        <figure>
                                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                                        </figure>
                                    </div>
                                <?php endif; ?>
                                <div class="blog-information">
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <?php
                                    if(function_exists('is_buddypress')){
                                        if(is_buddypress() == 'true'){
                                            the_content();
                                        }
                                        else{
                                            echo do_shortcode(get_the_content());                                
                                        }
                                    }
                                    else{
                                        echo do_shortcode(get_the_content());                                
                                    }

                                    $flagLinkPage = false;

                                    if ($flagLinkPage):
                                        wp_link_pages(array(
                                            'before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'attorney') . '</span>',
                                            'after' => '</div>',
                                            'link_before' => '<span>',
                                            'link_after' => '</span>',
                                            'pagelink' => '<span class="screen-reader-text">' . __('Page', 'attorney') . ' </span>%',
                                            'separator' => '<span class="screen-reader-text">, </span>',
                                        ));
                                    endif;
                                    ?>
                                </div>
                            </div> <!-- post ends here -->
                            <?php
                            // If comments are open or we have at least one comment, load up the comment template.
                            if (comments_open() || get_comments_number()) :
                                comments_template();
                            endif;
                        endwhile;
                    else :
                        get_template_part('content/none');
                    endif;
                    ?>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-3 blog-page blog-sidebar">
                    <?php get_sidebar(); ?>
                </div>
            </div> <!-- row ends here -->
        </div> <!-- container ends here -->
    </section>
    <!--blog-content Section End Here -->
</div>
<!--content Section End Here -->
<?php
get_footer();
