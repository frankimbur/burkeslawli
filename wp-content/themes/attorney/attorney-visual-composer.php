<?php
/**
 * Template Name: Attorney Visual Composer Page
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
if (have_posts()):
    while (have_posts()): the_post();
        the_content();
    endwhile;
endif;
get_footer();
        