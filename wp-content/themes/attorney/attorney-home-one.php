<?php
/**
 * Template Name: Attorney Home One
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
if (have_posts()):
    while (have_posts()): the_post();
        ?>

        <!-- Banner Slider Starts -->
        <?php get_template_part('content/home/home', 'one-slider'); ?>
        <!-- Banner Slider Ends -->
        <div id="content">
            <!--practice-area Section Start Here -->
            <?php get_template_part('content/home/home', 'one-practice'); ?>
            <!--practice-area Section End Here -->
            <!--about-us Section Start Here -->
            <?php get_template_part('content/home/home', 'one-about-us'); ?>
            <!--about-us Section End  Here -->
            <!--news-testimonial Section Start Here -->
            <?php get_template_part('content/home/home', 'one-news-testimonial'); ?>
            <!--news-testimonial Section End  Here -->
            <!--client Section start  Here -->
            <?php get_template_part('content/home/home', 'one-client'); ?>
            <!--client Section End  Here -->
            <?php get_template_part('content/home/home', 'one-hiring'); ?>
            <!--client Section End  Here -->
        </div>
        <?php
    endwhile;
endif;
get_footer();
