<?php
/**
 * Attorney - Clients Section Shortcode
 * 
 * This template is use by js composer plugin 
 * 
 * @package attorney
 * @subpackage attorney.vc_templates
 * @since attorney 1.0.0
 */
extract(shortcode_atts(array(
    'main_title' => '',
    'main_title_description' => '',
                ), $atts));
?>
<div class="page-template-attorney-home-two page-template-attorney-home-two-php">
    <section class="client animate-effect">
        <div class="container">
            <div class="row">
                <header class="col-xs-12 section-header">
                    <a href="javascript:void(0);" class="icon-civil"> <i class="civilean-law-svg"> <img src="<?php echo (ot_get_option('civil_litigation_image')) ? ot_get_option('civil_litigation_image') : get_template_directory_uri() . '/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i> </a>
                    <?php $ClientSectionTitle = $main_title; ?>
                    <?php $ClientsSectionTitleDescription = $main_title_description; ?>
                    <?php if (!empty($ClientSectionTitle)): ?>
                        <h2><?php echo esc_html($ClientSectionTitle); ?></h2>
                    <?php endif; ?>
                    <?php if (!empty($ClientsSectionTitleDescription)): ?>
                        <span class="about-tag"><?php echo esc_html($ClientsSectionTitleDescription); ?></span>
                    <?php endif; ?>
                </header>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php $attorney_client = new WP_Query(array('post_type' => 'attorney_client', 'posts_per_page' => '6')); ?>
                    <?php if ($attorney_client->have_posts()): ?>
                        <ul class="client-listing clearfix">
                            <?php while ($attorney_client->have_posts()): $attorney_client->the_post(); ?>
                                <?php if (has_post_thumbnail()) { ?>
                                    <li><?php the_post_thumbnail(); ?></li>
                                <?php } ?>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                    <?php wp_reset_postdata(); ?>
                </div>

            </div>

        </div>

    </section>
</div>
<?php
echo $this->endBlockComment('attorney_clients_section_shortcode');
