<?php
/**
 * Attorney - About us
 * 
 * This template is use by js composer plugin 
 * 
 * @package attorney
 * @subpackage attorney.vc_templates
 * @since attorney 1.0.0
 */
extract(shortcode_atts(array(
    'main_title' => '',
    'main_title_description' => '',
    'read_more_link' => '',
    'section_title' => '',
    'section_description' => '',
    'phone_no_title' => '',
    'phone_no' => '',
    'email_title' => '',
    'email' => '',
                ), $atts));
?>
<div class="page-template-attorney-home-two page-template-attorney-home-two-php">
    <section class="about-us animate-effect">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 about-us-detail ">
                    <a href="#" class="icon-civil"> <i class="civilean-law-svg"> <img src="<?php echo (ot_get_option('civil_litigation_image')) ? ot_get_option('civil_litigation_image') : get_template_directory_uri() . '/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i> </a>
                    <?php $aboutTitle = $main_title; ?>
                    <?php if (!empty($aboutTitle)): ?>
                        <h2><?php echo esc_html($aboutTitle); ?></h2>
                    <?php endif; ?>

                    <?php $AboutTitleDescription = $main_title_description; ?>
                    <?php if (!empty($AboutTitleDescription)): ?>
                        <span class="about-tag"><?php echo esc_html($AboutTitleDescription); ?></span>
                    <?php endif; ?>

                    <a href="<?php echo $read_more_link; ?>" class="more-btn"><?php _e('READ MORE', 'attorney'); ?></a>

                </div>

                <div class="col-xs-12 col-sm-9 col-md-9 ">
                    <div class="about-us-details">
                        <?php $AboutContentTitle = $section_title; ?>
                        <?php if (!empty($AboutContentTitle)): ?>
                            <strong class="about-us-heading"><?php echo esc_html($AboutContentTitle); ?></strong>
                        <?php endif; ?>

                        <?php $AboutContentDescription = $section_description; ?>
                        <?php if (!empty($AboutContentDescription)): ?>
                            <p class="content-description"><?php echo esc_html($AboutContentDescription); ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="contact-details clearfix">
                        <div class="phone-detail clearfix">
                            <?php $AboutContactNoTitle = $phone_no_title; ?>
                            <?php $AboutContactNo = $phone_no; ?>
                            <?php if (!empty($AboutContactNo)): ?>
                                <a href="javascript: void(0);" class="fa fa-mobile"> &nbsp;</a>
                                <div class="phone-detail-inner">
                                    <span><?php echo esc_html($AboutContactNoTitle); ?></span>
                                    <a href="callto:<?php echo esc_attr($AboutContactNo); ?>" class="phone-number"><?php echo esc_html($AboutContactNo); ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="mail-detail">
                            <?php $AboutEmailTile = $email_title; ?>
                            <?php $AaboutEmail = $email; ?>
                            <?php if (!empty($AboutContactNo)): ?>
                                <a href="javascript: void(0);" class="fa fa-envelope-o"> &nbsp;</a>
                                <div class="phone-detail-inner">
                                    <span><?php echo esc_html($AboutEmailTile); ?></span>
                                    <a href="mailto:<?php echo esc_attr($AaboutEmail); ?>" class="phone-number"><?php echo esc_html($AaboutEmail); ?></a>
                                </div>
                            <?php endif; ?>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
<?php
echo $this->endBlockComment('attorney_about_us_shortcode');
