<?php

/**
 * Attorney - Testimonials Slider
 * 
 * This template is use by js composer plugin 
 * 
 * @package attorney
 * @subpackage attorney.vc_templates.vc-map
 * @since attorney 1.0.0
 */
add_action('vc_before_init', 'attorney_testimonials_slider');

function attorney_testimonials_slider() {
    vc_map(array(
        "name" => esc_html__("Attorney Testimonials Slider", 'attorney'),
        "base" => "attorney_testimonials_slider_shortcode",
        "class" => "",
        "icon" => "icon-wpb-vc_extend",
        "category" => esc_html__('Attorney', 'attorney'),
        "description" => esc_html__("Testimonials Slider", "attorney"),
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "",
                "class" => "",
                "heading" => esc_html__("Enter Main Title", 'attorney'),
                "param_name" => "main_title",
                "value" => '',
                "description" => esc_html__("Section main title.", 'attorney')
            ),
            array(
                "type" => "textfield",
                "holder" => "",
                "class" => "",
                "heading" => esc_html__("Enter Main Title Description", 'attorney'),
                "param_name" => "main_title_description",
                "value" => '',
                "description" => esc_html__("Section main title description.", 'attorney')
            ),
            array(
                "type" => "dropdown",
                "holder" => "",
                "class" => "",
                "heading" => esc_html__("Select No. of Testimonials", 'attorney'),
                "param_name" => "no_of_testimonials",
                "value" => array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, -1),
                "description" => esc_html__("Select No. of Testimonials to display in Slider.", 'attorney')
            ),
        )
    ));
}

class WPBakeryShortCode_attorney_testimonials_slider_shortcode extends WPBakeryShortCode {
    
}
