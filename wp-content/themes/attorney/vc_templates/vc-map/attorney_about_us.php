<?php
/**
 * Attorney - About us
 * 
 * This template is use by js composer plugin 
 * 
 * @package attorney
 * @subpackage attorney.vc_templates.vc-map
 * @since attorney 1.0.0
 */
add_action('vc_before_init', 'attorney_about_us');

function attorney_about_us() {
    vc_map(array(
    "name" => esc_html__("Attorney About Us", 'attorney'),
    "base" => "attorney_about_us_shortcode",
    "class" => "",
    "icon" => "icon-wpb-vc_extend",
    "category" => esc_html__('Attorney', 'attorney'),
    "description" => esc_html__("Attorney About Us Section", "attorney"),
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Main Title", 'attorney'),
            "param_name" => "main_title",
            "value" => '',
            "description" => esc_html__("Section main title.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Main Title Description", 'attorney'),
            "param_name" => "main_title_description",
            "value" => '',
            "description" => esc_html__("Section main title description.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Read More Link", 'attorney'),
            "param_name" => "read_more_link",
            "value" => '',
            "description" => esc_html__("Read More Link of Section.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Section Title", 'attorney'),
            "param_name" => "section_title",
            "value" => '',
            "description" => esc_html__("Title of Section.", 'attorney')
        ),
        array(
            "type" => "textarea_html",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Section Description", 'attorney'),
            "param_name" => "section_description",
            "value" => '',
            "description" => esc_html__("Description of Section.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Phone Title", 'attorney'),
            "param_name" => "phone_no_title",
            "value" => '',
            "description" => esc_html__("Phone Title.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Phone Number", 'attorney'),
            "param_name" => "phone_no",
            "value" => '',
            "description" => esc_html__("Phone Number.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Email Title", 'attorney'),
            "param_name" => "email_title",
            "value" => '',
            "description" => esc_html__("Email Title.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Email Address", 'attorney'),
            "param_name" => "email",
            "value" => '',
            "description" => esc_html__("Email Address.", 'attorney')
        ),
    )
));
}

class WPBakeryShortCode_attorney_about_us_shortcode extends WPBakeryShortCode { }
