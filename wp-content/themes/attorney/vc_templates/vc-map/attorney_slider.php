<?php
/**
 * Attorney - Slider
 * 
 * This template is use by js composer plugin 
 * 
 * @package attorney
 * @subpackage attorney.vc_templates.vc-map
 * @since attorney 1.0.0
 */
add_action('vc_before_init', 'attorneys_slider_shortcode');

function attorneys_slider_shortcode() {
    vc_map(array(
    "name" => esc_html__("Attorneys Slider", 'attorney'),
    "base" => "attorneys_slider_shortcode",
    "class" => "",
    "icon" => "icon-wpb-vc_extend",
    "category" => esc_html__('Attorney', 'attorney'),
    "description" => esc_html__("Attorneys Slider", "attorney"),
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Main Title", 'attorney'),
            "param_name" => "main_title",
            "value" => '',
            "description" => esc_html__("Section main title.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Main Title Description", 'attorney'),
            "param_name" => "main_title_description",
            "value" => '',
            "description" => esc_html__("Section main title description.", 'attorney')
        ),
    )
));

}

class WPBakeryShortCode_attorneys_slider_shortcode extends WPBakeryShortCode { }
