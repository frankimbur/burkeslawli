<?php

/**
 * Attorney - Clients Section
 * 
 * This template is use by js composer plugin 
 * 
 * @package attorney
 * @subpackage attorney.vc_templates.vc-map
 * @since attorney 1.0.0
 */
add_action('vc_before_init', 'attorney_clients_section');

function attorney_clients_section() {

    vc_map(array(
        "name" => esc_html__("Attorney Clients", 'attorney'),
        "base" => "attorney_clients_section_shortcode",
        "class" => "",
        "icon" => "icon-wpb-vc_extend",
        "category" => esc_html__('Attorney', 'attorney'),
        "description" => esc_html__("Clients Section", "attorney"),
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "",
                "class" => "",
                "heading" => esc_html__("Enter Main Title", 'attorney'),
                "param_name" => "main_title",
                "value" => '',
                "description" => esc_html__("Section main title.", 'attorney')
            ),
            array(
                "type" => "textfield",
                "holder" => "",
                "class" => "",
                "heading" => esc_html__("Enter Main Title Description", 'attorney'),
                "param_name" => "main_title_description",
                "value" => '',
                "description" => esc_html__("Section main title description.", 'attorney')
            ),
        )
    ));
}

class WPBakeryShortCode_attorney_clients_section_shortcode extends WPBakeryShortCode {
    
}
