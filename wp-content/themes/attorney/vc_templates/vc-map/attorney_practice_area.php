<?php
/**
 * Attorney - practice area 
 * 
 * This template is use by js composer plugin 
 * 
 * @package attorney
 * @subpackage attorney.vc_templates.vc-map
 * @since attorney 1.0.0
 */
add_action('vc_before_init', 'attorney_practice_area');

function attorney_practice_area() {
   vc_map(array(
    "name" => esc_html__("Attorney Practice Area", 'attorney'),
    "base" => "attorney_practice_area_shortcode",
    "class" => "",
    "icon" => "icon-wpb-vc_extend",
    "category" => esc_html__('Attorney', 'attorney'),
    "description" => esc_html__("Attorney Practice Area Section", "attorney"),
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Main Title", 'attorney'),
            "param_name" => "main_title",
            "value" => '',
            "description" => esc_html__("Section main title.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Enter Main Title Description", 'attorney'),
            "param_name" => "main_title_description",
            "value" => '',
            "description" => esc_html__("Section main title description.", 'attorney')
        ),
        array(
            "type" => "attach_image",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice One Image", 'attorney'),
            "param_name" => "practice_one_image",
            "value" => '',
            "description" => esc_html__("Practice One Image.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice One Title", 'attorney'),
            "param_name" => "practice_one_title",
            "value" => '',
            "description" => esc_html__("Practice One Title.", 'attorney')
        ),
        array(
            "type" => "textarea",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice One Description", 'attorney'),
            "param_name" => "practice_one_description",
            "value" => '',
            "description" => esc_html__("Practice One Description.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice One Link", 'attorney'),
            "param_name" => "practice_one_link",
            "value" => '',
            "description" => esc_html__("Practice One Link.", 'attorney')
        ),
        array(
            "type" => "attach_image",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Two Image", 'attorney'),
            "param_name" => "practice_two_image",
            "value" => '',
            "description" => esc_html__("Practice Two Image.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Two Title", 'attorney'),
            "param_name" => "practice_two_title",
            "value" => '',
            "description" => esc_html__("Practice Two Title.", 'attorney')
        ),
        array(
            "type" => "textarea",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Two Description", 'attorney'),
            "param_name" => "practice_two_description",
            "value" => '',
            "description" => esc_html__("Practice Two Description.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Two Link", 'attorney'),
            "param_name" => "practice_two_link",
            "value" => '',
            "description" => esc_html__("Practice Two Link.", 'attorney')
        ),
        array(
            "type" => "attach_image",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Three Image", 'attorney'),
            "param_name" => "practice_three_image",
            "value" => '',
            "description" => esc_html__("Practice Three Image.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Three Title", 'attorney'),
            "param_name" => "practice_three_title",
            "value" => '',
            "description" => esc_html__("Practice Three Title.", 'attorney')
        ),
        array(
            "type" => "textarea",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Three Description", 'attorney'),
            "param_name" => "practice_three_description",
            "value" => '',
            "description" => esc_html__("Practice Three Description.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Three Link", 'attorney'),
            "param_name" => "practice_three_link",
            "value" => '',
            "description" => esc_html__("Practice Three Link.", 'attorney')
        ),
        array(
            "type" => "attach_image",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Four Image", 'attorney'),
            "param_name" => "practice_four_image",
            "value" => '',
            "description" => esc_html__("Practice Four Image.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Four Title", 'attorney'),
            "param_name" => "practice_four_title",
            "value" => '',
            "description" => esc_html__("Practice Four Title.", 'attorney')
        ),
        array(
            "type" => "textarea",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Four Description", 'attorney'),
            "param_name" => "practice_four_description",
            "value" => '',
            "description" => esc_html__("Practice Four Description.", 'attorney')
        ),
        array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => esc_html__("Practice Four Link", 'attorney'),
            "param_name" => "practice_four_link",
            "value" => '',
            "description" => esc_html__("Practice Four Link.", 'attorney')
        ),
    )
));
 
}

class WPBakeryShortCode_attorney_practice_area_shortcode extends WPBakeryShortCode { }
