<?php

/**
 * Attorney -  js composer file 
 * 
 * This template is use by js composer plugin 
 * 
 * @package attorney
 * @subpackage attorney.vc_templates
 * @since attorney 1.0.0
 */
// 
// require_once( "attorney_about_us.php");
// require_once( "attorney_clients_section.php");
// require_once( "attorney_practice_area.php");
// require_once( "attorney_testimonials_slider.php");
// require_once( "attorney_slider.php");

  $vc_files = array(
		'attorney_about_us.php',
		'attorney_clients_section.php',
		'attorney_practice_area.php',
		'attorney_testimonials_slider.php',
		'attorney_slider.php',
);

foreach ($vc_files as $file) {
    attorney_inclusion('vc_templates/vc-map/'.$file);
}
