<?php
/**
 * Attorney - Slider
 * 
 * This template is use by js composer plugin 
 * 
 * @package attorney
 * @subpackage attorney.vc_templates
 * @since attorney 1.0.0
 */
extract(shortcode_atts(array(
    'main_title' => '',
    'main_title_description' => '',
                ), $atts));
?>
<div class="page-template-attorney-home-two page-template-attorney-home-two-php">
    <section class="attorney animate-effect">
        <div class="container">

            <div class="row">

                <header class="col-xs-12">
                    <a href="javascript: void(0);" class="icon-civil"> <i class="civilean-law-svg"> <img src="<?php echo (ot_get_option('civil_litigation_image')) ? ot_get_option('civil_litigation_image') : get_template_directory_uri() . '/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i> </a>
                    <?php $AttorneySectionTitle = $main_title; ?>
                    <?php $AttorneySectionTitleDescription = $main_title_description; ?>
                    <?php if (!empty($AttorneySectionTitle)): ?>
                        <h2><?php echo esc_html($AttorneySectionTitle); ?></h2>
                    <?php endif; ?>
                    <?php if (!empty($AttorneySectionTitleDescription)): ?>
                        <span class="about-tag"><?php echo esc_html($AttorneySectionTitleDescription); ?></span>
                    <?php endif; ?>
                </header>
            </div>

            <div id="owl-demo">
                <?php $attorney = new WP_Query(array('post_type' => 'attorney', 'posts_per_page' => get_option('posts_per_page'))); ?>
                <?php if ($attorney->have_posts()): ?>
                    <?php while ($attorney->have_posts()): $attorney->the_post(); ?>
                        <div class="attorney-listing zoom">
                            <?php $imageUrl = wp_get_attachment_url(get_post_thumbnail_id()) ?>
                            <?php if (!empty($imageUrl)): ?>
                                <figure> 
                                    <img src="<?php echo attorney_resize($imageUrl, '263', '262'); ?>" alt="" title="" />
                                </figure>
                            <?php endif; ?>
                            <h3><?php the_title(); ?><span  class="status"><?php echo get_post_meta(get_the_id(), 'attorney_designation', 'true'); ?></span></h3>
                            <?php //the_excerpt(); ?>
                            <p><?php echo wp_trim_words(get_the_excerpt(), 15, ''); ?></p>
                            <a href="<?php the_permalink(); ?>" class="arrow-more"> <i class="fa fa-angle-right"> &nbsp;</i></a>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>

        </div>

    </section>
</div>
<?php
echo $this->endBlockComment('attorneys_slider_shortcode');
