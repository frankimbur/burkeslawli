<?php
/**
 * Attorney - Practice Area 
 *  
 * This template is use by js composer plugin 
 * 
 * @package attorney
 * @subpackage attorney.vc_templates
 * @since attorney 1.0.0
 */
extract(shortcode_atts(array(
    'main_title' => '',
    'main_title_description' => '',
    'practice_one_image' => '',
    'practice_two_image' => '',
    'practice_three_image' => '',
    'practice_four_image' => '',
    'practice_one_title' => '',
    'practice_two_title' => '',
    'practice_three_title' => '',
    'practice_four_title' => '',
    'practice_one_description' => '',
    'practice_two_description' => '',
    'practice_three_description' => '',
    'practice_four_description' => '',
    'practice_one_link' => '',
    'practice_two_link' => '',
    'practice_three_link' => '',
    'practice_four_link' => '',
                ), $atts));
?>
<div class="page-template-attorney-home-two page-template-attorney-home-two-php">
    <section class="practice-area animate-effect">
        <div class="container">
            <div class="row">
                <header class="col-xs-12 section-header">
                    <a href="javascript: void(0);" class="icon-civil"> <i class="civilean-law-svg"> <img src="<?php echo (ot_get_option('civil_litigation_image')) ? ot_get_option('civil_litigation_image') : get_template_directory_uri() . '/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i> </a>
                    <?php $PracticeSectionTitle = $main_title; ?>
                    <?php $PracticeSectionTitleDescription = $main_title_description; ?>
                    <?php if (!empty($PracticeSectionTitle)): ?>
                        <h2><?php echo esc_html($PracticeSectionTitle); ?></h2>
                    <?php endif; ?>
                    <?php if (!empty($PracticeSectionTitleDescription)): ?>
                        <span class="about-tag"><?php echo esc_html($PracticeSectionTitleDescription); ?></span>
                    <?php endif; ?>
                </header>
                <div class="col-xs-12 practice-law-list">
                    <div class="row">
                        <!-- loop -->
                        <div class="col-xs-12 col-sm-3 col-md-3 family-law">
                            <div class="family-group">
                                <?php if ((wp_get_attachment_url($practice_one_image))): ?>
                                    <i class="family-law-svg"> <img src="<?php echo wp_get_attachment_url($practice_one_image); ?>" alt=""  title="" class="svg"/> </i>
                                <?php else: ?>
                                    <i class="family-law-svg"> <img src="<?php echo get_template_directory_uri() . '/assets/svg/family-law.svg'; ?>" alt=""  title="" class="svg"/> </i>
                                <?php endif; ?>
                            </div>
                            <h3 class="h3"><?php echo $practice_one_title; ?></h3>
                            <p class="content-description"><?php echo $practice_one_description; ?></p>
                            <a href="<?php echo $practice_one_link; ?>" class="arrow-more"> <i class="fa fa-angle-right"> &nbsp;</i></a>
                        </div>
                        <!-- loop -->
                        <!-- loop -->
                        <div class="col-xs-12 col-sm-3 col-md-3 family-law">
                            <div class="family-group">
                                <?php if ((wp_get_attachment_url($practice_two_image))): ?>
                                    <i class="family-law-svg"> <img src="<?php echo wp_get_attachment_url($practice_two_image); ?>" alt=""  title="" class="svg"/> </i>
                                <?php else: ?>
                                    <i class="family-law-svg"> <img src="<?php echo get_template_directory_uri() . '/assets/svg/business-law.svg'; ?>" alt=""  title="" class="svg"/> </i>
                                <?php endif; ?>
                            </div>
                            <h3 class="h3"><?php echo $practice_two_title; ?></h3>
                            <p class="content-description"><?php echo $practice_two_description; ?></p>
                            <a href="<?php echo $practice_two_link; ?>" class="arrow-more"> <i class="fa fa-angle-right"> &nbsp;</i></a>
                        </div>
                        <!-- loop -->
                        <!-- loop -->
                        <div class="col-xs-12 col-sm-3 col-md-3 family-law">
                            <div class="family-group">
                                <?php if ((wp_get_attachment_url($practice_three_image))): ?>
                                    <i class="family-law-svg"> <img src="<?php echo wp_get_attachment_url($practice_three_image); ?>" alt=""  title="" class="svg"/> </i>
                                <?php else: ?>
                                    <i class="family-law-svg"> <img src="<?php echo get_template_directory_uri() . '/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i>
                                <?php endif; ?>
                            </div>
                            <h3 class="h3"><?php echo $practice_three_title; ?></h3>
                            <p class="content-description"><?php echo $practice_three_description; ?></p>
                            <a href="<?php echo $practice_three_link; ?>" class="arrow-more"> <i class="fa fa-angle-right"> &nbsp;</i></a>
                        </div>
                        <!-- loop -->
                        <!-- loop -->
                        <div class="col-xs-12 col-sm-3 col-md-3 family-law">
                            <div class="family-group">
                                <?php if ((wp_get_attachment_url($practice_four_image))): ?>
                                    <i class="family-law-svg"> <img src="<?php echo wp_get_attachment_url($practice_four_image); ?>" alt=""  title="" class="svg"/> </i>
                                <?php else: ?>
                                    <i class="family-law-svg"> <img src="<?php echo get_template_directory_uri() . '/assets/svg/trust-estates.svg'; ?>" alt=""  title="" class="svg"/> </i>
                                <?php endif; ?>
                            </div>
                            <h3 class="h3"><?php echo $practice_four_title; ?></h3>
                            <p class="content-description"><?php echo $practice_four_description; ?></p>
                            <a href="<?php echo $practice_four_link; ?>" class="arrow-more"> <i class="fa fa-angle-right"> &nbsp;</i></a>
                        </div>
                        <!-- loop -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
echo $this->endBlockComment('attorney_practice_area_shortcode');
