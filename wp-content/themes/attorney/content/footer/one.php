<?php
/**
 * Attorney - Footer Template 
 *
 * @package Attorney
 * @since attorney 1.0
 */
?>
<footer id="footer" class="footer-type-one">
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-8">
                    <nav class="navbar navbar-default">
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                            <?php wp_nav_menu(array('theme_location' => 'footer_menu', 'menu_class' => 'nav navbar-nav', 'container' => 'nav')); ?>
                        </div>
                    </nav>
                </div>
                <div class="consultation">
                    <?php $contact = ot_get_option('attorney_consultation'); ?> 
                    <?php if (!empty($contact)): ?>
                        <a href="<?php echo esc_attr($contact, 'attorney'); ?>">
                            <i class="svg-shape icon-enevlope"> 
                                <?php echo esc_url(attorneySvgIcon('enevlope')); ?>
                            </i><span><?php echo ot_get_option('attorney_consultation_text'); ?>&nbsp;<i class="fa fa-chevron-right"></i></span> </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6 block-right">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 practice-listing practice-footer tablet-mode">
                            <?php if (is_active_sidebar('footer-one-sidebar-three')): ?>
                                <?php dynamic_sidebar('footer-one-sidebar-three'); ?>
                            <?php endif; ?>
                        </div>

                        <div class="col-xs-12 col-sm-6 practice-listing listing-continue tablet-mode">
                            <?php if (is_active_sidebar('footer-one-sidebar-four')): ?>
                                <?php dynamic_sidebar('footer-one-sidebar-four'); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6  spacer-mobile">
                            <?php if (is_active_sidebar('footer-one-sidebar-one')): ?>
                                <?php dynamic_sidebar('footer-one-sidebar-one'); ?>
                            <?php endif; ?>
                            <span class="copy-right">
                                <?php echo ot_get_option('at_copyright'); ?>
                            </span>
                        </div>
                        <div class="col-xs-12 col-sm-6  spacer-mobile">
                            <?php if (is_active_sidebar('footer-one-sidebar-two')): ?>
                                <?php dynamic_sidebar('footer-one-sidebar-two'); ?>
                            <?php endif; ?>
                            <?php do_action("attorney_header_social"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--footer Section End Here -->
</div>
<?php wp_footer(); ?>
</body>
</html>
