<?php
/**
 * Attorney - Footer Template 
 *
 * @package Attorney
 * @since attorney 1.0
 */
?>
<!--footer Section Start Here -->
<footer id="footer" class="footer-type-four">
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 spacer-mobile">
                    <?php if (is_active_sidebar('footer-four-sidebar-one')): ?>
                        <?php dynamic_sidebar('footer-four-sidebar-one'); ?> 
                    <?php endif; ?>
                    <?php if (is_active_sidebar('footer-four-sidebar-two')): ?>
                        <?php dynamic_sidebar('footer-four-sidebar-two'); ?> 
                    <?php endif; ?>
                    <?php do_action("attorney_header_social"); ?>
                </div>
                <div class="col-xs-12 col-sm-3 practice-footer tablet-mode practice-listing">
                    <?php if (is_active_sidebar('footer-four-sidebar-three')): ?>
                        <?php dynamic_sidebar('footer-four-sidebar-three'); ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-12 col-sm-5">
                    <?php if (is_active_sidebar('footer-four-sidebar-four')): ?>
                        <?php dynamic_sidebar('footer-four-sidebar-four'); ?> 
                    <?php endif; ?>
                </div>
            </div>
            <span class="copy-right"><?php echo ot_get_option('at_copyright'); ?></span>
        </div>
    </div>
</footer>
<!--footer Section End Here -->
</div>
<?php wp_footer(); ?>
</body>
</html>
