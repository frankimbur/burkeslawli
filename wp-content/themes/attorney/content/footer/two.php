<?php
/**
 * Attorney - Footer Template 
 *
 * @package Attorney
 * @since attorney 1.0
 */
?>
<!--footer Section Start Here -->
<footer id="footer" class="footer-type-two">

    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 spacer-mobile footer-box">
                    <?php if (is_active_sidebar('footer-two-sidebar-one')): ?>
                        <?php dynamic_sidebar('footer-two-sidebar-one'); ?>
                    <?php endif; ?>
                    <span class="get-theme">
                        <?php echo ot_get_option('at_copyright'); ?>
                    </span>
                    <?php echo ot_get_option('at_designed_by'); ?>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 practice-footer practice-listing tablet-mode footer-box">
                    <?php if (is_active_sidebar('footer-two-sidebar-two')): ?>
                        <?php dynamic_sidebar('footer-two-sidebar-two'); ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 practice-listing listing-continue tablet-mode spacer-mobile footer-box footer-list-right">
                    <?php if (is_active_sidebar('footer-two-sidebar-three')): ?>
                        <?php dynamic_sidebar('footer-two-sidebar-three'); ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 footer-box">

                    <?php if (is_active_sidebar('footer-two-sidebar-four')): ?>
                        <?php dynamic_sidebar('footer-two-sidebar-four'); ?>
                    <?php endif; ?>
                    <?php do_action("attorney_header_social"); ?>
                </div>
            </div>
        </div>

    </div>

</footer>
<!--footer Section End Here -->
</div>
<?php wp_footer(); ?>
</body>
</html>
