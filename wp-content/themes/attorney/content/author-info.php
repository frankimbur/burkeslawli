<div class="blog-details-page">
    <div class="blog-user-info clearfix">
        <figure class="animate-effect anim-section animate">
            <?php $authorID = get_post_field('post_author', get_the_id()); ?>
            <img title="" alt="" src="<?php echo esc_url((get_the_author_meta('image', $authorID))? get_the_author_meta('image', $authorID): get_template_directory_uri()."/assets/img/default_avatar.jpeg"); ?>">
            <?php //echo get_avatar( get_the_author_meta( 'ID' ), 32 ); ?>
        </figure>
        <div class="user-blog-detail">
            <div class="user-detail-header">
                <div class="name-user-info animate-effect anim-section animate">
                    <h3><?php echo get_the_author_meta('nickname', $authorID); ?></h3>
                    <span class="heading-details"><?php echo get_the_author_meta('position', $authorID); ?></span>
                </div>
                <div class="media-selection animate-effect anim-section animate">
                    <span><?php _e('Follow me on :', 'attorney'); ?></span>
                    <span data-toggle="dropdown" class="social-icons dropdown-toggle"> <span class="facebook_icon"> <a class="fa fa-facebook" href="#"></a> </span> <span class="arrow"><a class="fa fa-angle-down" href="#"></a></span> </span>
                    <ul role="menu" class="dropdown-menu">
                        <?php $facebook = get_the_author_meta('facebook', $authorID); ?>
                        <?php $google_plus = get_the_author_meta('google_plus', $authorID); ?>
                        <?php $twitter = get_the_author_meta('twitter', $authorID); ?>
                        <?php $pinterest = get_the_author_meta('pinterest', $authorID); ?>
                        <?php $instagram = get_the_author_meta('instagram', $authorID); ?>
                        
                        <?php if(!empty($facebook)): ?>
                        <li class="facebook-share">
                            <a class="fa fa-facebook" href="<?php echo esc_url($facebook); ?>"></a>
                        </li>
                        <?php endif; ?>
                        <?php if(!empty($twitter)): ?>
                        <li class="twitter-share">
                            <a class="fa fa-twitter" href="<?php echo esc_url($twitter); ?>"></a>
                        </li>
                        <?php endif; ?>
                        <?php if(!empty($google_plus)): ?>
                        <li class="plus-share">
                            <a class="fa fa-google-plus" href="<?php echo esc_url($google_plus); ?>"></a>
                        </li>
                        <?php endif; ?>
                        <?php if(!empty($pinterest)): ?>
                        <li class="plus-share">
                            <a class="fa fa-pinterest" href="<?php echo esc_url($pinterest); ?>"></a>
                        </li>
                        <?php endif; ?>
                        <?php if(!empty($instagram)): ?>
                        <li class="plus-share">
                            <a class="fa fa-instagram" href="<?php echo esc_url($instagram); ?>"></a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <p><?php echo get_the_author_meta('description', $authorID) ?></p>
            <?php if(!is_author()): ?>
            <a href="<?php echo get_author_posts_url($authorID); ?>" class="more-btn animate-effect anim-section animate"><?php _e('View more post of ', 'attorney'); echo get_the_author_meta('nickname', $authorID); ?><i class="fa fa-chevron-right"></i> </a>
            <?php endif; ?>
        </div>
    </div>
</div>
