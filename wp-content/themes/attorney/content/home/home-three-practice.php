<?php
/**
 * Attorney -  Home Three Practice
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="practice-area anim-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 section-header">
                <a href="javascript: void(0);" class="icon-civil"> <i class="civilean-law-svg"> <img src="<?php echo (ot_get_option('civil_litigation_image'))? ot_get_option('civil_litigation_image'): get_template_directory_uri().'/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i> </a>
                <?php $PracticeSectionTitle = get_post_meta(get_the_id(), 'practice_section_title3', true); ?>
                <?php $PracticeSectionTitleDescription = get_post_meta(get_the_id(), 'practice_section_title_description3', true); ?>
                <?php if (!empty($PracticeSectionTitle)): ?>
                    <h2><?php echo esc_html($PracticeSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($PracticeSectionTitleDescription)): ?>
                    <span class="about-tag"><?php echo esc_html($PracticeSectionTitleDescription); ?></span>
                <?php endif; ?>
            </div>
            <?php $PracticeAreaList = get_post_meta(get_the_id(), 'practice_area_list3', true); ?>
            <?php if (!empty($PracticeAreaList)): ?>
            <div class="col-xs-12 practice-law-list">
                <div class="row">
                    <?php foreach ($PracticeAreaList as $PracticeArea): ?>
                    <div class="col-xs-12 col-sm-3 col-md-3 family-law">
                        <?php if (!empty($PracticeArea['practice_area_image3'])): ?>
                            <div class="family-group">
                                <i class="family-law-svg"> <img src="<?php echo esc_url($PracticeArea['practice_area_image3']); ?>" alt=""  title="" class="svg"/> </i>
                            </div>
                        <?php endif; ?>
                        
                        <?php if (!empty($PracticeArea['title'])): ?>
                            <h3 class="h3"><?php echo esc_html($PracticeArea['title']); ?></h3>
                        <?php endif; ?>
                            
                        <?php if (!empty($PracticeArea['practice_area_description3'])): ?>
                            <p class="content-description"><?php echo esc_html($PracticeArea['practice_area_description3']); ?></p>
                        <?php endif; ?>
                        <?php if (!empty($PracticeArea['practice_area_link3'])): ?>
                                <a href="<?php echo get_permalink($PracticeArea['practice_area_link3']); ?>" class="arrow-more"> <i class="fa fa-angle-right"> &nbsp;</i></a>
                            <?php endif; ?>     
                    </div>
                    <?php endforeach; ?>
                </div>

            </div>
            <?php endif; ?>
        </div>

    </div>

</section>
