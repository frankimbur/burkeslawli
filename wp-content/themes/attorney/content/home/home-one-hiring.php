<?php
/**
 * Attorney -  Home One Hiring
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<?php $HiringSectionBackground = get_post_meta(get_the_id(), 'hiring_section_background', true); ?>
<?php $HiringSectionStyle = ($HiringSectionBackground)? 'style="background-image: url('.$HiringSectionBackground.');"':''; ?>

<section <?php echo ($HiringSectionStyle)? $HiringSectionStyle: ''; ?>  class="hiring-info animate-effect">
    <div class="container ">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8">
                <?php $HiringSectionTitle = get_post_meta(get_the_id(), 'hiring_section_title', true); ?>
                <?php $HiringSectionDescription = get_post_meta(get_the_id(), 'hiring_section_description', true); ?>
                <?php $HiringSectionLink = get_post_meta(get_the_id(), 'hiring_section_link', true); ?>
                <?php if (!empty($HiringSectionTitle)): ?>
                    <h2 class="long-heading"><?php echo esc_html($HiringSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($HiringSectionDescription)): ?>
                    <p class="hiring-detail"><?php echo esc_html($HiringSectionDescription); ?></p>
                <?php endif; ?>
            </div>
            <?php if (!empty($HiringSectionLink)): ?>
                <div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-1">
                    <a href="<?php echo esc_url($HiringSectionLink); ?>" class="more-btn btn-footer"><?php _e('Know More', 'attorney'); ?><i class="fa fa-chevron-right"></i></a>
                </div>
            <?php endif; ?>
        </div>

    </div>

</section>
