<?php
/**
 * Attorney -  Home Three Hiring
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="hiring-info-detail anim-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <a href="#" class="icon-civil"><i class="civilean-law-svg"> <img src="<?php echo (ot_get_option('civil_litigation_image'))? ot_get_option('civil_litigation_image'): get_template_directory_uri().'/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i></a>
                <div class="heading-hiring">
                     <?php $HiringSectionTitle = get_post_meta(get_the_id(), 'hiring_section_title3', true); ?>
                    <?php $HiringSectionDescription = get_post_meta(get_the_id(), 'hiring_section_description3', true); ?>
                    <?php $HiringSectionLink = get_post_meta(get_the_id(), 'hiring_section_link3', true); ?>
                     <?php $HiringSectionSubTitle = get_post_meta(get_the_id(), 'hiring_section_sub_title3', true); ?>
                        <?php if (!empty($HiringSectionTitle)): ?>
                        <h2><?php echo esc_html($HiringSectionTitle); ?></h2>
                    <?php endif; ?>
                    <?php if (!empty($HiringSectionSubTitle)): ?>
                        <p class="about-tag"><?php echo esc_html($HiringSectionSubTitle); ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 apply-section">
                <?php if (!empty($HiringSectionDescription)): ?>
                        <p><?php echo esc_html($HiringSectionDescription); ?></p>
                    <?php endif; ?>
                   <?php if(!empty($HiringSectionLink)): ?>
                        <a href="<?php echo esc_attr($HiringSectionLink); ?>" class="btn-default btn-grey"><?php _e('apply now', 'attorney'); ?></a>
                  <?php endif; ?>
            </div>
        </div>
    </div>
</section>
