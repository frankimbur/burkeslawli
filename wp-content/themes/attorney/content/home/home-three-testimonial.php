<?php
/**
 * Attorney -  Home Three Testimonials
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
               
<section class="testimonial animate-effect">
    <div class="container">
        <div class="row">
            <header class="col-xs-12 section-header">
                <a href="#" class="icon-civil"> <i class="civilean-law-svg"> <img src="<?php echo (ot_get_option('civil_litigation_image'))? ot_get_option('civil_litigation_image'): get_template_directory_uri().'/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i> </a>
                <?php $TestimonialsSectionTitle = get_post_meta(get_the_id(), 'testimonials_section_title3', true); ?>
                <?php $TestimonialsSectionTitleDescription = get_post_meta(get_the_id(), 'testimonials_section_title_description3', true); ?>
                <?php if (!empty($TestimonialsSectionTitle)): ?>
                    <h2><?php echo esc_html($TestimonialsSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($TestimonialsSectionTitleDescription)): ?>
                    <span class="about-tag"><?php echo esc_html($TestimonialsSectionTitleDescription); ?></span>
                <?php endif; ?>
            </header>
        </div>
        <div id="owl-two">
            <?php $NoOfPosts = get_post_meta(get_the_id(), 'no_of_testimonials3', true); ?>
            <?php
            if (empty($NoOfPosts)):
                $NoOfPosts = get_option('posts_per_page');
            endif;
            $testimonial = new WP_Query(array('post_type' => 'testimonial', 'posts_per_page' => $NoOfPosts));
            ?>
            <?php if ($testimonial->have_posts()): ?>
                <?php while ($testimonial->have_posts()): $testimonial->the_post(); ?>
                    <div class="row" >
                        <div class="col-xs-12 col-sm-10 col-md-10 col-md-offset-1 col-sm-offset-1 testimonial-content">
                            <p class="testimonial-detail side-colon"><?php echo get_the_content(); ?></p>
                        </div>

                        <div class="testimonial-member clearfix">
                            <div class="member-container clearfix">
                                <div class="member-pics">
                                    <?php $imageUrl = wp_get_attachment_url(get_post_thumbnail_id()) ?>
                                    <?php if (!empty($imageUrl)): ?>
                                        <figure> 
                                            <img src="<?php echo attorney_resize($imageUrl, '72', '72'); ?>" alt="" title="" />
                                        </figure>
                                    <?php endif; ?>
                                </div>
                                <div class="member-content">
                                    <span><?php _e('By', 'attorney'); ?> </span>
                                    <strong><?php the_title(); ?></strong>
                                </div>
                            </div>

                        </div>

                    </div>
                    <?php
                endwhile;
            endif;
            wp_reset_postdata();
            ?>
        </div>

    </div>
</section>
