<?php
/**
 * Attorney -  Home Four News & Testimonials
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="news-testimonial anim-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php $NewsSectionTitle = get_post_meta(get_the_id(), 'news_section_title4', true); ?>
                <?php $NewsSectionTitleDescription = get_post_meta(get_the_id(), 'news_section_title_description4', true); ?>
                <?php if (!empty($NewsSectionTitle)): ?>
                    <h2><?php echo esc_html($NewsSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($NewsSectionTitleDescription)): ?>
                    <span class="practice-desp"><?php echo esc_html($NewsSectionTitleDescription); ?></span>
                <?php endif; ?>
            </div>
        </div>
        <?php $Blogposts = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 2)); ?>
        <?php if ($Blogposts->have_posts()): ?>
            <?php
            $i = 1;
            while ($Blogposts->have_posts()) : $Blogposts->the_post();
                ?>
                <div class="row equal  <?php echo ($i % 2 != 0) ? 'odd' : 'even'; ?>">
                    <div class="col-xs-12">
                        <div class="testimonial-pics">
                            <?php $imageUrl = wp_get_attachment_url(get_post_thumbnail_id()) ?>
                            <?php if (!empty($imageUrl)): ?>
                                <figure class="user-image"> 
                                    <img src="<?php echo esc_url(attorney_resize($imageUrl, '571', '441')); ?>" alt="" title="" />
                                </figure>
                            <?php endif; ?>
                        </div>
                        <div class="testimonial-text-right <?php echo ($i % 2 != 0) ? 'change-bg' : 'new-black'; ?>">
                            <div class="head-date">
                                <span class="date"><?php echo get_the_date('M Y'); ?></span>
                                <span class="comment"><?php echo wp_count_comments(get_the_id())->approved; ?> <?php _e('Comment', 'attorney'); ?></span>
                            </div>
                            <h2><?php the_title(); ?></h2>
                            <?php
                            $categories = wp_get_post_categories(get_the_id());
                            $flag = 0;
                            ?>
                            <span class="posted-law"><?php esc_html_e('Posted in', 'attorney'); ?> <?php foreach ($categories as $category): ?>
                                    <?php if ($flag == 0): ?>
                                        <a href="<?php echo get_category_link($category); ?>"><?php echo get_cat_name($category); ?></a><?php
                                        $flag = 1;
                                    else:
                                        ?><a href="<?php echo get_category_link($category); ?>">, <?php echo get_cat_name($category); ?></a>
                                    <?php
                                    endif;
                                endforeach;
                                ?></span>
                                <?php the_excerpt(); ?>
                            <a class="more-btn" href="<?php the_permalink(); ?>"><?php _e('Know More', 'attorney'); ?> <i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
                <?php
                $i++;
            endwhile;
        endif;
        wp_reset_postdata();
        ?> 
        <div class="testimonial-box">
            <div class="row">
                <div class="col-xs-12">
                    <?php $TestimonialsSectionTitle = get_post_meta(get_the_id(), 'testimonials_section_title4', true); ?>
                    <?php $TestimonialsSectionTitleDescription = get_post_meta(get_the_id(), 'testimonials_section_title_description4', true); ?>
                    <?php if (!empty($TestimonialsSectionTitle)): ?>
                        <h2><?php echo esc_html($TestimonialsSectionTitle); ?></h2>
                    <?php endif; ?>
                    <?php if (!empty($TestimonialsSectionTitleDescription)): ?>
                        <span class="practice-desp"><?php echo esc_html($TestimonialsSectionTitleDescription); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div id="owl-two" class="testimonial-box1">
                <?php $NoOfPosts = get_post_meta(get_the_id(), 'no_of_testimonials4', true); ?>
                <?php
                if (empty($NoOfPosts)):
                    $NoOfPosts = get_option('posts_per_page');
                endif;
                $testimonial = new WP_Query(array('post_type' => 'testimonial', 'posts_per_page' => $NoOfPosts));
                ?>
                <?php if ($testimonial->have_posts()): ?>
                    <?php while ($testimonial->have_posts()): $testimonial->the_post(); ?>
                        <div class="row">

                            <div class="col-xs-12 col-sm-2 testimonial-user">
                                <?php $imageUrl = wp_get_attachment_url(get_post_thumbnail_id()) ?>
                                <?php if (!empty($imageUrl)): ?>
                                    <figure> 
                                        <img src="<?php echo attorney_resize($imageUrl, '96', '96'); ?>" alt="" title="" />
                                    </figure>
                                <?php endif; ?>
                                <span><?php the_title(); ?></span>
                                <?php $TestimonialRating = get_post_meta(get_the_id(), 'testimonial_rating', true); ?>
                                <?php if (!empty($TestimonialRating)): ?>
                                    <?php
                                    $maxStar = 5;
                                    $count = 1;
                                    $halfStar = 0;
                                    if ($TestimonialRating > $maxStar) {
                                        $TestimonialRating = $maxStar;
                                    }
                                    if (floor($TestimonialRating) < $TestimonialRating) {
                                        $halfStar = 1;
                                    }
                                    ?>
                                    <div class="fk-stars clearfix" title="5-star">
                                        <?php
                                        while ($count <= $maxStar):
                                            print('<i class="fa fa-star"></i>');
                                            $count++;
                                            if ($count > floor($TestimonialRating)) {
                                                $count--;
                                                break;
                                            }
                                        endwhile;
                                        if (($count < $maxStar) && ($halfStar == 1)) {
                                            print('<i class="fa fa-star-half-empty"></i>');
                                            $count++;
                                        }
                                        $emptyStar = $maxStar - $count;
                                        if ($emptyStar > 0) {
                                            for ($i = 1; $i <= $emptyStar; $i++) {
                                                print('<i class="fa fa-star star-bg"></i>');
                                            }
                                        }
                                        ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-xs-12 col-sm-10 user-information-block">
                                <?php $TestimonialSubTitle = get_post_meta(get_the_id(), 'testimonial_sub_title', true); ?>
                                <?php if (!empty($TestimonialSubTitle)): ?>
                                    <h2 class="h2"><?php echo esc_html($TestimonialSubTitle); ?></h2>
                                <?php endif; ?>
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <?php
                    endwhile;
                endif;
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</section>
<?php 
