<?php
/**
 * Attorney -  Home One News & Testimonials
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="news-testimonial  animate-effect">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6 news">
                <?php $NewsSectionTitle = get_post_meta(get_the_id(), 'news_section_title', true); ?>
                <?php $NewsSectionTitleDescription = get_post_meta(get_the_id(), 'news_section_title_description', true); ?>
                <?php if (!empty($NewsSectionTitle)): ?>
                    <h2><?php echo esc_html($NewsSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($NewsSectionTitleDescription)): ?>
                    <span class="practice-desp"><?php echo esc_html($NewsSectionTitleDescription); ?></span>
                <?php endif; ?>
                <div class="row">

                    <?php $Blogposts = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 2)); ?>
                    <?php if ($Blogposts->have_posts()): ?>
                        <?php while ($Blogposts->have_posts()) : $Blogposts->the_post(); ?>
                            <article class="col-xs-12 col-sm-6 col-md-6 spacer-mobile news-half  zoom">
                                <?php $imageUrl = wp_get_attachment_url(get_post_thumbnail_id()) ?>
                                <?php if (!empty($imageUrl)): ?>
                                    <figure class="user-image"> 
                                        <img src="<?php echo attorney_resize($imageUrl, '262', '198'); ?>" alt="" title="" />
                                    </figure>
                                <?php endif; ?>
                                <div class="figure-description clearfix">
                                    <span class="date"><?php echo get_the_date('M Y'); ?></span>
                                    <span class="comment"><?php echo wp_count_comments(get_the_id())->approved; ?> <?php _e('Comment', 'attorney'); ?></span>
                                </div>

                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <p><?php echo wp_trim_words( get_the_excerpt(), 15, '' ); ?></p>
                                <?php $categories = wp_get_post_categories(get_the_id());
                                $flag = 0;
                                ?>
                                <span class="posted-law"><?php _e('Posted in', 'attorney'); ?><?php foreach ($categories as $category): ?>
                                        <?php if ($flag == 0): ?>
                                            <a href="<?php echo get_category_link($category); ?>"><?php echo get_cat_name($category); ?></a><?php
                                            $flag = 1;
                                        else:
                                            ?><a href="<?php echo get_category_link($category); ?>">, <?php echo get_cat_name($category); ?></a>
                                        <?php
                                        endif;
                                    endforeach;
                                    ?></span>
                            </article>
                            <?php
                        endwhile;
                    endif;
                    wp_reset_postdata();
                    ?> 

                </div>

            </div>

            <div class="col-xs-12 col-md-6 testimonial">
                <?php $TestimonialsSectionTitle = get_post_meta(get_the_id(), 'testimonials_section_title', true); ?>
                <?php $TestimonialsSectionTitleDescription = get_post_meta(get_the_id(), 'testimonials_section_title_description', true); ?>
                <?php if (!empty($TestimonialsSectionTitle)): ?>
                    <h2><?php echo esc_html($TestimonialsSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($TestimonialsSectionTitleDescription)): ?>
                    <span class="practice-desp"><?php echo esc_html($TestimonialsSectionTitleDescription); ?></span>
                <?php endif; ?>
                <div class="row" id="owl-demo-home">
                    <?php $NoOfPosts = get_post_meta(get_the_id(), 'no_of_testimonials', true); ?>
                    <?php $backgroundImage = get_post_meta(get_the_id(), 'testimonial_slider_background', true); ?>
                    <?php $backgroundStyle = ($backgroundImage)? 'style="background-image: url('.$backgroundImage.');"': ''; ?>
                    <?php
                    if (empty($NoOfPosts)):
                        $NoOfPosts = get_option('posts_per_page');
                    endif;
                    $testimonial = new WP_Query(array('post_type' => 'testimonial', 'posts_per_page' => $NoOfPosts));
                    ?>
                    <?php if ($testimonial->have_posts()): ?>
                        <?php while ($testimonial->have_posts()): $testimonial->the_post(); ?>
                    <div <?php echo ($backgroundStyle)? $backgroundStyle : ''; ?> class="col-xs-12 user-testimonial ">
                                <div class="testimonial-inner ">
                                    <?php $TestimonialSubTitle = get_post_meta(get_the_id(), 'testimonial_sub_title', true); ?>
                                    <?php if (!empty($TestimonialSubTitle)): ?>
                                        <h2 class="h2"><?php echo esc_html($TestimonialSubTitle); ?></h2>
                                    <?php endif; ?>
                                    <?php the_content(); ?>
                                    <span class="user-name"><?php the_title(); ?></span>
                                    <?php $TestimonialRating = get_post_meta(get_the_id(), 'testimonial_rating', true); ?>
                                    <?php if (!empty($TestimonialRating)): ?>
                                        <?php
                                        $maxStar = 5;
                                        $count = 1;
                                        $halfStar = 0;
                                        if ($TestimonialRating > $maxStar) {
                                            $TestimonialRating = $maxStar;
                                        }
                                        if (floor($TestimonialRating) < $TestimonialRating) {
                                            $halfStar = 1;
                                        }
                                        ?>
                                        <div class="fk-stars clearfix" title="5-star">
                                            <?php
                                            while ($count <= $maxStar):
                                                echo '<i class="fa fa-star"></i>';
                                                $count++;
                                                if ($count > floor($TestimonialRating)) {
                                                    $count--;
                                                    break;
                                                }
                                            endwhile;
                                            if (($count < $maxStar) && ($halfStar == 1)) {
                                                echo '<i class="fa fa-star-half-empty"></i>';
                                                $count++;
                                            }
                                            $emptyStar = $maxStar - $count;
                                            if ($emptyStar > 0) {
                                                for ($i = 1; $i <= $emptyStar; $i++) {
                                                    echo '<i class="fa fa-star star-bg"></i>';
                                                }
                                            }
                                            ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php $imageUrl = wp_get_attachment_url(get_post_thumbnail_id()) ?>
                                    <?php if (!empty($imageUrl)): ?>
                                        <figure class="user-image"> 
                                            <img src="<?php echo attorney_resize($imageUrl, '96', '96'); ?>" alt="" title="" />
                                        </figure>
                                    <?php endif; ?>

                                </div>

                            </div>
                            <?php
                        endwhile;
                    endif;
                    wp_reset_postdata();
                    ?>
                </div>

            </div>

        </div>
    </div>
</section>
