<?php
/**
 * Attorney -  Home Three About Us
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="about-us anim-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 about-us-detail ">

                <a href="#" class="icon-civil"><i class="civilean-law-svg"> <img src="<?php echo (ot_get_option('civil_litigation_image'))? ot_get_option('civil_litigation_image'): get_template_directory_uri().'/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i></a>
                <?php $aboutTitle = get_post_meta(get_the_id(), 'about_title3', true); ?>
                <?php if(!empty($aboutTitle)): ?>
                <h2><?php echo esc_html($aboutTitle); ?></h2>
                <?php endif; ?>
                
                <?php $AboutTitleDescription = get_post_meta(get_the_id(), 'about_title_description3', true); ?>
                <?php if(!empty($AboutTitleDescription)): ?>
                <span class="about-tag"><?php echo esc_html($AboutTitleDescription); ?></span>
                <?php endif; ?>


            </div>

            <div class="col-xs-12 ">
                <div class="about-us-details">

                    <?php $AboutContentDescription = get_post_meta(get_the_id(), 'about_content_description3', true); ?>
                    <?php if(!empty($AboutContentDescription)): ?>
                    <p class="content-description"><?php print($AboutContentDescription); ?></p>
                    <?php endif; ?>
                    
                </div>
                <div class="contact-details">
                    <span><?php _e('Get your Free Consultation  :', 'attorney'); ?></span>
                    <div class="phone-detail clearfix">
                        <?php $AboutContactNo = get_post_meta(get_the_id(), 'about_contact_no3' ,true); ?>
                        <?php if(!empty($AboutContactNo)): ?>
                        <a href="callto:<?php echo esc_attr($AboutContactNo); ?>" class="phone-number"><?php echo esc_html($AboutContactNo); ?></a>
                        <?php endif; ?>
                        <span class="seprate"><?php _e('or', 'attorney'); ?></span>
                         <?php $AaboutEmail = get_post_meta(get_the_id(), 'about_email3' ,true); ?>
                        <?php if(!empty($AaboutEmail)): ?>
                        <a href="mailto:<?php echo esc_attr($AaboutEmail); ?>" class="phone-number email-build"><?php echo esc_html($AaboutEmail); ?></a>
                        <?php endif; ?>
                    </div>

                </div>

            </div>
        </div>
    </div>

</section>
