<?php
/**
 * Attorney -  Home Two Slider
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<!--slider Section Start Here -->
<section id="slider" class="slider-two">
    <div class="container-fluid">
        <div class="row">
            <div class="banner-slider">
                <?php echo do_shortcode(get_post_meta(get_the_id(), 'attorney_rev_slider2', true)); ?>
            </div>
        </div>
    </div>

</section>
<!--slider Section End Here -->
