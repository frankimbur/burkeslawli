<?php /**
 * Attorney -  Home Four Practice
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */ ?>
<section class="practice-area anim-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 practice-header">
                <?php $PracticeSectionTitle = get_post_meta(get_the_id(), 'practice_section_title4', true); ?>
                <?php $PracticeSectionTitleDescription = get_post_meta(get_the_id(), 'practice_section_title_description4', true); ?>
                <?php if (!empty($PracticeSectionTitle)): ?>
                    <h2><?php echo esc_html($PracticeSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($PracticeSectionTitleDescription)): ?>
                    <span class="practice-desp"><?php echo esc_html($PracticeSectionTitleDescription); ?></span>
                <?php endif; ?>
            </div>

            <?php $PracticeAreaList = get_post_meta(get_the_id(), 'practice_area_list4', true); ?>
            <?php if (!empty($PracticeAreaList)): ?>
                <div class="col-xs-12 practice-law-list">
                    <div class="row">
                        <?php $count = count($PracticeAreaList); ?>
                        <?php
                        $i = 1;
                        foreach ($PracticeAreaList as $PracticeArea):
                            ?>
                            <div class="col-xs-12 col-sm-3 col-md-3 family-law <?php echo ($count == $i) ? 'trust' : 'family-law-1'; ?> ">
                                <?php if (!empty($PracticeArea['title'])): ?>
                                    <h3 class="h3"><?php echo esc_html($PracticeArea['title']); ?></h3>
                                <?php endif; ?>
                                <div class="group-container clearfix">
                                    <?php if (!empty($PracticeArea['practice_area_image4'])): ?>
                                        <div class="family-group">
                                            <i class="family-law-svg"> <img src="<?php echo esc_url($PracticeArea['practice_area_image4']); ?>" alt=""  title="" class="svg"/> </i>
                                        </div>
                                    <?php endif; ?>
                                    <div class="left-group-text">
                                        <?php if (!empty($PracticeArea['practice_area_description4'])): ?>
                                            <p><?php echo esc_html($PracticeArea['practice_area_description4']); ?></p>
                                        <?php endif; ?>
                                        <?php if (!empty($PracticeArea['practice_area_link4'])): ?>
                                            <a href="<?php echo get_permalink($PracticeArea['practice_area_link4']); ?>" class="more-btn"><?php _e('Know More', 'attorney'); ?><i class="fa fa-chevron-right"></i></a>
                                            <?php endif; ?> 
                                    </div>
                                </div>

                            </div>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </div>

                </div>
            <?php endif; ?>
        </div>

    </div>

</section>
