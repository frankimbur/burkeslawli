<?php
/**
 * Attorney -  Home One About Us
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="about-us animate-effect">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 section-header">
                <?php $AboutSectionTitle = get_post_meta(get_the_id(), 'about_section_title', true); ?>
                <?php $AboutSectionTitleDescription = get_post_meta(get_the_id(), 'about_section_title_description', true); ?>
                <?php if (!empty($AboutSectionTitle)): ?>
                    <h2><?php echo esc_html($AboutSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($AboutSectionTitleDescription)): ?>
                    <span class="practice-desp"><?php echo esc_html($AboutSectionTitleDescription); ?></span>
                <?php endif; ?>
            </div>

        </div>
    </div>

    <div role="tabpanel">
        <div class="tabing-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php echo get_post_meta(get_the_id(), 'about_tab_one_label', true); ?></a>
                            </li>
                            <li role="presentation">
                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?php echo get_post_meta(get_the_id(), 'about_tab_two_label', true); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="row">
                                <div class="col-xs-12 col-sm-5 col-md-5 block-box ">
                                    <?php $QualifiedAttorneysTitle = get_post_meta(get_the_id(), 'qualified_attorneys_title', true); ?>
                                    <?php $QualifiedAttorneys = get_post_meta(get_the_id(), 'qualified_attorneys', true); ?>
                                    <?php if (!empty($QualifiedAttorneysTitle)): ?>
                                        <h3 class="h3"><?php echo esc_html($QualifiedAttorneysTitle); ?></h3>
                                    <?php endif; ?>
                                    <?php if (!empty($QualifiedAttorneys)): ?>
                                        <p><?php echo esc_html($QualifiedAttorneys); ?></p>
                                    <?php endif; ?>
                                    <?php $LeftSectionHistories = get_post_meta(get_the_id(), 'left_section_histories', true); ?>
                                    <?php foreach ($LeftSectionHistories as $history): ?>
                                        <div class="moment  zoom">
                                            <?php $datetime = new DateTime(get_post_meta($history, 'datetime', true)); ?>
                                            <span><?php echo $datetime->format('F Y') ?></span>
                                            <h2><?php echo get_the_title($history); ?></h2>
                                            <?php
                                            $thumbnail_id = get_post_thumbnail_id($history);
                                            $thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));
                                            if ($thumbnail_image && isset($thumbnail_image[0])) {
                                                ?>
                                                <figure class="">
                                                    <img src="<?php echo wp_get_attachment_url($thumbnail_id); ?>" alt="" title=""/>
                                                </figure>
                                                <p><?php echo $thumbnail_image[0]->post_excerpt; ?></p>
                                            <?php }
                                            ?>
                                            <a class="more-btn" href="<?php echo get_permalink($history); ?>"><?php _e('Know More', 'attorney'); ?> <i class="fa fa-chevron-right"></i></a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>

                                <div class="seprater col-xs-12 col-sm-2 col-md-2 ">
                                    <span><?php _e('present', 'attorney'); ?></span>
                                    <span class="small-circle"></span>
                                    <span class="large-circle"></span>
                                    <a href="javascript: void(0);" class="arrow-down"> <i class="fa fa-chevron-down"></i> <i class="fa fa-chevron-down"></i> </a>
                                </div>

                                <div class="col-xs-12 col-sm-5 col-md-5 attroney-base ">
                                    <?php $LongLineAttorneysTitle = get_post_meta(get_the_id(), 'long_line_of_attorneys_title', true); ?>
                                    <?php $LongLineAttorneys = get_post_meta(get_the_id(), 'long_line_of_attorneys', true); ?>
                                    <?php if (!empty($LongLineAttorneysTitle)): ?>
                                        <h3 class="h3"> a long line of attorneys </h3>
                                    <?php endif; ?>
                                    <?php if (!empty($LongLineAttorneys)): ?>
                                        <p><?php echo esc_html($LongLineAttorneys); ?></p>
                                    <?php endif; ?>

                                    <?php $RightSectionHistories = get_post_meta(get_the_id(), 'right_section_histories', true); ?>
                                    <?php foreach ($RightSectionHistories as $history): ?>
                                        <div class="moment moment-right zoom">
                                            <?php $datetime = new DateTime(get_post_meta($history, 'datetime', true)); ?>
                                            <span><?php echo esc_html($datetime->format('F Y')) ?></span>
                                            <h2><?php echo get_the_title($history); ?></h2>
                                            <?php
                                            $thumbnail_id = get_post_thumbnail_id($history);
                                            $thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));
                                            if ($thumbnail_image && isset($thumbnail_image[0])) {
                                                ?>
                                                <figure class="">
                                                    <img src="<?php echo wp_get_attachment_url($thumbnail_id); ?>" alt="" title=""/>
                                                </figure>
                                                <p><?php echo $thumbnail_image[0]->post_excerpt; ?></p>
                                            <?php }
                                            ?>
                                            <a class="more-btn" href="<?php echo get_permalink($history); ?>"><?php _e('Know More', 'attorney'); ?> <i class="fa fa-chevron-right"></i></a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="row">
                                <?php $attorney = new WP_Query(array('post_type' => 'attorney', 'paged' => $paged, 'posts_per_page' => 4)); ?>
                                <?php if ($attorney->have_posts()): ?>
                                    <?php while ($attorney->have_posts()): $attorney->the_post(); ?>
                                        <div class="col-xs-12 col-sm-6 col-md-3 attorney-listing  zoom">
                                            <?php $imageUrl = wp_get_attachment_url(get_post_thumbnail_id()) ?>
                                            <?php if (!empty($imageUrl)): ?>
                                                <figure> 
                                                    <img src="<?php echo attorney_resize($imageUrl, '262', '262'); ?>" alt="" title="" />
                                                </figure>
                                            <?php endif; ?>
                                            <h2 class="at-h2"><?php the_title(); ?><span><?php echo get_post_meta(get_the_id(), 'attorney_designation', 'true'); ?></span></h2>
                                            <?php //the_excerpt(); ?>
                                            <p><?php echo wp_trim_words( get_the_excerpt(), 15, '' ); ?></p>
                                            <a class="more-btn" href="<?php the_permalink(); ?>"><?php _e('Know More', 'attorney'); ?> <i class="fa fa-chevron-right"></i></a>
                                            <ul class="media-listing">
                                                <?php $twitter = get_post_meta(get_the_id(), 'attorney_twitter', 'true'); ?>
                                                <?php if (!empty($twitter)): ?>
                                                    <li>
                                                        <a href="<?php echo esc_url($twitter); ?>" target="_blank" class="fa fa-twitter">&nbsp;</a>
                                                    </li>
                                                <?php endif; ?>
                                                <?php $facebook = get_post_meta(get_the_id(), 'attorney_facebook', 'true'); ?>
                                                <?php if (!empty($facebook)): ?>
                                                    <li>
                                                        <a href="<?php echo esc_url($facebook); ?>" target="_blank" class="fa fa-facebook">&nbsp;</a>
                                                    </li>
                                                <?php endif; ?>
                                                <?php $linkedin = get_post_meta(get_the_id(), 'attorney_linkedin', 'true'); ?>
                                                <?php if (!empty($linkedin)): ?>
                                                    <li>
                                                        <a href="<?php echo esc_url($linkedin); ?>" target="_blank" class="fa fa-linkedin-square">&nbsp;</a>
                                                    </li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                                <?php wp_reset_postdata(); ?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</section>
