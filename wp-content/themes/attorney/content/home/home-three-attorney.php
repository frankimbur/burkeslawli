<?php
/**
 * Attorney -  Home Three Attorney *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="attorney anim-section">
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="heading-label">
                    <a href="javascript: void(0);" class="icon-civil"> <i class="civilean-law-svg"> <img src="<?php echo (ot_get_option('civil_litigation_image'))? ot_get_option('civil_litigation_image'): get_template_directory_uri().'/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i> </a>
                    <?php $AttorneySectionTitle = get_post_meta(get_the_id(), 'attorney_section_title3', true); ?>
                    <?php $AttorneySectionTitleDescription = get_post_meta(get_the_id(), 'attorney_section_title_description3', true); ?>
                    <?php if (!empty($AttorneySectionTitle)): ?>
                        <h2><?php echo esc_html($AttorneySectionTitle); ?></h2>
                    <?php endif; ?>
                    <?php if (!empty($AttorneySectionTitleDescription)): ?>
                        <span class="about-tag"><?php echo esc_html($AttorneySectionTitleDescription); ?></span>
                    <?php endif; ?>
                </div>

                <div id="owl-demo1" class="clearfix">
                    <?php $attorney = new WP_Query(array('post_type' => 'attorney', 'posts_per_page' => get_option('posts_per_page'))); ?>
                        <?php if ($attorney->have_posts()): ?>
                            <?php while ($attorney->have_posts()): $attorney->the_post(); ?>
                    <div class="attorney-listing zoom">
                        <?php $imageUrl = wp_get_attachment_url(get_post_thumbnail_id()) ?>
                        <?php if (!empty($imageUrl)): ?>
                            <figure> 
                                <img src="<?php echo attorney_resize($imageUrl, '263', '262'); ?>" alt="" title="" />
                            </figure>
                        <?php endif; ?>
                        <h3><?php the_title(); ?><span  class="status"><?php echo get_post_meta(get_the_id(), 'attorney_designation', 'true'); ?></span></h3>
                        <p><?php echo wp_trim_words( get_the_content(), 35, $more = '' ); ?></p>

                        <a href="<?php the_permalink(); ?>" class="arrow-more"> <i class="fa fa-angle-right"> &nbsp;</i></a>

                    </div>
                            <?php endwhile; ?>
                    <?php endif; ?>
                <?php wp_reset_postdata(); ?>
                </div>
            </div>


            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="news-update-block">
                    <div class="heading-label">
                        <a href="javascript: void(0);" class="icon-civil"> <i class="civilean-law-svg"> <img src="<?php echo get_template_directory_uri(); ?>/assets/svg/civil-litigation.svg" alt=""  title="" class="svg"/> </i> </a>
                        <?php $newsSectionTitle = get_post_meta(get_the_id(), 'news_section_title3', true); ?>
                        <?php $newsSectionBackground = get_post_meta(get_the_id(), 'news_slider_background3', true); ?>
                        <?php $newsSectionStyle = ($newsSectionBackground)? 'style="background-image: url('.$newsSectionBackground.');"':''; ?>
                        <?php if(!empty($newsSectionTitle)): ?>
                        <h2><?php echo esc_html($newsSectionTitle); ?></h2>
                        <?php endif; ?>
                        
                        <?php $newsSectionTitledescription = get_post_meta(get_the_id(), 'news_section_title_description3', true); ?>
                        <?php if(!empty($newsSectionTitledescription)): ?>
                        <span class="about-tag"><?php echo esc_html($newsSectionTitledescription); ?></span>
                        <?php endif; ?>
                    </div>

                    <div id="owl-demo2" class="clearfix">
                        <?php
                        $PostArray = array(); 
                        $i = 0;
                        $j = 0;
                        $Blogposts = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 6)); 
                        if ($Blogposts->have_posts()): 
                            while ($Blogposts->have_posts()) : $Blogposts->the_post(); 
                              $PostArray[$i][$j] = get_the_id();
                              if($j == 1){
                                  $j = 0;
                                  $i++;
                              }
                              $j++;
                            endwhile;
                        endif; 
                        ?>
                        <?php wp_reset_postdata(); ?>
                        <?php foreach ($PostArray as $slide): ?>
                            <div <?php echo ($newsSectionStyle)? $newsSectionStyle: ''; ?> class="news-upadte-content">
                                <?php foreach ($slide as $postId): ?>
                                    <div class="news-listing-detail clearfix">
                                        <div class="left-block">
                                            
                                            <span><span><?php echo get_the_date('j', $postId); ?></span><?php echo get_the_date('M Y', $postId); ?></span>

                                        </div>
                                        <div class="right-block">
                                            <h5><?php echo get_the_title($postId); ?></h5>
                                            <p><?php echo wp_trim_words( get_post_field('post_content',$postId), 25, $more = '' ); ?></p>
                                            <a href="<?php echo get_the_permalink($postId); ?>" class="arrow-more"> <i class="fa fa-angle-right"> &nbsp;</i></a>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endforeach; ?>
                        
                    </div>
                </div>	
            </div>

        </div>
    </div>

</section>
