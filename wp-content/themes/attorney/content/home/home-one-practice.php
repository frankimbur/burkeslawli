<?php
/**
 * Attorney -  Home One Practice
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="practice-area animate-effect">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 section-header">
                <?php $PracticeSectionTitle = get_post_meta(get_the_id(), 'practice_section_title', true); ?>
                <?php $PracticeSectionTitleDescription = get_post_meta(get_the_id(), 'practice_section_title_description', true); ?>
                <?php if (!empty($PracticeSectionTitle)): ?>
                    <h2><?php echo esc_html($PracticeSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($PracticeSectionTitleDescription)): ?>
                    <span class="practice-desp"><?php echo esc_html($PracticeSectionTitleDescription); ?></span>
                <?php endif; ?>
            </div>
            <?php $PracticeAreaList = get_post_meta(get_the_id(), 'h1_practice_area_list', true); ?>
            <?php if (!empty($PracticeAreaList)): ?>
                <div class="col-xs-12 practice-law-list">
                    <div class="row">
                        <?php foreach ($PracticeAreaList as $PracticeArea): ?>
                            <div class="col-xs-12 col-sm-3 col-md-3 family-law">
                                <?php if (!empty($PracticeArea['title'])): ?>
                                    <h3 class="h3"><?php echo esc_html($PracticeArea['title']); ?></h3>
                                <?php endif; ?>
                                <?php if (!empty($PracticeArea['h1_practice_area_image'])): ?>
                                    <div class="family-group">
                                        <i class="family-law-svg"> <img src="<?php echo esc_url($PracticeArea['h1_practice_area_image']); ?>" alt=""  title="" class="svg"/> </i>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($PracticeArea['h1_practice_area_description'])): ?>
                                    <p><?php echo esc_html($PracticeArea['h1_practice_area_description']); ?></p>
                                <?php endif; ?>

                                <?php if (!empty($PracticeArea['h1_practice_area_link'])): ?>
                                    <a href="<?php echo get_permalink($PracticeArea['h1_practice_area_link']); ?>" class="more-btn"><?php _e('Know More', 'attorney'); ?><i class="fa fa-chevron-right"></i></a>
                                    <?php endif; ?> 
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            <?php endif; ?>

        </div>

    </div>

</section>
