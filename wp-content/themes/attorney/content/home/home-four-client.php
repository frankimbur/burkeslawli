<?php
/**
 * Attorney -  Home Four client
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="client anim-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                <?php $ClientSectionTitle = get_post_meta(get_the_id(), 'client_section_title4', true); ?>
                <?php $ClientsSectionTitleDescription = get_post_meta(get_the_id(), 'clients_section_title_description4', true); ?>
                <?php if (!empty($ClientSectionTitle)): ?>
                    <h2><?php echo esc_html($ClientSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($ClientsSectionTitleDescription)): ?>
                    <span class="practice-desp"><?php echo esc_html($ClientsSectionTitleDescription); ?></span>
                <?php endif; ?>
                    
                    <?php $attorney_client = new WP_Query(array('post_type' => 'attorney_client', 'posts_per_page' => '5')); ?>
                <?php if ($attorney_client->have_posts()): ?>
                    <ul class="client-listing clearfix">
                        <?php while ($attorney_client->have_posts()): $attorney_client->the_post(); ?>
                            <?php if (has_post_thumbnail()) { ?>
                                <li><?php the_post_thumbnail(); ?></li>
                            <?php } ?>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>

                </div>

            </div>

        </div>

    </section>
