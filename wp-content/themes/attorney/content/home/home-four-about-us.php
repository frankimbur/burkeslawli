<?php
/**
 * Attorney -  Home Four About Us
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="about-us anim-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 practice-header">
                <?php $AboutSectionTitle = get_post_meta(get_the_id(), 'about_section_title4', true); ?>
                <?php $AboutSectionTitleDescription = get_post_meta(get_the_id(), 'about_section_title_description4', true); ?>
                <?php if (!empty($AboutSectionTitle)): ?>
                    <h2><?php echo esc_html($AboutSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($AboutSectionTitleDescription)): ?>
                    <span class="practice-desp"><?php echo esc_html($AboutSectionTitleDescription); ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div role="tabpanel">
        <div class="tabing-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php _e('History', 'attorney'); ?></a>
                            </li>
                            <li role="presentation">
                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?php _e('Attorneys', 'attorney'); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <?php $QualifiedAttorneysTitle = get_post_meta(get_the_id(), 'qualified_attorneys_title4', true); ?>
                                    <?php $QualifiedAttorneys = get_post_meta(get_the_id(), 'qualified_attorneys4', true); ?>
                                    <?php if (!empty($QualifiedAttorneysTitle)): ?>
                                        <h3 class="h3"><?php echo esc_html($QualifiedAttorneysTitle); ?></h3>
                                    <?php endif; ?>
                                    <?php if (!empty($QualifiedAttorneys)): ?>
                                        <p><?php print($QualifiedAttorneys); ?></p>
                                    <?php endif; ?>
                                </div>
                                <div class="col-xs-12 col-sm-6 attroney-base">
                                    <?php $LongLineAttorneysTitle = get_post_meta(get_the_id(), 'long_line_of_attorneys_title4', true); ?>
                                    <?php $LongLineAttorneys = get_post_meta(get_the_id(), 'long_line_of_attorneys4', true); ?>
                                    <?php if (!empty($LongLineAttorneysTitle)): ?>
                                        <h3 class="h3"><?php echo esc_html($LongLineAttorneysTitle); ?></h3>
                                    <?php endif; ?>
                                    <?php if (!empty($LongLineAttorneys)): ?>
                                        <p><?php echo esc_html($LongLineAttorneys); ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="row">
                                <?php $SectionHistories = get_post_meta(get_the_id(), 'section_histories4', true); ?>
                                <?php if(!empty($SectionHistories)): ?>
                                    <?php foreach ($SectionHistories as $history): ?>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="moment zoom">
                                        <?php $datetime = new DateTime(get_post_meta($history, 'datetime', true)); ?>
                                            <span><?php print($datetime->format('F Y')) ?></span>
                                            <h2><?php echo get_the_title($history); ?></h2>                                     
                                            <?php
                                            $thumbnail_id = get_post_thumbnail_id($history);
                                            $thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));
                                            if ($thumbnail_image && isset($thumbnail_image[0])) {
                                                ?>
                                                <figure class="">
                                                    <img src="<?php echo wp_get_attachment_url($thumbnail_id); ?>" alt="" title=""/>
                                                </figure>
                                                <p><?php print($thumbnail_image[0]->post_excerpt); ?></p>
                                            <?php }
                                            ?>
                                            <a class="more-btn" href="<?php echo esc_url(get_permalink($history)); ?>"><?php _e('Know More', 'attorney'); ?> <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="row">
                                <?php $attorney = new WP_Query(array('post_type' => 'attorney', 'paged' => $paged, 'posts_per_page' => 4)); ?>
                                <?php if ($attorney->have_posts()): ?>
                                    <?php while ($attorney->have_posts()): $attorney->the_post(); ?>
                                <div class="col-xs-12 col-sm-3 col-md-3 attorney-listing">
                                    <?php $imageUrl = wp_get_attachment_url(get_post_thumbnail_id()) ?>
                                            <?php if (!empty($imageUrl)): ?>
                                                <figure> 
                                                    <img src="<?php echo esc_url(attorney_resize($imageUrl, '263', '262')); ?>" alt="" title="" />
                                                </figure>
                                            <?php endif; ?>
                                    <h2 class="at-h2"><?php the_title(); ?><span><?php echo get_post_meta(get_the_id(), 'attorney_designation', 'true'); ?></span></h2>
                                    <?php $AttorneyContactNo = get_post_meta(get_the_id(), 'attorney_contact_no', true); ?>
                                    <?php if(!empty($AttorneyContactNo)): ?>
                                    <a href="tel:<?php echo esc_attr($AttorneyContactNo); ?>" class="contact-number clearfix"><i class="svg-shape tab-two-left-icon icon-call"><?php echo attorneySvgIcon('phone'); ?></i><span><?php echo esc_html($AttorneyContactNo); ?></span> </a>
                                    <?php endif; ?>
                                    <?php $AttorneyEmail = get_post_meta(get_the_id(), 'attorney_email_id', true); ?>
                                    <?php if(!empty($AttorneyEmail)): ?>
                                    <a href="mailto: <?php echo esc_html($AttorneyEmail); ?>" class="contact-number mail-info clearfix"><i class="svg-shape tab-two-left-icon icon-enevlope"><?php echo attorneySvgIcon('enevlope'); ?></i><span><?php echo esc_html($AttorneyEmail); ?></span> </a>
                                    <?php endif; ?>
                                    <ul class="media-listing clearfix">
                                        <?php $twitter = get_post_meta(get_the_id(), 'attorney_twitter', 'true'); ?>
                                        <?php if (!empty($twitter)): ?>
                                            <li>
                                                <a href="<?php echo esc_url($twitter); ?>" target="_blank" class="fa fa-twitter">&nbsp;</a>
                                            </li>
                                        <?php endif; ?>
                                        <?php $facebook = get_post_meta(get_the_id(), 'attorney_facebook', 'true'); ?>
                                        <?php if (!empty($facebook)): ?>
                                            <li>
                                                <a href="<?php echo esc_url($facebook); ?>" target="_blank" class="fa fa-facebook">&nbsp;</a>
                                            </li>
                                        <?php endif; ?>
                                        <?php $linkedin = get_post_meta(get_the_id(), 'attorney_linkedin', 'true'); ?>
                                        <?php if (!empty($linkedin)): ?>
                                            <li>
                                                <a href="<?php echo esc_url($linkedin); ?>" target="_blank" class="fa fa-linkedin-square">&nbsp;</a>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                    <a class="more-btn" href="<?php the_permalink(); ?>"><?php _e('Know More', 'attorney'); ?> <i class="fa fa-chevron-right"></i></a>
                                </div>
                                 <?php endwhile; ?>
                                <?php endif; ?>
                                <?php wp_reset_postdata(); ?>                                                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
