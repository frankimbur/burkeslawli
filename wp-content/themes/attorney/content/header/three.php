<?php
/**
 * Attorney - Header Two
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<!--header Section Start Here -->
<header class="normal header-type-three" id="header">
    <div class="navigation-header">
        <div class="container">
            <div class="row">
                <a class="col-xs-6 col-sm-2 logo" href="<?php echo esc_url(home_url('/')); ?>">
                    <?php $logo = (ot_get_option('attorney_logo')) ? ot_get_option('attorney_logo') : get_stylesheet_directory_uri() . "/assets/img/logo.png";
                     if(!empty($logo)): ?>
                        <img title="<?php echo get_bloginfo('name'); ?>" alt="<?php echo get_bloginfo('name'); ?>" src="<?php echo esc_url($logo); ?>"></a>
                    <?php endif; ?>
                </a>
                <div class="col-xs-12 col-sm-10 col-md-9 col-md-offset-1 navigation">
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only"><?php echo esc_html('Toggle navigation', 'attorney'); ?></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                            <?php do_action('attorney_nav_menu'); ?>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
<!--header Section End Here -->
<?php 

                        
