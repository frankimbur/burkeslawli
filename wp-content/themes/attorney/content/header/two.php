<?php
/**
 * Attorney - Header Two
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>			
<!--header Section Start Here -->
<header id="header" class="normal header-type-two">
    <div class="navigation-header">
        <div class="container">
            <div class="row">
                <a href="<?php echo esc_url(home_url('/')); ?>" class="col-xs-6 col-sm-2 col-md-2 logo">
                    <?php $logo = (ot_get_option('attorney_logo')) ? ot_get_option('attorney_logo') : get_stylesheet_directory_uri() . "/assets/img/logo.png";
                     if(!empty($logo)): ?>
                        <img title="<?php echo get_bloginfo('name'); ?>" alt="<?php echo get_bloginfo('name'); ?>" src="<?php echo esc_url($logo); ?>"></a>
                    <?php endif; ?>                
                </a>
                <div class="col-xs-12 col-sm-10 col-md-8 navigation">
                    <nav class="navbar navbar-default">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only"><?php echo esc_html('Toggle navigation', 'attorney'); ?></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <?php do_action('attorney_nav_menu'); ?>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div>
                <?php $contactNo = ot_get_option('attorney_contact_no'); if(!empty($contactNo)): ?>
                <a href="tel:<?php echo preg_replace('/[^\d]/', '', $contactNo); ?>" class="col-xs-12 col-md-2 contact-number clearfix"><i class="fa fa-phone-square"> </i><span><?php echo esc_html($contactNo); ?></span> </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>
<?php 
