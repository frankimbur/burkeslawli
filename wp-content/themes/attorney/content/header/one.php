<?php
/**
 * Attorney - Core files
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<!--header Section Start Here -->
<header class="normal header-type-one" id="header">
    <div class="primary-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-2 media-header">
                    <?php do_action("attorney_header_social"); ?>
                </div>

                <div class="col-xs-12 col-sm-8 col-md-10">
                    <div class="primary-right-block">
                        <div class="consultation">
                            <?php $contact = ot_get_option('attorney_consultation'); ?> 
                            <?php if(!empty($contact)): ?>
                            <a href="<?php echo esc_attr($contact); ?>">
                                    <i class="svg-shape icon-enevlope"> 
                                        <?php print(attorneySvgIcon('enevlope')); ?>
                                    </i><span><?php echo ot_get_option('attorney_consultation_text'); ?>&nbsp;<i class="fa fa-chevron-right"></i></span> </a>
                                    <?php endif; ?>

                        </div>
                        <button class="search"><i class="fa fa-search"></i></button>
                            <?php do_action('attorney_header_search'); ?>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <div class="navigation-header">
        <div class="container">
            <div class="row">
                <a class="col-xs-12 col-sm-2 col-md-2 logo" href="<?php echo esc_url(home_url('/')); ?>">
                    <?php $logo = (ot_get_option('attorney_logo')) ? ot_get_option('attorney_logo') : get_stylesheet_directory_uri() . "/assets/img/logo.png";
                    if(!empty($logo)): ?>
                    <img title="<?php echo get_bloginfo('name'); ?>" alt="<?php echo get_bloginfo('name'); ?>" src="<?php echo esc_url($logo); ?>"></a>
                    <?php endif; ?>
                <div class="col-xs-12 col-sm-10 col-md-8 navigation">
                    <nav class="navbar navbar-default">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button data-target="#bs-example-navbar-collapse-2" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only"><?php echo esc_html('Toggle navigation', 'attorney'); ?></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div id="bs-example-navbar-collapse-2" class="collapse navbar-collapse">
                            <?php do_action('attorney_nav_menu'); ?>
                        </div><!-- /.navbar-collapse -->
                    </nav>

                </div>
                <?php $contactNo = ot_get_option('attorney_contact_no'); ?>
                <?php if(!empty($contactNo)): ?>
                <a class="col-xs-12 col-md-2 contact-number clearfix" href="tel:<?php echo preg_replace('/[^\d]/', '', $contactNo); ?>">
                    <i class="icon-call svg-shape">
                        <?php print(attorneySvgIcon('phone')); ?>
                    </i>
                    <span><?php echo esc_html($contactNo); ?></span>
                </a>
                <?php endif; ?>
                
            </div>

        </div>

    </div>

</header>
<!--header Section End Here -->
