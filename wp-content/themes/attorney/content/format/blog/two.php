<div id="post-<?php the_ID(); ?>" <?php post_class('blog-listing zoom picture-item clearfix'); ?>>
    <div class="blog-listing-pics">
        <?php if (has_post_thumbnail() && !post_password_required() && !is_attachment()) : ?>
        <figure>
            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('attorney_blog_two'); ?></a>
        </figure>
        <?php endif; ?>
    </div>
    <div class="blog-information">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <div class="blog-admin-info underline-label">
            <span ><?php printf(__("- By : %s", "attorney"), "<span> " . '<a href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '">' . esc_html(get_the_author()) . '</a>' . " </span>"); ?>
        </div>

        <p class="about-us-paragraph">
            <?php echo wp_trim_words( get_the_excerpt(), $num_words = 40, $more = '' ); ?>
            <a href="<?php the_permalink(); ?>"><i class="fa fa-chevron-right"></i></a>
        </p>
        <ul class="blog-comment">
            <li>
                <a href="<?php comments_link(); ?>" class="rightline"> <i class="fa fa-comments-o"></i> <?php _e('Comment', 'attorney'); ?> </a>
            </li>
            <li class="heart-status">
                <a href="#" data-id="<?php echo get_the_id(); ?>" class="rightline attorneyfblike"> <i class="fa fa-eye"></i> <?php _e('like', 'attorney'); ?> </a>
            </li>
            <li class="share-box">
                <a href="#"> <i class="fa fa-share-alt"></i> <?php _e('Share', 'attorney'); ?> </a>
                <div class="share-buttons">
                    <span class='st__large' displayText=''></span>
                    <span class='st_facebook_large' displayText='Facebook'></span>
                    <span class='st_twitter_large' displayText='Tweet'></span>
                    <span class='st_googleplus_large' displayText='Google +'></span>
                </div>
            </li>
        </ul>
    </div>
</div>
