<div id="post-<?php the_ID(); ?>" <?php post_class('blog-listing blog-one-page clearfix'); ?>>
    <?php if (has_post_thumbnail() && !post_password_required() && !is_attachment()) : ?>
        <div class="blog-listing-pics animate-effect">
            <figure>
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            </figure>
        </div>
    <?php endif; ?>
    <div class="blog-information animate-effect">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php if (!is_search()): ?>
            <div class="blog-admin-info underline-label">
                <?php do_action("attorney_format_blog_meta"); ?>
            </div>
        <?php endif; ?>
        <?php the_content(); ?>
        <?php wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . esc_html__('Pages:', 'attorney') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>
    </div>
</div>