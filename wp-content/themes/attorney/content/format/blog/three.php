<div id="post-<?php the_ID(); ?>" <?php post_class('blog-listing clearfix'); ?>>
    <?php if (has_post_thumbnail() && !post_password_required() && !is_attachment()) : ?>
    <div class="blog-listing-pics zoom">
        <div class="blog-listing-pics animate-effect">
            <figure>
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('attorney_blog_three'); ?></a>
            </figure>
        </div>
    
        <ul class="blog-comment clear-fix">
            <li>
                <?php $comments_count = wp_count_comments(); ?>
                <a href="<?php echo get_comments_link( get_the_id() ); ?>"> <i class="fa fa-comments-o"></i> : <?php echo $comments_count->approved; ?> </a>
            </li>
            <?php $ViewsCount = get_post_meta(get_the_id(), 'wpb_post_views_count', true); ?>
            <?php if(!empty($ViewsCount)): ?>
            <li class="heart-status">
                <a href="<?php the_permalink(); ?>"> <i class="fa fa-heart-o"></i> : <?php echo $ViewsCount; ?></a>
            </li>
            <?php endif; ?>
            <li class="share-box">
                <a href="#"> <i class="fa fa-share-alt"></i> : <?php do_action('totalShareCount', get_permalink()); ?> </a>
            </li>
        </ul>

    </div>
    <?php endif; ?>
    
    <div class="blog-information">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <div class="blog-admin-info underline-label">
            <span ><?php printf(__("- By : %s", "attorney"), "<span> " . '<a href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '">' . esc_html(get_the_author()) . '</a>' . " </span>"); ?>
             <span><a href="<?php echo get_month_link(get_the_time('Y'), get_the_time('m')); ?>"><?php printf(__("Date : %s", "attorney"), '<span>' . get_the_date("d-M-y") . '</span>'); ?></a></span>

        </div>
        <?php the_content(); ?>
    </div>

</div>