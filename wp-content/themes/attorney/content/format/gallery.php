<?php
/**
 * Attorney - gallery
 * Used for both single and index/archive/search.
 * 
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.content.format
 * @since attorney 1.0
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('blog-listing clearfix'); ?>>
    <?php if (has_post_thumbnail() && !post_password_required() && !is_attachment()) : ?>
        <div class="blog-listing-pics">
            <figure>
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            </figure>
        </div>
    <?php endif; ?>
    <div class="blog-information">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php
        do_action("attorney_format_blog_meta");
        the_content();
        ?>
    </div>
</div>
<?php 