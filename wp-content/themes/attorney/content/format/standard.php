<?php
/**
 * Attorney - standard
 * Used for both single and index/archive/search.
 * 
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.content.format
 * @since attorney 1.0
 */
if (!is_single()) : 
    $template_part = Attorney\Layout\Layout_Option::get_blog();
    include $template_part['template_part'].'.php' ;
else:
    ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class('blog-listing blog-details-page clearfix'); ?>>
        <?php if (has_post_thumbnail() && !post_password_required() && !is_attachment()) : ?>
            <div class="blog-listing-pics">
                <figure>
                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                </figure>
                <?php do_action("attorney_format_blog_single_share_meta", get_the_id() ); ?>
            </div>
        <?php endif; ?>
        <div class="blog-information">
            <h3><?php the_title(); ?></h3>
            <?php do_action("attorney_format_blog_meta_single"); 
            the_content();
            ?>
        </div>
        <?php attorney_post_navigation(); ?>
    </div>
    <?php
    get_template_part('content/author', 'info'); 
endif;
