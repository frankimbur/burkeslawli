<?php
/**
 * Attorney - taxonomy post_format
 * The template for displaying Post Format pages
 * 
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<!--content Section Start Here -->
<div id="content">
    <!--blog-content Section Start Here -->
    <section class="blog-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-9">
                    <?php
                    if (have_posts()) :
                        ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( '%s Archives', 'attorney' ), '<span>' . esc_html( get_post_format_string( get_post_format() ) ) . '</span>' ); ?></h1>
			</header><!-- .archive-header -->
                        <?php
                        while (have_posts()) : the_post();
                            $postFormat = (get_post_format()) ? get_post_format() : "standard";
                            get_template_part('content/format/' . $postFormat);
                        endwhile;
                        attorney_pagenavi();
                    else :
                        get_template_part('content/none');
                    endif;
                    ?>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-3">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
    <!--blog-content Section End Here -->
</div>
<!--content Section End Here -->
<?php
get_footer();