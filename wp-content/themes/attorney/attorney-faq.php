<?php
/**
 * Template Name: Attorney FAQ
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); ?>
        <?php do_action('attorney_format_blog_banner', get_the_id()); ?>
<!--content Section Start Here -->
<div id="content" class="attorney-detail-page faq-page">
    <!--faq Section End Here -->
    <section class="faq-listing">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                   <?php $PageTitle = get_post_meta(get_the_id(),'page_title', true); ?>
                    <?php if(!empty($PageTitle)): ?>
                    <h2><?php echo esc_html($PageTitle); ?></h2>
                    <?php endif; ?>
                    <?php $TitleDescription = get_post_meta(get_the_id(),'title_description', true); ?>
                    <?php if(!empty($TitleDescription)): ?>
                        <span class="heading-details"><?php echo esc_html($TitleDescription); ?></span>
                    <?php endif; ?>
                </div>
            </div> 
            <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
                <?php $faq = new WP_Query(array('post_type' => 'faq', 'posts_per_page' => get_option('posts_per_page'), 'paged' => $paged)); ?>
                <?php if ($faq->have_posts()): ?>
                    <div class="row faq-group">
                    <?php while ($faq->have_posts()): $faq->the_post(); ?>
                        <div class="col-xs-12 col-sm-6 faq-listing-list">
                            <span class="question-label "><?php _e('Q', 'attorney'); ?></span>
                            <div class="question-block ">
                                <h3><?php the_title(); ?></h3>
                                <?php the_content(); ?>
                            </div>
                        </div>
            <?php endwhile; ?> 
                </div>
            <?php attorney_pagenavi($faq);
            else :
                get_template_part('content/none');
            endif;
            ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </section>

    <!--contact-form-form Section End Here -->

    <!-- consulation Section Start Here -->
    
    <?php $newsletter_shortcode = get_post_meta(get_the_id(),'newsletter_shortcode', true); ?>
    <?php if(!empty($newsletter_shortcode)): ?>
    <?php $newsletter_background = get_post_meta(get_the_id(),'ask_question_background', true); ?>
    <?php $newsletter_style = ($newsletter_background)? 'style="background-image: url('.$newsletter_background.');"':''; ?>
    <section <?php echo ($newsletter_style)? $newsletter_style: ''; ?> class="consulation faq-comment">
        <div class="container">
            <div class="row">
                <?php echo do_shortcode($newsletter_shortcode); ?> 
            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- consulation Section Start Here -->
</div>
    <?php endwhile; ?>
<?php endif; ?>
<!--content Section ends Here -->
<?php
get_footer(); 
