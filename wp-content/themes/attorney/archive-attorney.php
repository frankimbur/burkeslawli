<?php
/**
 * Attorney - archive page
 * The template for displaying archive pages
 * 
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<!--content Section Start Here -->
<div id="content">
    <!--attorney-detail-container Section Start Here -->
    <section class="attorney-listing">
        <div class="container">
            <div class="row">
                <?php if (have_posts()): ?>
                    <?php while (have_posts()): the_post(); ?>
                        <div class="col-xs-12 col-sm-6 col-md-4 attorney-listing-box zoom">
                            <figure> 
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                            </figure>
                            <div class="attorney-information clearfix">
                                <div class="attorney-name-label">
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <span class="label-text underline-label"><?php echo get_post_meta( get_the_id(), 'attorney_designation', 'true' ); ?></span>

                                </div>
                                <ul class="media-listing">
                                    <?php $twitter = get_post_meta( get_the_id(), 'attorney_twitter', 'true' ); ?>
                                    <?php if(!empty($twitter)): ?>
                                    <li>
                                        <a href="<?php echo esc_url($twitter); ?>" target="_blank" class="fa fa-twitter">&nbsp;</a>
                                    </li>
                                    <?php endif; ?>
                                    <?php $facebook = get_post_meta( get_the_id(), 'attorney_facebook', 'true' ); ?>
                                    <?php if(!empty($facebook)): ?>
                                    <li>
                                        <a href="<?php echo esc_url($facebook); ?>" target="_blank" class="fa fa-facebook">&nbsp;</a>
                                    </li>
                                    <?php endif; ?>
                                    <?php $linkedin = get_post_meta( get_the_id(), 'attorney_linkedin', 'true' ); ?>
                                    <?php if(!empty($linkedin)): ?>
                                    <li>
                                        <a href="<?php echo esc_url($linkedin); ?>" target="_blank" class="fa fa-linkedin-square">&nbsp;</a>
                                    </li>
                                    <?php endif; ?>
                                </ul>

                            </div>
                            <?php the_excerpt(); ?>
                             <?php $cunsult_text = get_post_meta( get_the_id(), 'attorney_cunsult_text', 'true' ); 
                            $cunsult = (!empty($cunsult_text))? $cunsult_text : 'cunsult him' ;
                            ?>
                             <a class="consult-btn " href="<?php the_permalink(); ?>"><?php echo $cunsult; ?> <i class="fa fa-chevron-right"></i></a>

                        </div>
                    <?php endwhile; ?>
                    <?php attorney_pagenavi();
                    else :
                        get_template_part('content/none');
                    endif; ?>
                    <?php wp_reset_postdata(); ?>
            </div>

        </div>

    </section>

    <!--attorney-detail-container Section End Here -->

</div>
<!--content Section End Here -->
<?php
get_footer();
