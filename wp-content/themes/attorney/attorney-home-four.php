<?php
/**
 * Template Name: Attorney Home Four
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
if (have_posts()):
    while (have_posts()): the_post();
        ?>
        <!-- Banner Slider Starts -->
        <?php get_template_part('content/home/home', 'four-slider'); ?>
        <!-- Banner Slider Ends -->
        <div id="content"> 
            <!--practice-area Section Start Here -->
            <?php get_template_part('content/home/home', 'four-practice'); ?>
            <!--practice-area Section End Here -->
            <!--about-us Section Start Here -->
            <?php get_template_part('content/home/home', 'four-about-us'); ?>
            <!--about-us Section End  Here -->
            <!--news-testimonial Section Start Here -->
            <?php get_template_part('content/home/home', 'four-news-testimonial'); ?>
            <!--news-testimonial Section End  Here -->

            <!--client Section start  Here -->
            <?php get_template_part('content/home/home', 'four-client'); ?>
            <!--client Section End  Here -->

            <!--client Section End  Here -->
            <?php get_template_part('content/home/home', 'four-hiring'); ?>
            <!--client Section End  Here -->

        </div>
        <?php
    endwhile;
endif;
get_footer();
