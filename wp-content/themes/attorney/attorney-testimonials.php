<?php
/**
 * Template Name: Attorney Testinonials
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); ?>
        <?php do_action('attorney_format_blog_banner', get_the_id()); ?>
<!--content Section Start Here -->
<div id="content">
    <!--happy-clients and testimonial Section start Here -->
    <section class="happy-clients testimonial">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php $PageTitle = get_post_meta(get_the_id(),'page_title', true); ?>
                    <?php if(!empty($PageTitle)): ?>
                    <h2><?php echo esc_html($PageTitle); ?></h2>
                    <?php endif; ?>
                    <?php $TitleDescription = get_post_meta(get_the_id(),'title_description', true); ?>
                    <?php if(!empty($TitleDescription)): ?>
                        <span class="heading-details underline-label"><?php echo esc_html($TitleDescription); ?></span>
                    <?php endif; ?>
                </div>

            </div>

            <div class="row">
                <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
                <?php $testimonial = new WP_Query(array('post_type' => 'testimonial', 'posts_per_page' => get_option('posts_per_page'), 'paged' => $paged)); ?>
                   <?php if($testimonial->have_posts()): ?>
                     <?php while ($testimonial->have_posts()): $testimonial->the_post(); ?>
                         <div class="happy-client-list clearfix">
                        <?php if (has_post_thumbnail()): ?>
                            <figure class="col-xs-12 col-sm-3 col-md-3  animate-effect">
                                <?php the_post_thumbnail(); ?>
                            </figure>
                        <?php endif; ?>
                            <div class="col-xs-12 col-sm-9 col-md-9 happy-client-state animate-effect">
                                <p class="about-us-paragraph"> <?php echo get_the_content(); ?> </p>
                                <span class="taging-client">
                                    <span class="posting-by"><?php _e('By', 'attorney'); ?></span>
                                    <a href="# <?php //the_permalink(); ?>"><?php the_title(); ?></a>
                                </span>
                            </div>
                        </div>
                    <?php endwhile; 
                        attorney_pagenavi($testimonial);
                    else :
                        get_template_part('content/none');
                    endif;
                    wp_reset_postdata();
                    ?>
            </div>
        </div>
    </section>
    <!--happy-clients Section End Here -->
</div>
<!--content Section ends Here -->
    <?php endwhile; ?>
<?php endif; ?>
<?php
get_footer();
