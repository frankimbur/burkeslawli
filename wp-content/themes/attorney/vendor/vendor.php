<?php

/**
 * Attorney - Vendor 
 * 
 * @package attorney
 * @since attorney 1.0.0
 */

attorney_inclusion('vendor/envato_setup/envato_setup.php');
attorney_inclusion('vendor/option-tree.php');
