<?php
/**
 * TGM integration
 *
 * @package wptm.inc.tgm
 * @since wptm 1.0.0
 */
add_action('admin_menu', function() {
    call_user_func("add_submenu" . "_page", 'themes.php', 'Theemon Importer', 'Theemon Importer', 'edit_theme_options', 'theemon-importer', 'theemon_importer');
});
add_action('admin_head', function(){
    ?>
<style>
    a[href="themes.php?page=theemon-importer"] {
	display: none !important;
    }
    </style>
<?php
});
function theemon_importer() {
    if (class_exists('TGM_Plugin_Activation')) {

	    $tgmPluginActivation = new TGM_Plugin_Activation();
	    $tgmPluginActivation->install_plugins_page();
	} else {
	    echo "TGM ERROR";
	}
   
}
 
attorney_inclusion( 'vendor/envato_setup/tgm/tgm-plugin.php' );

/**
 * Register TGM Plugins
 */
function wptm_register_required_plugins() {
    define("WPTM_PLUGIN_DIR", WPTM_DIR.'/data/plugins/');
    
    //lists of plugins
    $plugins = attorney_inclusion( 'vendor/envato_setup/tgm/config.php', false, false, true  );

    $config = array(
        'id' => 'tgmpa', 
        'default_path' => '', 
        'menu' => 'theemon-importer', 
        'parent_slug' => 'admin.php', 
        'capability' => 'edit_theme_options', 
        'has_notices' => true, 
        'dismissable' => true, 
        'dismiss_msg' => '', 
        'is_automatic' => false, 
        'message' => '', 
    );

    tgmpa($plugins, $config);
}
add_action('tgmpa_register', 'wptm_register_required_plugins');
