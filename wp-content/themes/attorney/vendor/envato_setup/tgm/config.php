<?php

/**
 * TGM integration
 * 
 * List of plugins
 *
 * @package wptm.inc.tgm
 * @since wptm 1.0.0
 */
return array(
    array(
        'name' => esc_html__('Attorney Bundle', 'attorney'),
        'slug' => 'attorney-bundle',
       'source' => 'http://theemon.com/plugins-territory/attorney/attorney-bundle.zip',
        'required' => true,
    ),
    array(
        'name' => esc_html__('WPBakery Visual Composer', 'attorney'),
        'slug' => 'js_composer',
        'source' => 'http://theemon.com/plugins-territory/js_composer.zip',
        'required' => true,
    ), array(
        'name' => esc_html__('Revolution Slider', 'attorney'),
        'slug' => 'revslider',
        'source' => 'http://theemon.com/plugins-territory/revslider.zip',
        'required' => true,
    ),
    array(
        'name' => esc_html__('Contact Form 7', 'attorney'),
        'slug' => 'contact-form-7',
        'required' => true,
    ),
    array(
        'name' => esc_html__('Mailchimp', 'attorney'),
        'slug' => 'mailchimp-for-wp',
        'required' => false,
    ),
    array(
        'name' => esc_html__('Easy Rating', 'attorney'),
        'slug' => 'easy-rating',
       'source' => 'http://theemon.com/plugins-territory/attorney/easy-rating.zip',
        'required' => false,
    ),
    array(
        'name' => esc_html__('Social Count Plus', 'attorney'),
        'slug' => 'social-count-plus',
        'required' => false,
    ),
    array(
        'name' => esc_html__('Envato Market', 'attorney'),
        'slug' => 'envato-market',
        'source' => 'http://theemon.com/plugins-territory/envato-market.zip', 
        'required' => false,
    ),
);
