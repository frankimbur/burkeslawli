<?php
/**
 * Template Name: Attorney Home Three
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
if (have_posts()):
    while (have_posts()): the_post();
        ?>
        <!-- Banner Slider Starts -->
        <?php get_template_part('content/home/home', 'three-slider'); ?>
        <!-- Banner Slider Ends -->
        <div id="content">
            <!--about-us Section Start Here -->
            <?php get_template_part('content/home/home', 'three-about-us'); ?>
            <!--about-us Section End  Here -->

            <!--practice-area Section Start Here -->
            <?php get_template_part('content/home/home', 'three-practice'); ?>
            <!--practice-area Section End Here -->

            <!--Attorney Section End Here -->
            <?php get_template_part('content/home/home', 'three-attorney'); ?>
            <!--Attorney Section End Here -->

            <!--testimonial Section Start Here -->
            <?php get_template_part('content/home/home', 'three-testimonial'); ?>
            <!--news-testimonial Section End  Here -->

            <!--client Section start  Here -->
            <?php get_template_part('content/home/home', 'three-client'); ?>
            <!--client Section End  Here -->

            <!--hiring-info-block Section End  Here -->
            <?php get_template_part('content/home/home', 'three-hiring'); ?>
            <!--hiring-info-block Section End  Here -->

        </div>
        <?php
    endwhile;
endif;
get_footer();