<?php
/**
 * Template Name: Attorney History
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); ?>
        <?php do_action('attorney_format_blog_banner', get_the_id()); ?>
        <!--content Section Start Here -->
        <div id="content">
            <!--history-content Section Start Here -->
            <section class="history-content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php $PageTitle = get_post_meta(get_the_id(),'page_title', true); ?>
                            <?php if(!empty($PageTitle)): ?>
                            <h2><?php echo esc_html($PageTitle); ?></h2>
                            <?php endif; ?>
                            <?php $TitleDescription = get_post_meta(get_the_id(),'title_description', true); ?>
                            <?php if(!empty($TitleDescription)): ?>
                                <span class="heading-details"><?php echo esc_html($TitleDescription); ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="video-container clearfix">
                            <div class="col-xs-12 col-sm-5 video-section">
                                <?php echo get_post_meta(get_the_id(),'video', true); ?>
                            </div>
                            <div class="col-xs-12 col-sm-7 history-declaration">
                                <?php the_content(); ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--history-content Section End Here -->

            <!-- our-history year section start here -->

            <section class="our-history-year">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php $OurYearTitle = get_post_meta(get_the_id(),'our_year_title', true); ?>
                            <?php if(!empty($OurYearTitle)): ?>
                            <h2><?php echo esc_html($OurYearTitle); ?></h2>
                            <?php endif; ?>
                            <?php $OurYearTitleDescription = get_post_meta(get_the_id(),'our_year_title_description', true); ?>
                            <?php if(!empty($OurYearTitleDescription)): ?>
                                <span class="heading-details"><?php echo esc_html($OurYearTitleDescription); ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="yearwise-listing clearfix">
                            <div class="block-divisor animate-effect">
                            </div>
                            <?php $i = 1; ?> 
                            <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
                            <?php $history = new WP_Query(array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'history', 'posts_per_page' => get_option('posts_per_page'), 'paged' => $paged)); ?>
                            <?php if ($history->have_posts()): ?>
                                <?php while ($history->have_posts()): $history->the_post(); ?>
                                    <?php
                                    $class = '';
                                    if ($i % 2 != 0):
                                        $class = 'spacer-right';
                                    endif;
                                    ?>
                                    <div class="col-xs-12 col-sm-6 year-listing-block <?php echo esc_attr($class); ?> animate-effect">
                                        <?php if($i%2 != 0): ?> 
                                            <span class="dot-circle"><span></span></span>
                                        <?php endif; ?>
                                        <h3 class="underline-label">
                                            <span><?php
                                                $datetime = new DateTime(get_post_meta(get_the_id(), 'datetime', true));
                                                echo esc_html($datetime->format('Y'))?> </span><?php echo '1' == $i ? '- Starting year' : '- year'; ?>
                                        </h3>
                                        <?php the_content(); ?>
                                    </div>
                                    <?php
                                    $i++;
                                endwhile;
                                attorney_pagenavi($history);
                            else :
                                get_template_part('content/none');
                            endif;
                            wp_reset_postdata();
                            ?>    
                        </div>
                    </div> <!-- .row ends here -->
                </div>
            </section>

            <!-- our-history year section End here -->

        </div>
        <!--content Section ends Here -->
    <?php endwhile; ?>
<?php endif; ?>
<?php
get_footer();
