<?php
/**
 * Attorney - tag page
 * The template for displaying Tag pages
 * 
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<!--content Section Start Here -->
<div id="content">
    <!--blog-content Section Start Here -->
    <section class="blog-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-9">
                    <?php
                    if (have_posts()) :
                        ?>
			<header class="archive-header">
				<h2><?php printf( __( 'Tag Archives: %s', 'attorney' ), single_tag_title( '', false ) ); ?></h2>
				<?php if ( tag_description() ) : // Show an optional tag description ?>
				<div class="archive-meta"><?php echo tag_description(); ?></div>
				<?php endif; ?>
			</header><!-- .archive-header -->
                        <?php
                        while (have_posts()) : the_post();
                            //$postFormat = (get_post_format()) ? get_post_format() : "standard";
                            $postFormat = "standard";
                            get_template_part('content/format/' . $postFormat);
                        endwhile;
                        attorney_pagenavi();
                    else :
                        get_template_part('content/none');
                    endif;
                    ?>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-3 blog-page blog-sidebar">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
    <!--blog-content Section End Here -->
</div>
<!--content Section End Here -->
<?php
get_footer();
