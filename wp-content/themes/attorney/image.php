<?php
/**
 * Attorney - single
 * The template for displaying all single posts.
 * 
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<!--content Section Start Here -->
<div id="content">
    <!--blog-content Section Start Here -->
    <section class="blog-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-9">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) : the_post();
                            ?>
                            <div id="post-<?php the_ID(); ?>" <?php post_class('blog-listing clearfix'); ?>>
                                <div class="blog-listing-pics">
                                    <figure>
                                        <?php attorney_the_attached_image(); ?>
                                    </figure>
                                </div>
                                <p> <?php
                                $metadata = wp_get_attachment_metadata();
                                printf('<span class="attachment-meta full-size-link"><a href="%1$s" title="%2$s">%3$s (%4$s &times; %5$s)</a></span>', esc_url(wp_get_attachment_url()), esc_attr__('Link to full-size image', 'attorney'), __('Full resolution', 'attorney'), $metadata['width'], $metadata['height']);
                                        ?></p>
                                <div class="blog-information">
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <?php
                                    if (has_excerpt()) : the_excerpt();
                                    endif;
                                    if (!empty($post->post_content)) : the_content();
                                    endif;
                                    ?>
                                </div>
                            </div>

                            <?php
                            // If comments are open or we have at least one comment, load up the comment template.
                            if (comments_open() || get_comments_number()) :
                                comments_template();
                            endif;
                        endwhile;
                    else :
                        get_template_part('content/none');
                    endif;
                    ?>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-3">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
    <!--blog-content Section End Here -->
</div>
<!--content Section End Here -->
<?php
get_footer();

