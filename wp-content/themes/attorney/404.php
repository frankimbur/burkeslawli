<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Attorney
 */
get_header();

$deleteImage = (ot_get_option('attorney_404_img')) ? ot_get_option('attorney_404_img') : get_template_directory_uri().'/assets/img/delete-box.png';
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <section class="banner-one" id="slider">
            <div class="about-banner" style="background: url('<?php ot_echo_option('not_found_banner_image'); ?>') center top no-repeat; background-size: cover;">
                <div class="container banner-text">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1><?php esc_html_e('404', 'attorney'); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="content">
            <!--page-not-found Section Start Here -->
            <section class="page-not-found">
                <div class="container">
                    <div class="row">
                        <span class="animate-effect bin anim-section animate"> <img title="" alt="" src="<?php echo esc_url($deleteImage); ?>"> 
                            <i class="fa fa-times"></i>
                        </span>
                        <span class="error-value animate-effect anim-section animate"><?php esc_html_e('404', 'attorney') ?></span>
                        <span class="error-msg animate-effect anim-section animate"><?php  printf("<span> %s </span> %s", esc_html__("oops", "attorney"), esc_html__("this page cannot be found", "attorney"));  ?> </span>
                    </div>
                </div>
            </section>
            <!--page-not-found Section End Here -->
        </div>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
