<?php
/**
 * Template Name: Attorney Home Two
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
if (have_posts()):
    while (have_posts()): the_post();
        ?>
        <!-- Banner Slider Starts -->
        <?php get_template_part('content/home/home', 'two-slider'); ?>
        <!-- Banner Slider Ends -->
        <div id="content">
            <!--about-us Section Start Here -->
            <?php get_template_part('content/home/home', 'two-about-us'); ?>
            <!--about-us Section End  Here -->

            <!--practice-area Section Start Here -->
            <?php get_template_part('content/home/home', 'two-practice'); ?>
            <!--practice-area Section End Here -->

            <!--Attorney Section End Here -->
            <?php get_template_part('content/home/home', 'two-attorney'); ?>

            <!--testimonial Section Start Here -->
            <?php get_template_part('content/home/home', 'two-testimonial'); ?>
            <!--news-testimonial Section End  Here -->

            <!--client Section start  Here -->
            <?php get_template_part('content/home/home', 'two-client'); ?>
            <!--client Section End  Here -->

        </div>
        <?php
    endwhile;
endif;
get_footer();
