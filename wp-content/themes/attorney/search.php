<?php
/**
 * Attorney - Search page
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<!--content Section Start Here -->
<div id="content">
    <!--blog-content Section Start Here -->
    <section class="blog-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-9 blog-three">
                    <?php
                    if (have_posts()) :
                        ?>
                        <header class="page-header">
                                    <h2 class="page-title"><?php printf( __( 'Search Results for: %s', 'attorney' ), get_search_query() ); ?></h1>
                        </header><!-- .page-header -->
                        <?php
                        while (have_posts()) : the_post();
                            ?>
                            <div id="post-<?php the_ID(); ?>" class="blog-listing clearfix">
                                <?php if (has_post_thumbnail() && !post_password_required() && !is_attachment()) : ?>
                                    <div class="blog-listing-pics zoom">
<!--                                        <div class="blog-listing-pics animate-effect">
                                            <figure>
                                                <a href="<?php //the_permalink(); ?>"><?php //the_post_thumbnail('attorney_blog_three'); ?></a>
                                            </figure>
                                        </div>-->


                                    </div>
                                <?php endif; ?>

                                <div class="blog-information">
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                   <div class="blog-admin-info underline-label">
                                        <span ><?php printf(__("- By : %s", "attorney"), "<span> " . '<a href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '">' . esc_html(get_the_author()) . '</a>' . " </span>"); ?>
                                            <!--<span><a href="<?php //echo get_month_link(get_the_time('Y'), get_the_time('m')); ?>"><?php //printf(__("Date : %s", "attorney"), '<span>' . get_the_date("d-M-y") . '</span>'); ?></a></span> -->

                                    </div>
                                    <?php the_excerpt(); ?>
                                </div>

                            </div>
                            <?php
                        endwhile;
                        attorney_pagenavi();
                    else :
                        get_template_part('content/none');
                    endif;
                    ?>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-3 blog-page blog-sidebar">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
    <!--blog-content Section End Here -->
</div>
<!--content Section End Here -->
<?php
get_footer();
