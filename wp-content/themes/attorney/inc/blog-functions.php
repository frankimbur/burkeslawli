<?php
/**
 * Attorney - Blog Functions
 * 
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */

/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
if (!function_exists('attorney_entry_footer')) :
    function attorney_entry_footer() {
        // Hide category and tag text for pages.
        if ('post' == get_post_type()) {
            /* translators: used between list items, there is a space after the comma */
            $categories_list = get_the_category_list(esc_html__(', ', 'attorney'));
            if ($categories_list && attorney_categorized_blog()) {
                printf('<span class="cat-links">' . esc_html__('Posted in %1$s', 'attorney') . '</span>', $categories_list); // WPCS: XSS OK.
            }

            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list('', esc_html__(', ', 'attorney'));
            if ($tags_list) {
                printf('<span class="tags-links">' . esc_html__('Tagged %1$s', 'attorney') . '</span>', $tags_list); // WPCS: XSS OK.
            }
        }

        if (!is_single() && !post_password_required() && ( comments_open() || get_comments_number() )) {
            echo '<span class="comments-link">';
            comments_popup_link(esc_html__('Leave a comment', 'attorney'), esc_html__('1 Comment', 'attorney'), esc_html__('% Comments', 'attorney'));
            echo '</span>';
        }

        edit_post_link(esc_html__('Edit', 'attorney'), '<span class="edit-link">', '</span>');
    }

endif;

/**
 * Display the archive title based on the queried object.
 */
if (!function_exists('attorney_the_archive_title')) :

    function attorney_the_archive_title($before = '', $after = '') {
        if (is_category()) {
            $title = sprintf(esc_html__('Category: %s', 'attorney'), single_cat_title('', false));
        } elseif (is_tag()) {
            $title = sprintf(esc_html__('Tag: %s', 'attorney'), single_tag_title('', false));
        } elseif (is_author()) {
            $title = sprintf(esc_html__('Author: %s', 'attorney'), '<span class="vcard">' . get_the_author() . '</span>');
        } elseif (is_year()) {
            $title = sprintf(esc_html__('Year: %s', 'attorney'), get_the_date(esc_html_x('Y', 'yearly archives date format', 'attorney')));
        } elseif (is_month()) {
            $title = sprintf(esc_html__('Month: %s', 'attorney'), get_the_date(esc_html_x('F Y', 'monthly archives date format', 'attorney')));
        } elseif (is_day()) {
            $title = sprintf(esc_html__('Day: %s', 'attorney'), get_the_date(esc_html_x('F j, Y', 'daily archives date format', 'attorney')));
        } elseif (is_tax('post_format')) {
            if (is_tax('post_format', 'post-format-aside')) {
                $title = esc_html_x('Asides', 'post format archive title', 'attorney');
            } elseif (is_tax('post_format', 'post-format-gallery')) {
                $title = esc_html_x('Galleries', 'post format archive title', 'attorney');
            } elseif (is_tax('post_format', 'post-format-image')) {
                $title = esc_html_x('Images', 'post format archive title', 'attorney');
            } elseif (is_tax('post_format', 'post-format-video')) {
                $title = esc_html_x('Videos', 'post format archive title', 'attorney');
            } elseif (is_tax('post_format', 'post-format-quote')) {
                $title = esc_html_x('Quotes', 'post format archive title', 'attorney');
            } elseif (is_tax('post_format', 'post-format-link')) {
                $title = esc_html_x('Links', 'post format archive title', 'attorney');
            } elseif (is_tax('post_format', 'post-format-status')) {
                $title = esc_html_x('Statuses', 'post format archive title', 'attorney');
            } elseif (is_tax('post_format', 'post-format-audio')) {
                $title = esc_html_x('Audio', 'post format archive title', 'attorney');
            } elseif (is_tax('post_format', 'post-format-chat')) {
                $title = esc_html_x('Chats', 'post format archive title', 'attorney');
            }
        } elseif (is_post_type_archive()) {
            $title = sprintf(esc_html__('Archives: %s', 'attorney'), post_type_archive_title('', false));
        } elseif (is_tax()) {
            $tax = get_taxonomy(get_queried_object()->taxonomy);
            /* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
            $title = sprintf(esc_html__('%1$s: %2$s', 'attorney'), $tax->labels->singular_name, single_term_title('', false));
        } else {
            $title = esc_html__('Archives', 'attorney');
        }

        /**
         * Filter the archive title.
         *
         * @param string $title Archive title to be displayed.
         */
        $title = apply_filters('get_the_archive_title', $title);

        if (!empty($title)) {
            echo $before . $title . $after;  // WPCS: XSS OK.
        }
    }

endif;

/**
 * Display category, tag, or term description.
 */
if (!function_exists('attorney_the_archive_description')) :

    function attorney_the_archive_description($before = '', $after = '') {
        $description = apply_filters('get_the_archive_description', term_description());

        if (!empty($description)) {
            echo $before . $description . $after;  // WPCS: XSS OK.
        }
    }

endif;


/**
 * Display navigation to next/previous set of posts when applicable.
 */
if (!function_exists('attorney_the_posts_navigation')) :

    function attorney_the_posts_navigation() {
        // Don't print empty markup if there's only one page.
        if ($GLOBALS['wp_query']->max_num_pages < 2) {
            return;
        }
        ?>
        <nav class="navigation posts-navigation" role="navigation">
            <h2 class="screen-reader-text"><?php esc_html_e('Posts navigation', 'attorney'); ?></h2>
            <div class="nav-links">
                <?php if (get_next_posts_link()) : ?>
                    <div class="nav-previous"><?php next_posts_link(esc_html__('Older posts', 'attorney')); ?></div>
        <?php endif; ?>

        <?php if (get_previous_posts_link()) : ?>
                    <div class="nav-next"><?php previous_posts_link(esc_html__('Newer posts', 'attorney')); ?></div>
        <?php endif; ?>

            </div><!-- .nav-links -->
        </nav><!-- .navigation -->
        <?php
    }

endif;

/**
 * Display navigation to next/previous post when applicable.
 */
if (!function_exists('attorney_post_navigation')) :

    function attorney_post_navigation() {
        // Don't print empty markup if there's nowhere to navigate.
        $previous = ( is_attachment() ) ? get_post(get_post()->post_parent) : get_adjacent_post(false, '', true);
        $next = get_adjacent_post(false, '', false);

        if (!$next && !$previous) {
            return;
        }
        $nepo=get_next_post();
        $prpo=get_previous_post();
        ?>
          <ul class="pagination-list clearfix animate-effect anim-section animate">
                  <?php if(!empty($prpo)): ?>
                <li>
                        <a href="<?php echo get_permalink($prpo->ID); ?>"> <i class="fa fa-chevron-left"></i> <?php _e('Previous', 'attorney');?> </a>
                </li>
                <?php endif; ?> 
                <?php if(!empty($nepo)): ?>
               <li>
                        <a class="next line" href="<?php echo get_permalink($nepo->ID); ?>"> <?php _e('Next', 'attorney');?> <i class="fa fa-chevron-right"></i> </a>
                </li>
              <?php endif; ?>
         </ul><!-- .navigation -->
        <?php
    }

endif;

/**
 * Returns true if a blog has more than 1 category.
 */
function attorney_categorized_blog() {
    if (false === ( $all_the_cool_cats = get_transient('attorney_categories') )) {
        // Create an array of all the categories that are attached to posts.
        $all_the_cool_cats = get_categories(array(
            'fields' => 'ids',
            'hide_empty' => 1,
            // We only need to know if there is more than one category.
            'number' => 2,
                ));
        // Count the number of categories that are attached to the posts.
        $all_the_cool_cats = count($all_the_cool_cats);

        set_transient('attorney_categories', $all_the_cool_cats);
    }

    if ($all_the_cool_cats > 1) {
        return true;
    } else {
        return false;
    }
}

/**
 * Flush out the transients used in attorney_categorized_blog.
 */
function attorney_category_transient_flusher() {
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    delete_transient('attorney_categories');
}

add_action('edit_category', 'attorney_category_transient_flusher');
add_action('save_post', 'attorney_category_transient_flusher');

/**
 * Print the attached image with a link to the next attached image.
 */
if (!function_exists('attorney_the_attached_image')) :

    function attorney_the_attached_image() {
        $attachment_size = apply_filters('attorney_attachment_size', array(724, 724));
        $next_attachment_url = wp_get_attachment_url();
        $post = get_post();

        /*
         * Grab the IDs of all the image attachments in a gallery so we can get the URL
         * of the next adjacent image in a gallery, or the first image (if we're
         */
        $attachment_ids = get_posts(array(
            'post_parent' => $post->post_parent,
            'fields' => 'ids',
            'numberposts' => -1,
            'post_status' => 'inherit',
            'post_type' => 'attachment',
            'post_mime_type' => 'image',
            'order' => 'ASC',
            'orderby' => 'menu_order ID',
                ));

        // If there is more than 1 attachment in a gallery...
        if (count($attachment_ids) > 1) {
            foreach ($attachment_ids as $attachment_id) {
                if ($attachment_id == $post->ID) {
                    $next_id = current($attachment_ids);
                    break;
                }
            }

            // get the URL of the next image attachment...
            if ($next_id)
                $next_attachment_url = get_attachment_link($next_id);

            // or get the URL of the first image attachment.
            else
                $next_attachment_url = get_attachment_link(reset($attachment_ids));
        }

        printf('<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>', esc_url($next_attachment_url), the_title_attribute(array('echo' => false)), wp_get_attachment_image($post->ID, $attachment_size)
        );
    }
endif;
