<?php

/**
 * Attorney - Format Hooks
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */

attorney_inclusion( "inc/core/nav-walker.php" );
attorney_inclusion( "inc/core/layout-option.php" );
attorney_inclusion( "inc/core/header-layout.php" );

add_action("init",  array("Attorney\Layout\Header_Layout", "set_option"));
add_action("attorney_nav_menu", array("Attorney\Layout\Header_Layout", "nav_menu"));
add_filter("body_class", array("Attorney\Layout\Header_Layout", "body_class"));
add_action("attorney_header", array("Attorney\Layout\Header_Layout", "header"));
add_action("attorney_header_social", array("Attorney\Layout\Header_Layout", "social"));
add_action("attorney_header_search", array("Attorney\Layout\Header_Layout", "search"));

attorney_inclusion( "inc/core/blog-layout.php" );

add_filter('the_content_more_link', array("Attorney\Layout\Blog_Layout", "read_more_link"));
add_action("attorney_format_blog_meta", array("Attorney\Layout\Blog_Layout", "format_blog_meta"));
add_action("attorney_format_blog_meta_single", array("Attorney\Layout\Blog_Layout", "format_blog_meta_single"));
add_action("attorney_format_blog_single_share_meta", array("Attorney\Layout\Blog_Layout", "blog_single_share_meta") );
add_action("attorney_format_blog_banner", array("Attorney\Layout\Blog_Layout", "blog_banner_slider") );

attorney_inclusion( "inc/core/home-layout.php" );
add_action("ot_after_theme_options_save", array("Attorney\Layout\Home_Layout", "set_home_page"));
//add_action("init",  array("Attorney\Layout\Home_Layout", "set_home_page"));
//add_action("attorney_home", array("Attorney\Layout\Home_Layout", "home"));


attorney_inclusion( "inc/core/footer-layout.php" );

add_action("init",  array("Attorney\Layout\Footer_Layout", "set_option"));
add_action("attorney_footer", array("Attorney\Layout\Footer_Layout", "footer"));


attorney_inclusion( "inc/core/general-function.php" );

add_action("charity_post_in", array("Attorney\Layout\General_Function", "postCategory"));


attorney_inclusion( "inc/core/share-count.php" );
add_action("totalShareCount", array("Attorney\Layout\ShareCount", "totalShareCount"));

