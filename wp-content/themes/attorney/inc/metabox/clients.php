<?php

/**
 * Attorney - attorney Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyClientsMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "clientsMeta"));
    }

    /** 
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function clientsMeta($args) {
        $args[] = array(
            'id' => 'client_boxes',
            'title' => esc_html__('Clients Page Meta Box', 'attorney'),
            'desc' => '',
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
               array(
                    'id'          => 'clients_title',
                    'label'       => esc_html__( 'Clients Title', 'attorney' ),
                    'desc'        =>  esc_html__( 'Clients Title.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => '',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
                array(
                    'id'          => 'clients_title_descripton',
                    'label'       => esc_html__( 'Clients Title Descripton', 'attorney' ),
                    'desc'        =>  esc_html__( 'Clients Title Descripton.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => '',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
               array(
                    'id'          => 'testimonials_page_link',
                    'label'       => esc_html__( 'Testimonials Page Link', 'attorney' ),
                    'desc'        =>  esc_html__( 'Testimonials Page Link.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'custom-post-type-select',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'page',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
                
            )
        );
        return $args;
    }

}

new AttorneyClientsMeta();
