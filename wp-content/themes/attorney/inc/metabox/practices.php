<?php

/**
 * Attorney - practices Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyPracticesMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "practicesMeta"));
    }

    /**
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function practicesMeta($args) {
        $args[] = array(
            'id' => 'practices_boxes',
            'title' => esc_html__('Practices', 'attorney'),
            'desc' => '',
            'pages' => array('practices'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id' => 'practices_banner_image',
                    'label' => esc_html__('Upload Banner Image', 'attorney'),
                    'desc' => esc_html__('Upload Banner Image', 'attorney'),
                    'type' => 'upload',
                    'class' => '',
                ),
                array(
                    'id' => 'practices_sub_title',
                    'label' => esc_html__('Sub Title', 'attorney'),
                    'desc' => esc_html__('Sub Title', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
                array(
                    'id' => 'practices_svg',
                    'label' => esc_html__('Upload svg image', 'attorney'),
                    'desc' => esc_html__('Upload svg image', 'attorney'),
                    'type' => 'upload',
                    'class' => '',
                ),
                array(
                    'id' => 'practices_contact_link',
                    'label' => esc_html__('Contact Attorney Link', 'attorney'),
                    'desc' => esc_html__('Contact Attorney Link', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
            )
        );
        return $args;
    }

}

new AttorneyPracticesMeta();
