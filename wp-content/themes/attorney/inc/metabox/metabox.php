<?php

/**
 * Attorney - Metaboxes (Post & category) 
 *
 * @package     AttorneyTheme.inc.metabox
 * @since       attorney 1.0
 */

class AttorneyMetabox {

    public function __construct() {
        //add metabox
        add_filter("ot_post_formats", '__return_true');
        $this->loadMetabox();
        add_action("admin_init", array(&$this, 'registerMeta'));
    }

    /**
     * Register Metabox
     */
    function registerMeta() {
        $args = apply_filters("attorney_post_register", array());

        if (count($args) > 0):
            foreach ($args as $arg):
                ot_register_meta_box($arg);
            endforeach;
        endif;
    }

    function loadMetabox() {
       attorney_inclusion( "inc/metabox/attorney.php" );
       attorney_inclusion( "inc/metabox/practices.php" );
       attorney_inclusion( "inc/metabox/history.php" );
       attorney_inclusion( "inc/metabox/history-page.php" );
       attorney_inclusion( "inc/metabox/common-page.php" );
       attorney_inclusion( "inc/metabox/contact.php" );
       attorney_inclusion( "inc/metabox/about-us.php" );
       attorney_inclusion( "inc/metabox/clients.php" );
       attorney_inclusion( "inc/metabox/home-page-one.php" );
       attorney_inclusion( "inc/metabox/testimonial.php" );
       attorney_inclusion( "inc/metabox/home-page-four.php" );
       attorney_inclusion( "inc/metabox/home-page-two.php" );
       attorney_inclusion( "inc/metabox/home-page-three.php" );
       attorney_inclusion( "inc/metabox/faq.php" );
    }

}

new AttorneyMetabox();
