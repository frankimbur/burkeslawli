<?php

/**
 * Attorney - attorney Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyCommonPageMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "commonPageMeta"));
    }

    /** 
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function commonPageMeta($args) {
        $args[] = array(
            'id' => 'common_page_boxes',
            'title' => esc_html__('Common Pages Meta Box', 'attorney'),
            'desc' => '',
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id'          => 'page_title',
                    'label'       => esc_html__( 'Page Title', 'attorney' ),
                    'desc'        =>  esc_html__( 'Page Title.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => '',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
               array(
                    'id'          => 'title_description',
                    'label'       => esc_html__( 'Title Description', 'attorney' ),
                    'desc'        =>  esc_html__( 'Title Description.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => '',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
            )
        );
        return $args;
    }

}

new AttorneyCommonPageMeta();
