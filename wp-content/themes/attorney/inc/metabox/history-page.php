<?php

/**
 * Attorney - attorney Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyHistoryPageMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "historyPageMeta"));
    }

    /**
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function historyPageMeta($args) {
        $args[] = array(
            'id' => 'history_page_boxes',
            'title' => esc_html__('History', 'attorney'),
            'desc' => '',
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id'          => 'video',
                    'label'       => esc_html__( 'Enter Video Iframe', 'attorney' ),
                    'desc'        =>  esc_html__( 'Enter Video Iframe.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
               array(
                    'id'          => 'our_year_title',
                    'label'       => esc_html__( 'Our Year Title', 'attorney' ),
                    'desc'        =>  esc_html__( 'Our Year Title.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => '',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
               array(
                    'id'          => 'our_year_title_description',
                    'label'       => esc_html__( 'Our Year Title Description', 'attorney' ),
                    'desc'        =>  esc_html__( 'Our Year Title Description.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => '',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
            )
        );
        return $args;
    }
}
new AttorneyHistoryPageMeta();
