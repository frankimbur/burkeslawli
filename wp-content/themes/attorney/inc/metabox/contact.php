<?php

/**
 * Attorney - attorney Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyPageMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "pageMeta"));
    }

    /** 
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function pageMeta($args) {
        $args[] = array(
            'id' => 'page_boxes',
            'title' => esc_html__('Contact Page Meta Box', 'attorney'),
            'desc' => '',
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id'          => 'contact_form',
                    'label'       => esc_html__( 'Select Contact Form', 'attorney' ),
                    'desc'        =>  esc_html__( 'Select Contact Form.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'custom-post-type-select',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
               array(
                    'id'          => 'latitude',
                    'label'       => esc_html__( 'Enter Latitude', 'attorney' ),
                    'desc'        =>  esc_html__( 'Enter Latitude.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
                array(
                    'id'          => 'longitude',
                    'label'       => esc_html__( 'Enter Longitude', 'attorney' ),
                    'desc'        =>  esc_html__( 'Enter Longitude.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
               array(
                    'id'          => 'contact_details',
                    'label'       => esc_html__( 'Contact Details', 'attorney' ),
                    'desc'        =>  esc_html__( 'Contact Details.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
               array(
                    'id'          => 'office_address',
                    'label'       => esc_html__( 'Office Address', 'attorney' ),
                    'desc'        =>  esc_html__( 'Office Address.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'textarea',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
               array(
                    'id'          => 'phone',
                    'label'       => esc_html__( 'Phone', 'attorney' ),
                    'desc'        =>  esc_html__( 'Phone.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'textarea',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
              array(
                    'id'          => 'email',
                    'label'       => esc_html__( 'Email', 'attorney' ),
                    'desc'        =>  esc_html__( 'Email.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
                array(
                    'id'          => 'free_legal_consultation',
                    'label'       => esc_html__( 'Free Legal Consultation', 'attorney' ),
                    'desc'        =>  esc_html__( 'Free Legal Consultation.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ), 
              array(
                    'id'          => 'consultation_number',
                    'label'       => esc_html__( 'Consultation Phone Number', 'attorney' ),
                    'desc'        =>  esc_html__( 'Consultation Phone Number.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
             array(
                    'id'          => 'consultation_time',
                    'label'       => esc_html__( 'Consultation Time', 'attorney' ),
                    'desc'        =>  esc_html__( 'Consultation Time.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and' 
               ),
             array(
                    'id'          => 'legal_consulation_section_background',
                    'label'       => esc_html__( 'Upload Legal Consulation Section Background', 'attorney' ),
                    'desc'        =>  esc_html__( 'Legal Consulation Section Background.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'upload',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and' 
               ),
                
            )
        );
        return $args;
    }

}

new AttorneyPageMeta();
