<?php

/**
 * Attorney - testimonial Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyTestimonialMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "testimonialMeta"));
    }

    /**
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function testimonialMeta($args) {
        $args[] = array(
            'id' => 'testimonial_boxes',
            'title' => esc_html__('Testimonial', 'attorney'),
            'desc' => '',
            'pages' => array('testimonial'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id' => 'testimonial_sub_title',
                    'label' => esc_html__('Enter Sub Title', 'attorney'),
                    'desc' => esc_html__('Testimonial Sub Title.', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
                array(
                    'id' => 'testimonial_rating',
                    'label' => esc_html__('Enter Testimonial Reating', 'attorney'),
                    'desc' => esc_html__('Testimonial Reating (Between 1 to 5).', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
            	
            )
        );
        return $args;
    }

}

new AttorneyTestimonialMeta();
