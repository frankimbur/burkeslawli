<?php

/**
 * Attorney - attorney Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyHistoryMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "historyMeta"));
    }

    /**
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function historyMeta($args) {
        $args[] = array(
            'id' => 'history_boxes',
            'title' => esc_html__('History', 'attorney'),
            'desc' => '',
            'pages' => array('history'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id' => 'datetime',
                    'label' => esc_html__('Select History Date Time', 'attorney'),
                    'desc' => esc_html__('Select History Date Time', 'attorney'),
                    'type' => 'date-time-picker',
                    'class' => '',
                ),
            )
        );
        return $args;
    }

}

new AttorneyHistoryMeta();
