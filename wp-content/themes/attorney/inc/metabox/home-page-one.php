<?php

/**
 * Attorney - Home Page One Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyHomePageOneMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "homePageOneMeta"));
    }

    /**
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function homePageOneMeta($args) {
        $args[] = array(
            'id' => 'home_page_one_boxes',
            'title' => esc_html__('Home Page One Meta Box', 'attorney'),
            'desc' => esc_html__('Home Page One Meta Box', 'attorney'),
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id' => 'attorney_rev_slider',
                    'label' => esc_html__('Enter Slider Shortcode', 'attorney'),
                    'desc' => esc_html__('Enter Slider Shortcode', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'practice_section_title',
                    'label' => esc_html__('Practice Section Title', 'attorney'),
                    'desc' => esc_html__('Practice Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'practice_section_title_description',
                    'label' => esc_html__('Practice Section Title Description', 'attorney'),
                    'desc' => esc_html__('Practice Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'h1_practice_area_list',
                    'label' => esc_html__(' Practice Area List', 'attorney'),
                    'desc' => esc_html__('Principles List.', 'attorney'),
                    'std' => '',
                    'type' => 'list-item',
                    'section' => 'option_types',
                    'rows' => '',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and',
                    'settings' => array(
                        array(
                            'id' => 'h1_practice_area_image',
                            'label' => esc_html__('Practice Area Image', 'attorney'),
                            'desc' => '',
                            'std' => '',
                            'type' => 'upload',
                            'rows' => '1',
                            'post_type' => '',
                            'taxonomy' => '',
                            'min_max_step' => '',
                            'class' => '',
                            'condition' => '',
                            'operator' => 'and'
                        ),
                        array(
                            'id' => 'h1_practice_area_description',
                            'label' => esc_html__('Practice Area Description', 'attorney'),
                            'desc' => '',
                            'std' => '',
                            'type' => 'textarea-simple',
                            'rows' => '5',
                            'post_type' => '',
                            'taxonomy' => '',
                            'min_max_step' => '',
                            'class' => '',
                            'condition' => '',
                            'operator' => 'and'
                        ),
                        array(
                            'id' => 'h1_practice_area_link',
                            'label' => esc_html__('Practice Area Link', 'attorney'),
                            'desc' => esc_html__('Practice Area Link', 'attorney'),
                            'std'         => '',
                            'type'        => 'custom-post-type-select',
                            'section'     => 'option_types',
                            'rows'        => '',
                            'post_type'   => 'practices',
                            'taxonomy'    => '',
                            'min_max_step'=> '',
                            'class'       => '',
                            'condition'   => '',
                            'operator'    => 'and'
                       ),
                    )
                ),
                array(
                    'id' => 'about_section_title',
                    'label' => esc_html__('About Section Title', 'attorney'),
                    'desc' => esc_html__('About Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                
                
                 array(
                    'id' => 'about_tab_one_label',
                    'label' => esc_html__('About Section Tab One Label', 'attorney'),
                    'desc' => esc_html__('About Section Tab One Label', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                
                array(
                    'id' => 'about_tab_two_label',
                    'label' => esc_html__('About Section Tab Two Label', 'attorney'),
                    'desc' => esc_html__('About Section Tab Two Label', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                
                
                
                
                
                
                array(
                    'id' => 'about_section_title_description',
                    'label' => esc_html__('About Section Title Description', 'attorney'),
                    'desc' => esc_html__('About Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'qualified_attorneys_title',
                    'label' => esc_html__('Enter Qualified Attorneys Title Here.', 'attorney'),
                    'desc' => esc_html__('Enter Qualified Attorneys Title Here.', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'qualified_attorneys',
                    'label' => esc_html__('Enter Qualified Attorneys Text Here.', 'attorney'),
                    'desc' => esc_html__('Enter Qualified Attorneys Text Here.', 'attorney'),
                    'std' => '',
                    'type' => 'textarea-simple',
                    'rows' => '5',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'left_section_histories',
                    'label' => esc_html__('Select Left Section Histories', 'attorney'),
                    'desc' => esc_html__('Select Left Section Histories', 'attorney'),
                    'std'         => '',
                    'type'        => 'custom-post-type-checkbox',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'history',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
                array(
                    'id' => 'long_line_of_attorneys_title',
                    'label' => esc_html__('Enter Long Line of Attorneys Title Here.', 'attorney'),
                    'desc' => esc_html__('Enter Long Line of Attorneys Title Here.', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'long_line_of_attorneys',
                    'label' => esc_html__('Enter Long Line of Attorneys Text Here.', 'attorney'),
                    'desc' => esc_html__('Enter Long Line of Attorneys Text Here.', 'attorney'),
                    'std' => '',
                    'type' => 'textarea-simple',
                    'rows' => '5',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'right_section_histories',
                    'label' => esc_html__('Select Right Section Histories', 'attorney'),
                    'desc' => esc_html__('Select Right Section Histories', 'attorney'),
                    'std'         => '',
                    'type'        => 'custom-post-type-checkbox',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'history',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
               array(
                    'id' => 'news_section_title',
                    'label' => esc_html__('News Section Title', 'attorney'),
                    'desc' => esc_html__('News Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'news_section_title_description',
                    'label' => esc_html__('News Section Title Description', 'attorney'),
                    'desc' => esc_html__('News Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ), 
                
                
              array(
                    'id' => 'testimonials_section_title',
                    'label' => esc_html__('Testimonials Section Title', 'attorney'),
                    'desc' => esc_html__('Testimonials Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'testimonials_section_title_description',
                    'label' => esc_html__('Testimonials Section Title Description', 'attorney'),
                    'desc' => esc_html__('Testimonials Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'no_of_testimonials',
                    'label' => esc_html__('Enter Number of Testimonials to Display in Slider', 'attorney'),
                    'desc' => esc_html__('Number of Testimonials to Display in Slider.', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'testimonial_slider_background',
                    'label' => esc_html__('Upload Testimonial Slider Background Image', 'attorney'),
                    'desc' => esc_html__('Testimonial Slider Background Image.', 'attorney'),
                    'std' => '',
                    'type' => 'upload',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
              array(
                    'id' => 'client_section_title',
                    'label' => esc_html__('Clients Section Title', 'attorney'),
                    'desc' => esc_html__('Clients Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'clients_section_title_description',
                    'label' => esc_html__('Clients Section Title Description', 'attorney'),
                    'desc' => esc_html__('Clients Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'hiring_section_title',
                    'label' => esc_html__('Hiring Section Title', 'attorney'),
                    'desc' => esc_html__('Hiring Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'hiring_section_description',
                    'label' => esc_html__('Hiring Section Description', 'attorney'),
                    'desc' => esc_html__('Hiring Section Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                 array(
                    'id' => 'hiring_section_link',
                    'label' => esc_html__('Hiring Section Link', 'attorney'),
                    'desc' => esc_html__('Hiring Section Link', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'hiring_section_background',
                    'label' => esc_html__('Upload Hiring Section Background', 'attorney'),
                    'desc' => esc_html__('Hiring Section Background', 'attorney'),
                    'std' => '',
                    'type' => 'upload',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
            )
        );
        return $args;
    }
}
new AttorneyHomePageOneMeta();
