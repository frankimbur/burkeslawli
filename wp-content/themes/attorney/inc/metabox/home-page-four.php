<?php

/**
 * Attorney - Home Page One Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyHomePageFourMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "homePageFourMeta"));
    }

    /**
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function homePageFourMeta($args) {
        $args[] = array(
            'id' => 'home_page_four_boxes',
            'title' => esc_html__('Home Page Four Meta Box', 'attorney'),
            'desc' => esc_html__('Home Page Four Meta Box', 'attorney'),
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id' => 'attorney_rev_slider4',
                    'label' => esc_html__('Enter Slider Shortcode', 'attorney'),
                    'desc' => esc_html__('Enter Slider Shortcode', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'practice_section_title4',
                    'label' => esc_html__('Practice Section Title', 'attorney'),
                    'desc' => esc_html__('Practice Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'practice_section_title_description4',
                    'label' => esc_html__('Practice Section Title Description', 'attorney'),
                    'desc' => esc_html__('Practice Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'practice_area_list4',
                    'label' => esc_html__(' Practice Area List', 'attorney'),
                    'desc' => esc_html__('Principles List.', 'attorney'),
                    'std' => '',
                    'type' => 'list-item',
                    'section' => 'option_types',
                    'rows' => '',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and',
                    'settings' => array(
                        array(
                            'id' => 'practice_area_image4',
                            'label' => esc_html__('Practice Area Image', 'attorney'),
                            'desc' => '',
                            'std' => '',
                            'type' => 'upload',
                            'rows' => '1',
                            'post_type' => '',
                            'taxonomy' => '',
                            'min_max_step' => '',
                            'class' => '',
                            'condition' => '',
                            'operator' => 'and'
                        ),
                        array(
                            'id' => 'practice_area_description4',
                            'label' => esc_html__('Practice Area Description', 'attorney'),
                            'desc' => '',
                            'std' => '',
                            'type' => 'textarea-simple',
                            'rows' => '5',
                            'post_type' => '',
                            'taxonomy' => '',
                            'min_max_step' => '',
                            'class' => '',
                            'condition' => '',
                            'operator' => 'and'
                        ),
                        array(
                            'id' => 'practice_area_link4',
                            'label' => esc_html__('Practice Area Link', 'attorney'),
                            'desc' => esc_html__('Practice Area Link', 'attorney'),
                            'std'         => '',
                            'type'        => 'custom-post-type-select',
                            'section'     => 'option_types',
                            'rows'        => '',
                            'post_type'   => 'practices',
                            'taxonomy'    => '',
                            'min_max_step'=> '',
                            'class'       => '',
                            'condition'   => '',
                            'operator'    => 'and'
                       ),
                    )
                ),
                array(
                    'id' => 'about_section_title4',
                    'label' => esc_html__('About Section Title', 'attorney'),
                    'desc' => esc_html__('About Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'about_section_title_description4',
                    'label' => esc_html__('About Section Title Description', 'attorney'),
                    'desc' => esc_html__('About Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'qualified_attorneys_title4',
                    'label' => esc_html__('Enter Qualified Attorneys Title Here.', 'attorney'),
                    'desc' => esc_html__('Enter Qualified Attorneys Title Here.', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'qualified_attorneys4',
                    'label' => esc_html__('Enter Qualified Attorneys Text Here.', 'attorney'),
                    'desc' => esc_html__('Enter Qualified Attorneys Text Here.', 'attorney'),
                    'std' => '',
                    'type' => 'textarea-simple',
                    'rows' => '5',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'long_line_of_attorneys_title4',
                    'label' => esc_html__('Enter Long Line of Attorneys Title Here.', 'attorney'),
                    'desc' => esc_html__('Enter Long Line of Attorneys Title Here.', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'long_line_of_attorneys4',
                    'label' => esc_html__('Enter Long Line of Attorneys Text Here.', 'attorney'),
                    'desc' => esc_html__('Enter Long Line of Attorneys Text Here.', 'attorney'),
                    'std' => '',
                    'type' => 'textarea-simple',
                    'rows' => '5',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'section_histories4',
                    'label' => esc_html__('Select Histories', 'attorney'),
                    'desc' => esc_html__('Select Histories', 'attorney'),
                    'std'         => '',
                    'type'        => 'custom-post-type-checkbox',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'history',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
               array(
                    'id' => 'news_section_title4',
                    'label' => esc_html__('News Section Title', 'attorney'),
                    'desc' => esc_html__('News Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'news_section_title_description4',
                    'label' => esc_html__('News Section Title Description', 'attorney'),
                    'desc' => esc_html__('News Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ), 
              array(
                    'id' => 'testimonials_section_title4',
                    'label' => esc_html__('Testimonials Section Title', 'attorney'),
                    'desc' => esc_html__('Testimonials Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'testimonials_section_title_description4',
                    'label' => esc_html__('Testimonials Section Title Description', 'attorney'),
                    'desc' => esc_html__('Testimonials Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'no_of_testimonials4',
                    'label' => esc_html__('Enter Number of Testimonials to Display in Slider', 'attorney'),
                    'desc' => esc_html__('Number of Testimonials to Display in Slider.', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
              array(
                    'id' => 'client_section_title4',
                    'label' => esc_html__('Clients Section Title', 'attorney'),
                    'desc' => esc_html__('Clients Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'clients_section_title_description4',
                    'label' => esc_html__('Clients Section Title Description', 'attorney'),
                    'desc' => esc_html__('Clients Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'hiring_section_title4',
                    'label' => esc_html__('Hiring Section Title', 'attorney'),
                    'desc' => esc_html__('Hiring Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'hiring_section_description4',
                    'label' => esc_html__('Hiring Section Description', 'attorney'),
                    'desc' => esc_html__('Hiring Section Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                 array(
                    'id' => 'hiring_section_link4',
                    'label' => esc_html__('Hiring Section Link', 'attorney'),
                    'desc' => esc_html__('Hiring Section Link', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'hiring_section_background4',
                    'label' => esc_html__('Hiring Section Background', 'attorney'),
                    'desc' => esc_html__('Hiring Section Background', 'attorney'),
                    'std' => '',
                    'type' => 'upload',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
            )
        );
        return $args;
    }
}
new AttorneyHomePageFourMeta();
