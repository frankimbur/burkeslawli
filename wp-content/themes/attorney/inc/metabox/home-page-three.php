<?php
/**
 * Attorney - Home Page Two Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyHomePageThreeMeta {
    public function __construct() {
        $this->action();
    }
    function action() {
        add_filter("attorney_post_register", array(&$this, "homePageTwoMeta"));
    }
    /**
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function homePageTwoMeta($args) {
        $args[] = array(
            'id' => 'home_page_three_boxes',
            'title' => esc_html__('Home Page Four Meta Box', 'attorney'),
            'desc' => esc_html__('Home Page Four Meta Box', 'attorney'),
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id' => 'attorney_rev_slider3',
                    'label' => esc_html__('Enter Slider Shortcode', 'attorney'),
                    'desc' => esc_html__('Enter Slider Shortcode', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'about_title3',
                    'label' => esc_html__('About Section Title', 'attorney'),
                    'desc' => esc_html__('About Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'about_title_description3',
                    'label' => esc_html__('About Section Title Description', 'attorney'),
                    'desc' => esc_html__('About Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'about_content_description3',
                    'label' => esc_html__('About Section Content Description', 'attorney'),
                    'desc' => esc_html__('About Section Content Description', 'attorney'),
                    'std' => '',
                    'type' => 'textarea-simple',
                    'rows' => '5',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'about_contact_no3',
                    'label' => esc_html__('About Contact Number', 'attorney'),
                    'desc' => esc_html__('About Contact Number.', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'about_email3',
                    'label' => esc_html__('About Email', 'attorney'),
                    'desc' => esc_html__('About Email.', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),              
                
                array(
                    'id' => 'practice_section_title3',
                    'label' => esc_html__('Practice Section Title', 'attorney'),
                    'desc' => esc_html__('Practice Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'practice_section_title_description3',
                    'label' => esc_html__('Practice Section Title Description', 'attorney'),
                    'desc' => esc_html__('Practice Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'practice_area_list3',
                    'label' => esc_html__(' Practice Area List', 'attorney'),
                    'desc' => esc_html__('Principles List.', 'attorney'),
                    'std' => '',
                    'type' => 'list-item',
                    'section' => 'option_types',
                    'rows' => '',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and',
                    'settings' => array(
                        array(
                            'id' => 'practice_area_image3',
                            'label' => esc_html__('Practice Area Image', 'attorney'),
                            'desc' => '',
                            'std' => '',
                            'type' => 'upload',
                            'rows' => '1',
                            'post_type' => '',
                            'taxonomy' => '',
                            'min_max_step' => '',
                            'class' => '',
                            'condition' => '',
                            'operator' => 'and'
                        ),
                        array(
                            'id' => 'practice_area_description3',
                            'label' => esc_html__('Practice Area Description', 'attorney'),
                            'desc' => '',
                            'std' => '',
                            'type' => 'textarea-simple',
                            'rows' => '5',
                            'post_type' => '',
                            'taxonomy' => '',
                            'min_max_step' => '',
                            'class' => '',
                            'condition' => '',
                            'operator' => 'and'
                        ),
                        array(
                            'id' => 'practice_area_link3',
                            'label' => esc_html__('Practice Area Link', 'attorney'),
                            'desc' => esc_html__('Practice Area Link', 'attorney'),
                            'std'         => '',
                            'type'        => 'custom-post-type-select',
                            'section'     => 'option_types',
                            'rows'        => '',
                            'post_type'   => 'practices',
                            'taxonomy'    => '',
                            'min_max_step'=> '',
                            'class'       => '',
                            'condition'   => '',
                            'operator'    => 'and'
                       ),
                    )
                ),
                array(
                    'id' => 'attorney_section_title3',
                    'label' => esc_html__('Attorney Section Title', 'attorney'),
                    'desc' => esc_html__('Attorney Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'attorney_section_title_description3',
                    'label' => esc_html__('Attorney Section Title Description', 'attorney'),
                    'desc' => esc_html__('Attorney Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
               
                array(
                    'id' => 'news_section_title3',
                    'label' => esc_html__('News Section Title', 'attorney'),
                    'desc' => esc_html__('News Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'news_section_title_description3',
                    'label' => esc_html__('News Section Title Description', 'attorney'),
                    'desc' => esc_html__('News Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ), 
                array(
                    'id' => 'news_slider_background3',
                    'label' => esc_html__('Upload News Section Slider Background', 'attorney'),
                    'desc' => esc_html__('News Section Slider Background.', 'attorney'),
                    'std' => '',
                    'type' => 'upload',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
               
                array(
                    'id' => 'testimonials_section_title3',
                    'label' => esc_html__('Testimonials Section Title', 'attorney'),
                    'desc' => esc_html__('Testimonials Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'testimonials_section_title_description3',
                    'label' => esc_html__('Testimonials Section Title Description', 'attorney'),
                    'desc' => esc_html__('Testimonials Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'no_of_testimonials3',
                    'label' => esc_html__('Enter Number of Testimonials to Display in Slider', 'attorney'),
                    'desc' => esc_html__('Number of Testimonials to Display in Slider.', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                
                array(
                    'id' => 'client_section_title3',
                    'label' => esc_html__('Clients Section Title', 'attorney'),
                    'desc' => esc_html__('Clients Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'clients_section_title_description3',
                    'label' => esc_html__('Clients Section Title Description', 'attorney'),
                    'desc' => esc_html__('Clients Section Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'hiring_section_title3',
                    'label' => esc_html__('Hiring Section Title', 'attorney'),
                    'desc' => esc_html__('Hiring Section Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'hiring_section_sub_title3',
                    'label' => esc_html__('Hiring Section Sub Title', 'attorney'),
                    'desc' => esc_html__('Hiring Section Sub Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array( 
                    'id' => 'hiring_section_description3',
                    'label' => esc_html__('Hiring Section Description', 'attorney'),
                    'desc' => esc_html__('Hiring Section Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                 array(
                    'id' => 'hiring_section_link3',
                    'label' => esc_html__('Hiring Section Link', 'attorney'),
                    'desc' => esc_html__('Hiring Section Link', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '1',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ), 
            )
        );
        return $args;
    }
}
new AttorneyHomePageThreeMeta();
