<?php

/**
 * Attorney - About Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyAboutMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "aboutMeta"));
    }

    /**
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function aboutMeta($args) {
        $args[] = array(
            'id' => 'about_boxes',
            'title' => esc_html__('About Us Page Meta Box', 'attorney'),
            'desc' => '',
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id' => 'about_us_image',
                    'label' => esc_html__('About Us Image', 'attorney'),
                    'desc' => esc_html__('About Us Image.', 'attorney'),
                    'std' => '',
                    'type' => 'upload',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'principles_title',
                    'label' => esc_html__('Principles Title', 'attorney'),
                    'desc' => esc_html__('Principles Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '5',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'principles_title_description',
                    'label' => esc_html__('Principles Title Description', 'attorney'),
                    'desc' => esc_html__('Principles Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '5',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'principles_list',
                    'label' => esc_html__('Principles List', 'attorney'),
                    'desc' => esc_html__('Principles List.', 'attorney'),
                    'std' => '',
                    'type' => 'list-item',
                    'section' => 'option_types',
                    'rows' => '',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and',
                    'settings' => array(
                        array(
                            'id' => 'principles_description',
                            'label' => esc_html__('Principles Description', 'attorney'),
                            'desc' => '',
                            'std' => '',
                            'type' => 'textarea-simple',
                            'rows' => '5',
                            'post_type' => '',
                            'taxonomy' => '',
                            'min_max_step' => '',
                            'class' => '',
                            'condition' => '',
                            'operator' => 'and'
                        ),
                    )
                ),
                array(
                    'id' => 'practice_title',
                    'label' => esc_html__('Practice Title', 'attorney'),
                    'desc' => esc_html__('Practice Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '5',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'practice_title_description',
                    'label' => esc_html__('Practice Title Description', 'attorney'),
                    'desc' => esc_html__('Practice Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '5',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'practice_area_list',
                    'label' => esc_html__('Practice Area List', 'attorney'),
                    'desc' => esc_html__('Practice Area List.', 'attorney'),
                    'std' => '',
                    'type' => 'list-item',
                    'section' => 'option_types',
                    'rows' => '',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and',
                    'settings' => array(
                        array(
                            'id' => 'practice_description',
                            'label' => esc_html__('Practice Description', 'attorney'),
                            'desc' => '',
                            'std' => '',
                            'type' => 'textarea-simple',
                            'rows' => '5',
                            'post_type' => '',
                            'taxonomy' => '',
                            'min_max_step' => '',
                            'class' => '',
                            'condition' => '',
                            'operator' => 'and'
                        ),
                        array(
                            'id' => 'practice_image',
                            'label' => esc_html__('Upload Practice Image or SVG', 'attorney'),
                            'desc' => '',
                            'std' => '',
                            'type' => 'upload',
                            'rows' => '5',
                            'post_type' => '',
                            'taxonomy' => '',
                            'min_max_step' => '',
                            'class' => '',
                            'condition' => '',
                            'operator' => 'and'
                        ),
                    )
                ),
                array(
                    'id' => 'our_service_title',
                    'label' => esc_html__('Our Service Title', 'attorney'),
                    'desc' => esc_html__('Our Service Title', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '5',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'our_service_title_description',
                    'label' => esc_html__('Our Service Title Description', 'attorney'),
                    'desc' => esc_html__('Our Service Title Description', 'attorney'),
                    'std' => '',
                    'type' => 'text',
                    'rows' => '5',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'our_service_list',
                    'label' => esc_html__('Our Services List', 'attorney'),
                    'desc' => esc_html__('Our Services List.', 'attorney'),
                    'std' => '',
                    'type' => 'list-item',
                    'section' => 'option_types',
                    'rows' => '',
                    'post_type' => '',
                    'taxonomy' => '',
                    'min_max_step' => '',
                    'class' => '',
                    'condition' => '',
                    'operator' => 'and',
                    'settings' => array(
                        array(
                            'id' => 'service_page_link',
                            'label' => esc_html__('Service Page Link', 'attorney'),
                            'desc' => '',
                            'std' => '',
                            'type' => 'text',
                            'rows' => '5',
                            'post_type' => '',
                            'taxonomy' => '',
                            'min_max_step' => '',
                            'class' => '',
                            'condition' => '',
                            'operator' => 'and'
                        ),
                    )
                ),
            )
        );
        return $args;
    }

}

new AttorneyAboutMeta();

