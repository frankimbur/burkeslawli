<?php

/**
 * Attorney - attorney Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "attorneyMeta"));
    }

    /**
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function attorneyMeta($args) {
        $args[] = array(
            'id' => 'attorney_boxes',
            'title' => esc_html__('Attorney', 'attorney'),
            'desc' => '',
            'pages' => array('attorney'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
            array(
                    'id' => 'attorney_cunsult_text',
                    'label' => esc_html__('Enter Cunsult Text', 'attorney'),
                    'desc' => esc_html__('Enter Cunsult Text', 'attorney'),
                    'type' => 'text',
                    'class' => '',			
                
                ),
                array(
                    'id' => 'attorney_image',
                    'label' => esc_html__('Upload Attorney Second Layout Image', 'attorney'),
                    'desc' => esc_html__('Upload Attorney Second Layout Image', 'attorney'),
                    'type' => 'upload',
                    'class' => '',
                ),
                array(
                    'id' => 'attorney_detailed_page_image',
                    'label' => esc_html__('Upload Attorney detailed page Image', 'attorney'),
                    'desc' => esc_html__('Upload Attorney detailed page Image.', 'attorney'),
                    'type' => 'upload',
                    'class' => '',
                ),
                array(
                    'id' => 'attorney_designation',
                    'label' => esc_html__('Enter Here Designation Text', 'attorney'),
                    'desc' => esc_html__('Enter Here Designation Text', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
                array(
                    'id' => 'attorney_twitter',
                    'label' => esc_html__('Enter Here Twitter Link', 'attorney'),
                    'desc' => esc_html__('Enter Here Twitter Link', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
                array(
                    'id' => 'attorney_facebook',
                    'label' => esc_html__('Enter Here Facebook Link', 'attorney'),
                    'desc' => esc_html__('Enter Here Facebook Link', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
                array(
                    'id' => 'attorney_linkedin',
                    'label' => esc_html__('Enter Here Linkedin Link', 'attorney'),
                    'desc' => esc_html__('Enter Here Linkedin Link', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
                array(
                    'id' => 'attorney_contact_no',
                    'label' => esc_html__('Enter Here Contact No', 'attorney'),
                    'desc' => esc_html__('Enter Here Contact No', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
                array(
                    'id' => 'attorney_email_id',
                    'label' => esc_html__('Enter Here Email ID', 'attorney'),
                    'desc' => esc_html__('Enter Here Email ID', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
                array(
                    'id' => 'attorney_education',
                    'label' => esc_html__('Enter Here Education Text', 'attorney'),
                    'desc' => esc_html__('Enter Here Education Text', 'attorney'),
                    'type' => 'textarea',
                    'class' => '',
                ),
                array(
                    'id' => 'attorney_contact_form',
                    'label' => esc_html__('Enter Here Contact Form Shortcode', 'attorney'),
                    'desc' => esc_html__('Enter Here Contact Form Shortcode', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
                array(
                    'id' => 'free_legal_consulation_title',
                    'label' => esc_html__('Free legal consulation title', 'attorney'),
                    'desc' => esc_html__('Free legal consulation title.', 'attorney'),
                    'type' => 'text',
                    'class' => '',
                ),
                array(
                    'id' => 'free_legal_consulation_description',
                    'label' => esc_html__('Free legal consulation description', 'attorney'),
                    'desc' => esc_html__('Free legal consulation description.', 'attorney'),
                    'type' => 'textarea',
                    'class' => '',
                ),
            )
        );
        return $args;
    }

}

new AttorneyMeta();
