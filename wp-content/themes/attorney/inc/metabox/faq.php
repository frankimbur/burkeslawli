<?php

/**
 * Attorney - attorney Metabox
 *
 * @package     attorney.inc.metabox
 * @version     attorney 1.0
 */
class AttorneyFaqPageMeta {

    public function __construct() {
        $this->action();
    }

    function action() {
        add_filter("attorney_post_register", array(&$this, "FaqPageMeta"));
    }

    /** 
     * Attorney Metabox
     * @param type $args
     * @return string
     */
    function FaqPageMeta($args) {
        $args[] = array(
            'id' => 'faq_page_boxes',
            'title' => esc_html__('FAQ Page Meta Box', 'attorney'),
            'desc' => '',
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id'          => 'newsletter_shortcode',
                    'label'       => esc_html__( 'Enter Newsletter Shortcode', 'attorney' ),
                    'desc'        =>  esc_html__( 'Newsletter Shortcode.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'text',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
                array(
                    'id'          => 'ask_question_background',
                    'label'       => esc_html__( 'Upload Ask Question Background', 'attorney' ),
                    'desc'        =>  esc_html__( 'Ask Question Background.', 'attorney' ),
                    'std'         => '',
                    'type'        => 'upload',
                    'section'     => 'option_types',
                    'rows'        => '',
                    'post_type'   => 'wpcf7_contact_form',
                    'taxonomy'    => '',
                    'min_max_step'=> '',
                    'class'       => '',
                    'condition'   => '',
                    'operator'    => 'and'
               ),
                
            )
        );
        return $args;
    }

}

new AttorneyFaqPageMeta();
