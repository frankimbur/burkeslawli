<?php

return array(
    array(
        'id' => 'attorney_1home_layout',
        'label' => esc_html__('Select Home Page 1', 'attorney'),
        'desc' => esc_html__('Select page for home 1', 'attorney'),
        'std' => '',
        'type' => 'page-select',
        'section' => 'attorney_4home_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
    ),
    array(
        'id' => 'attorney_2home_layout',
        'label' => esc_html__('Select Home Page 2', 'attorney'),
        'desc' => esc_html__('Select page for home 2', 'attorney'),
        'std' => '',
        'type' => 'page-select',
        'section' => 'attorney_4home_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
    ),
    array(
        'id' => 'attorney_3home_layout',
        'label' => esc_html__('Select Home Page 3', 'attorney'),
        'desc' => esc_html__('Select page for home 3', 'attorney'),
        'std' => '',
        'type' => 'page-select',
        'section' => 'attorney_4home_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
    ),
    array(
        'id' => 'attorney_4home_layout',
        'label' => esc_html__('Select Home Page 4', 'attorney'),
        'desc' => esc_html__('Select page for home 4', 'attorney'),
        'std' => '',
        'type' => 'page-select',
        'section' => 'attorney_4home_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
    ),
    array(
        'id' => 'attorney_4home',
        'label' => esc_html__('Home Layout', 'attorney'),
        'desc' => esc_html__('Select Home layout.', 'attorney'),
        'std' => '',
        'type' => 'radio-image',
        'section' => 'attorney_4home_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => array(
            array(
                'value' => 'home_1',
                'label' => esc_html__('Home One', 'attorney'),
                'src' => get_template_directory_uri() . '/assets/img/home1.png'
            ),
            array(
                'value' => 'home_2',
                'label' => esc_html__('Home Two', 'attorney'),
                'src' => get_template_directory_uri() . '/assets/img/home2.png'
            ),
            array(
                'value' => 'home_3',
                'label' => esc_html__('Home Three', 'attorney'),
                'src' => get_template_directory_uri() . '/assets/img/home3.png'
            ),
            array(
                'value' => 'home_4',
                'label' => esc_html__('Home Four', 'attorney'),
                'src' => get_template_directory_uri() . '/assets/img/home4.png'
            )
        )
    )
);
