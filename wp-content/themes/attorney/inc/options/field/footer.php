<?php
return array(
            array(
                'id' => 'at_copyright',
                'label' => esc_html__('Copyright Text', 'attorney'),
                'desc' => esc_html__('Enter Copyright text here.', 'attorney'),
                'std' => '',
                'type' => 'textarea_simple',
                'section' => 'attorney_footer_section',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and'
            ),
            array(
                'id' => 'at_designed_by',
                'label' => esc_html__('Designed By', 'attorney'),
                'desc' => esc_html__('Enter Designed By text here.', 'attorney'),
                'std' => '',
                'type' => 'textarea_simple',
                'section' => 'attorney_footer_section',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and'
            ),
    
); 