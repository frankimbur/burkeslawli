<?php

return array(
    array(
        'id' => 'attorney_4footer',
        'label' => esc_html__('Footer Layout', 'attorney'),
        'desc' => esc_html__('Select Footer layout.', 'attorney'),
        'std' => '',
        'type' => 'radio-image',
        'section' => 'attorney_4footer_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => array(
            array(
                'value' => 'footer_1',
                'label' => esc_html__('Footer One', 'attorney'),
                'src' => get_template_directory_uri().'/assets/img/footer-one.png'
            ),
            array(
                'value' => 'footer_2',
                'label' => esc_html__('Footer Two', 'attorney'),
                'src' => get_template_directory_uri().'/assets/img/footer-two.png'
            ),
            array(
                'value' => 'footer_3',
                'label' => esc_html__('Footer Three', 'attorney'),
                'src' => get_template_directory_uri().'/assets/img/footer-three.png'
            ),
            array(
                'value' => 'footer_4',
                'label' => esc_html__('Footer Four', 'attorney'),
                'src' => get_template_directory_uri().'/assets/img/footer-four.png'
            )
        )
    )
);