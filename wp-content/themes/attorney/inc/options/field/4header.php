<?php

return array(
    array(
        'id' => 'attorney_4header',
        'label' => esc_html__('Header Layout', 'attorney'),
        'desc' => esc_html__('Select Header layout.', 'attorney'),
        'std' => '',
        'type' => 'radio-image',
        'section' => 'attorney_4header_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => array(
            array(
                'value' => 'header_1',
                'label' => esc_html__('Header One', 'attorney'),
                'src' => get_template_directory_uri().'/assets/img/header-one.png'
            ),
            array(
                'value' => 'header_2',
                'label' => esc_html__('Header Two', 'attorney'),
                'src' => get_template_directory_uri().'/assets/img/header-two.png'
            ),
            array(
                'value' => 'header_3',
                'label' => esc_html__('Header Three', 'attorney'),
                'src' => get_template_directory_uri().'/assets/img/header-three.png'
            ),
            array(
                'value' => 'header_4',
                'label' => esc_html__('Header Four', 'attorney'),
                'src' => get_template_directory_uri().'/assets/img/header-four.png'
            )
        )
    )
);