<?php

return array(
    array(
        'id' => 'attorney_3blog',
        'label' => esc_html__('Blog Layout', 'attorney'),
        'desc' => esc_html__('Select Blog layout.', 'attorney'),
        'std' => '',
        'type' => 'radio-image',
        'section' => 'attorney_3blog_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => array(
            array(
                'value' => 'blog_1',
                'label' => esc_html__('Blog One', 'attorney'),
                'src' => get_template_directory_uri().'/assets/img/blog2.png'
            ),
            array(
                'value' => 'blog_2',
                'label' => esc_html__('Blog Two', 'attorney'),
                'src' => get_template_directory_uri().'/assets/img/blog3.png'
            ),
            array(
                'value' => 'blog_3',
                'label' => esc_html__('Blog Three', 'attorney'),
                'src' => get_template_directory_uri().'/assets/img/blog1.png'
            )
        )
    )
);