<?php

return array(
    array(
        'id' => 'ad_body_layout',
        'label' => esc_html__('Theme Layout', 'attorney'),
        'desc' => esc_html__('Select theme layout.', 'attorney'),
        'std' => '',
        'type' => 'select',
        'section' => 'airdev_layout_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => array(
            array(
                'value' => 'fullWidth',
                'label' => esc_html__('Full Width', 'attorney'),
                'src' => ''
            ),
            array(
                'value' => 'boxed',
                'label' => esc_html__('Boxed', 'attorney'),
                'src' => ''
            )
        )
    ),
    // Header Color Style (Dark Or ThemeColor)
    // Patterns (Boxed version)
    array(
        'id' => 'ad_sticky_header',
        'label' => esc_html__('Sticky Header', 'attorney'),
        'desc' => esc_html__('Select a Header style for theme.', 'attorney'),
        'std' => '',
        'type' => 'select',
        'section' => 'airdev_layout_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => array(
            array(
                'value' => 'no',
                'label' => esc_html__('Normal', 'attorney'),
                'src' => ''
            ),
            array(
                'value' => 'yes',
                'label' => esc_html__('Inteligence', 'attorney'),
                'src' => ''
            )
        )
    ),
    array(
        'id' => 'ad_nav_menu',
        'label' => esc_html__('Navigation Menu', 'attorney'),
        'desc' => esc_html__('Select navigation menu for theme.', 'attorney'),
        'std' => '',
        'type' => 'select',
        'section' => 'airdev_layout_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => array(
            array(
                'value' => 'ad-mobile',
                'label' => esc_html__('Mobile', 'attorney'),
                'src' => ''
            ),
            array(
                'value' => 'horz',
                'label' => esc_html__('Horizontal', 'attorney'),
                'src' => ''
            )
        )
    ),
    
);