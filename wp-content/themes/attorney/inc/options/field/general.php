<?php

return array(
    array(
        'id' => 'attorney_logo',
        'label' => esc_html__('Site Header Logo', 'attorney'),
        'desc' => esc_html__('Enter Site header Logo Here.', 'attorney'),
        'std' => '',
        'type' => 'upload',
        'section' => 'attorney_general_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and'
    ),
    array(
        'id' => 'attorney_favicon',
        'label' => esc_html__('Favicon', 'attorney'),
        'desc' => esc_html__('Please Select a favicon for the theme.', 'attorney'),
        'std' => '',
        'type' => 'upload',
        'section' => 'attorney_general_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => ''
    ),
    array(
        'id' => 'attorney_stickey_header',
        'label' => esc_html__('Stickey Header', 'attorney'),
        'desc' => esc_html__('Select Stickey Header.', 'attorney'),
        'std' => '',
        'type' => 'select',
        'section' => 'attorney_general_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => array(
            array(
                'value' => 'normal',
                'label' => esc_html__('Normal', 'attorney'),
                'src' => ''
            ),
            array(
                'value' => 'fixed',
                'label' => esc_html__('Fixed', 'attorney'),
                'src' => ''
            ),
            array(
                'value' => 'intelligent',
                'label' => esc_html__('Intelligent', 'attorney'),
                'src' => ''
            )
        )
    ),
    array(
        'id' => 'attorney_theme_layout',
        'label' => esc_html__('Theme Layout', 'attorney'),
        'desc' => esc_html__('Select Theme Layout.', 'attorney'),
        'std' => '',
        'type' => 'select',
        'section' => 'attorney_general_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => array(
            array(
                'value' => 'full-width',
                'label' => esc_html__('Full Width', 'attorney'),
                'src' => ''
            ),
            array(
                'value' => 'boxed',
                'label' => esc_html__('Boxed', 'attorney'),
                'src' => ''
            )
        )
    ),
    array(
        'id' => 'attorney_contact_no',
        'label' => esc_html__('Contact No.', 'attorney'),
        'desc' => esc_html__('Enter Contact Number Here.', 'attorney'),
        'std' => '',
        'type' => 'text',
        'section' => 'attorney_general_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and'
    ),
    array(
        'id' => 'attorney_consultation_text',
        'label' => esc_html__('Consultation Text.', 'attorney'),
        'desc' => esc_html__('Enter Consultation Text Here.', 'attorney'),
        'std' => '',
        'type' => 'text',
        'section' => 'attorney_general_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and'
    ),
    array(
        'id' => 'attorney_consultation',
        'label' => esc_html__('Consultation.', 'attorney'),
        'desc' => esc_html__('Enter Consultation Link Here.', 'attorney'),
        'std' => '',
        'type' => 'text',
        'section' => 'attorney_general_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and'
    ),
    array(
        'id' => 'not_found_banner_image',
        'label' => esc_html__('404 Banner Image.', 'attorney'),
        'desc' => esc_html__('Upload 404 Page Banner Image.', 'attorney'),
        'std' => '',
        'type' => 'upload',
        'section' => 'attorney_general_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and'
    ),
    array(
        'id' => 'civil_litigation_image',
        'label' => esc_html__('Civil Litigation Image.', 'attorney'),
        'desc' => esc_html__('Civil Litigation Image.', 'attorney'),
        'std' => '',
        'type' => 'upload',
        'section' => 'attorney_general_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and'
    ),
    array(
        'id' => 'blog_detail_banner_image',
        'label' => esc_html__('Blog Detail Banner Image.', 'attorney'),
        'desc' => esc_html__('Blog Detail Banner Image.', 'attorney'),
        'std' => '',
        'type' => 'upload',
        'section' => 'attorney_general_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and'
    )
);
