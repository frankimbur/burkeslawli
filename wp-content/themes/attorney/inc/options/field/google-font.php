<?php

return array(
	array(
        'id' => 'attorney_google_api_key',
        'label' => esc_html__('Font API Key.', 'attorney'),
        'desc' => esc_html__('Enter Google Font API Key.', 'attorney'),
        'std' => '',
        'type' => 'text',
        'section' => 'attorney_google_font_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and'
    ),	
    array(
        'id' => 'attorney_google_fonts',
        'label' => esc_html__('Google Fonts', 'attorney'),
        'desc' => esc_html__('The Google Fonts option type will dynamically enqueue any number of Google Web Fonts into the document. Now we are providing support for four fonts (Exo, Raleway, Open Sans, Montserrat, Lato)', "attorney") ,
        'std' => array(
            array(
                'family' => 'exo',
                'variants' => array('400',"300","300italic","500","400italic","500italic","600","600italic","700","700italic"),
                'subsets' => array('latin')
            ),
           array(
                'family' => 'raleway',
                'variants' => array('300', '300italic', 'regular', '600', '700'),
                'subsets' => array('latin')
            ),
            array(
                'family' => 'opensans',
                'variants' => array('300', '300italic', 'regular', 'italic', '600', '600italic'),
                'subsets' => array('latin')
            ),
            array(
                'family' => 'montserrat',
                'variants' => array("400",'700'),
                'subsets' => array('latin')
            ),
            array(
                'family' => 'lato',
                'variants' => array("400","900","700italic","700","400italic","300italic","300","100italic","900italic","100"),
                'subsets' => array('latin')
            ),
        ),
        'type' => 'google-fonts',
        'section' => 'attorney_google_font_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and'
    ),
    
);