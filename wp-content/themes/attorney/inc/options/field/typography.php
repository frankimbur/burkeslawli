<?php

return array(
    array(
        'id' => 'attorney_color_main',
        'label' => esc_html__('Theme Color', 'attorney'),
        'desc' => esc_html__('Please Select a color Scheme for the theme.', 'attorney'),
        'std' => '',
        'type' => 'colorpicker',
        'section' => 'attorney_typography_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => ''
    ),
    array(
        'id' => 'attorney_theme_font',
        'label' => esc_html__('Theme Fonts', 'attorney'),
        'desc' => esc_html__('Pleaes select a font style .', 'attorney'),
        'std' => '',
        'type' => 'select',
        'section' => 'attorney_typography_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => attorney_google_font_families()/*array(
            array(
                'value' => "'Exo', sans-serif",
                'label' => esc_html__('Exo, sans-serif', 'attorney'),
                'src' => ''
            ),
            array(
                'value' => "'Raleway', sans-serif",
                'label' => esc_html__('Raleway, sans-serif', 'attorney'),
                'src' => ''
            ),
            array(
                'value' => "'Open Sans', sans-serif",
                'label' => esc_html__('Open Sans, sans-serif', 'attorney'),
                'src' => ''
            ),
            array(
                'value' => "'Montserrat',sans-serif",
                'label' => esc_html__('Montserrat,sans-serif', 'attorney'),
                'src' => ''
            ),
            array(
                'value' => "'Lato', sans-serif",
                'label' => esc_html__('Lato, sans-serif', 'attorney'),
                'src' => ''
            )
        )*/
    ),
    array(
        'id' => 'attorney_theme_effect',
        'label' => esc_html__('Theme Effect', 'attorney'),
        'desc' => esc_html__('Select Theme Effect.', 'attorney'),
        'std' => '',
        'type' => 'select',
        'section' => 'attorney_typography_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and',
        'choices' => array(
            array(
                'value' => 'normal',
                'label' => esc_html__('Normal', 'attorney'),
                'src' => ''
            ),
            array(
                'value' => 'transition',
                'label' => esc_html__('Transition', 'attorney'),
                'src' => ''
            )
        )
    )
);
