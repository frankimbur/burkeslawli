<?php

return array(
  array(
        'id' => 'attorney_google_map_api_key',
        'label' => esc_html__('Enter Map API Key Here.', 'attorney'),
        'desc' => esc_html__('Enter Map API Key Here.', 'attorney'),
        'std' => '',
        'type' => 'text',
        'section' => 'attorney_advance_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and'   
    ),
    array(
        'id' => 'attorney_404_img',
        'label' => esc_html__('Upload Here 404 Page Background Image', 'attorney'),
        'desc' => esc_html__('Please Upload Here 404 Page Background Image.', 'attorney'),
        'std' => '',
        'type' => 'upload',
        'section' => 'attorney_advance_section',
        'rows' => '',
        'post_type' => '',
        'taxonomy' => '',
        'min_max_step' => '',
        'class' => '',
        'condition' => '',
        'operator' => 'and'
    )
);
