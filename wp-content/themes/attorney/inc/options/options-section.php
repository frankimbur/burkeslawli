<?php
/**
 * Attorney - Theme Option 
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc.options
 * @since attorney 1.0
 */
return array(
    array(
        'id' => 'attorney_general_section',
        'icon' => 'dashboard',
        'title' => esc_html__('General', 'attorney')
    ),
    array(
        'id' => 'attorney_typography_section',
        'icon' => 'flask',
        'title' => esc_html__('Typography', 'attorney')
    ),
    array(
        'id' => 'attorney_google_font_section',
        'icon' => 'font',
        'title' => esc_html__('Google Fonts', 'attorney')
    ),
    array(
        'id' => 'attorney_4header_section',
        'icon' => 'compass',
        'title' => esc_html__('Header Layout', 'attorney')
    ),
    array(
        'id' => 'attorney_4home_section',
        'icon' => 'glass',
        'title' => esc_html__('Home Layout', 'attorney')
    ),
    array(
        'id' => 'attorney_4footer_section',
        'icon' => 'compass',
        'title' => esc_html__('Footer Layout', 'attorney')
    ),
    array(
        'id' => 'attorney_3blog_section',
        'icon' => 'compass',
        'title' => esc_html__('Blog Layout', 'attorney')
    ),
    array(
        'id' => 'attorney_footer_section',
        'icon' => 'folder',
        'title' => esc_html__('Footer', 'attorney')
    ),
    array(
        'id' => 'attorney_social_section',
        'icon' => 'cloud',
        'title' => esc_html__('Social Link', 'attorney')
    ),
    array(
        'id' => 'attorney_advance_section',
        'icon' => 'gear',
        'title' => esc_html__('Advance', 'attorney')
    ),
    array(
        'id' => 'attorney_single_page',
        'icon' => 'compass',
        'title' => esc_html__('Attorney Detailed Page', 'attorney')
    ),
);
