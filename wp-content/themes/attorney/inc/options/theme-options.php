<?php
/**
 * Attorney - Theme Option 
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc.options
 * @since attorney 1.0
 */

add_filter("ot_theme_options_sections", "attorney_options_section");

function attorney_options_section($sections) {
    $sections = attorney_inclusion('inc/options/options-section.php', false, false, true);
    return $sections;
}
add_filter("ot_theme_options_settings", 'attorney_options_settings');
function attorney_options_settings($settings=array()) {
    $options = array(
        "general",
        "typography",
        "google-font",
        "4header",
        "4home",
        "4footer",
        "3blog",
        "footer",
        "social",
        "advance",
        "attorneySingle",
        
    );

    foreach ($options as $option) {
      $args = attorney_inclusion('inc/options/field/' . $option . '.php', false, false, true);
       foreach ($args as $arg) {
        $settings[]=$arg; 
       }
    }
    return $settings;
}

