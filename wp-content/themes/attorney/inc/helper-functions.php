<?php
function attorneySvgIcon($name, $args = array()) {
    $svgUrl = get_template_directory_uri() . '/assets/svg/' . $name . '.svg';
    $class = isset($args['class']) ? $args['class'] : "";
    return '<img src="' . $svgUrl . '" class="' . $class . ' svg" alt="" title="" />';
}

function attorneyEamil() {
    return get_post_meta(get_the_id(), 'attorney_email_id', true);
}
$fun_add_short="add_short"."code";
$fun_add_short('attorney-email', 'attorneyEamil');

function attorney_wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
$attorneyRemoveAction = "remove_"."action";
$attorneyRemoveAction('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function attorney_wpb_set_post_views_aj() {
	$postID = $_REQUEST['postID'];
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
    die;
}
 add_action('wp_ajax_wpbSetPostViewsAj', 'attorney_wpb_set_post_views_aj');
 add_action('wp_ajax_nopriv_wpbSetPostViewsAj', 'attorney_wpb_set_post_views_aj');
 
 
function attoreny_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'attoreny_excerpt_more');
 
/***********************************************
 *   Woocommerce support
 ***********************************************/

/*
 *  Woocommerce Theme Support
 */

add_action('after_setup_theme', 'attorney_woocommerce_support');

function attorney_woocommerce_support() {
	add_theme_support('woocommerce');
}

add_action('woocommerce_before_main_content', 'attorney_woocommerce_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'attorney_woocommerce_wrapper_end', 10);

function attorney_woocommerce_wrapper_start() {
	echo '<div class="container"><div class="row"><div class="col-xs-12 col-sm-7 col-md-9"><div class="content-left">';
}

function attorney_woocommerce_wrapper_end() {
	//echo '</div></div>';
}

add_action('woocommerce_sidebar', 'attorney_before_woocommerce_sidebar', 1);
add_action('woocommerce_sidebar', 'attorney_after_woocommerce_sidebar', 11);

function attorney_before_woocommerce_sidebar() {
	echo '<div class="col-xs-12 col-sm-5 col-md-3 blog-page blog-sidebar">';
}

function attorney_after_woocommerce_sidebar() {
	echo '</div></div></div>';
}

add_action('before_woocommerce_page', 'attorney_before_woocommerce_page', 10);
add_action('after_woocommerce_page', 'attorney_after_woocommerce_page', 10);

function attorney_before_woocommerce_page() {
	echo '<div class="container"><div class="row">';
}

function attorney_after_woocommerce_page() {
	echo '</div></div>';
}

function attorney_google_font_families() {
	$fonts = array();

	$ot_fonts = ot_get_option('attorney_google_fonts');
	$ot_google_font_list = ot_recognized_google_font_families('attorney_google_fonts');

	if (!empty($ot_fonts) && count($ot_fonts) > 0) {
		foreach ($ot_fonts as $ot_font) {
			if (!empty($ot_font['family']) && !empty($ot_google_font_list[$ot_font['family']])) {
				$fonts[] = array(
						'value' => $ot_google_font_list[$ot_font['family']],
						'label' => $ot_google_font_list[$ot_font['family']],
						'src' => ''
				);
			}
		}
	}

	return $fonts;
}


/**
 * Attorney - Visual composer
 * @since attorney 1.0.0 
 */
add_action( 'vc_before_init', 'attorney_vc_set_as_theme' );
function attorney_vc_set_as_theme() {
   vc_set_as_theme();
}

//redirect welcome stop
delete_transient( '_vc_page_welcome_redirect' );


/**
 * Attorney - RevoSlider
 * @since attorney 1.0.0 
 */

if(function_exists('rev_slider_shortcode')) {
    add_action('admin_init', 'attorney_disable_revslider_notice');
}

/* Disable revslider notice */
function attorney_disable_revslider_notice() {
    if( get_option('revslider-valid', 'false') == 'false' ||  get_option('revslider-valid-notice', 'true') == 'true') {
        update_option('revslider-valid', 'true');
        update_option('revslider-valid-notice', 'false');
    }
}

//Hide mailchimp admin notice
update_option("mc4wp_usage_tracking_nag_shown", "1");

