<?php

namespace Attorney\Widget;

/**
 * Adds Footer One Description Widget widget.
 */
class Featured_Blog_Widget extends \WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
                'featured_blog', // Base ID
                __('Featured Blog Widget', 'attorney'), // Name
                array('description' => __('A Featured Blog Widget', 'attorney'),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance) {
        echo $args['before_widget'];
        $description = $instance['description'];
        ?>
        <div class="featured-blog">
            <?php
            if (!empty($instance['title'])) {
                echo $args['before_title'] . apply_filters('search_widget_title', $instance['title']) . $args['after_title'];
            }
            ?>
        <?php if (!empty($description)): ?>
                <span class="heading-details"><?php echo $description; ?></span>
        <?php endif; ?>
        <?php
        $posts_per_page = $instance['posts_per_page'];
        if(empty($posts_per_page) || $posts_per_page < 1 ) {
            $posts_per_page = 5;
        }
        ?>
         <?php $Blogposts = new \WP_Query(array('post_type' => 'post', 'posts_per_page' => $posts_per_page)); ?>
        <?php if ($Blogposts->have_posts()): ?>
                
            <ul class="featured-blog-list">
                <?php while ($Blogposts->have_posts()) : $Blogposts->the_post(); ?>
                
                    <li class="clearfix zoom">
                        <?php $attachmentURL = wp_get_attachment_thumb_url( get_post_thumbnail_id() ); ?>
                        <?php if(!empty($attachmentURL)): ?>
                        
                        <figure>
                            <a href="<?php the_permalink(); ?>"><img title="" alt="" src="<?php echo attorney_resize($attachmentURL, '97', '95'); ?>"></a>
                        </figure>
                        <?php endif; ?>
                        
                        <div class="featured-blog-descpt">
                            <h5><a href="<?php \the_permalink(); ?>"><?php \the_title(); ?></a></h5>
                            <span><?php _e('By', 'attorney'); ?> : <span><?php echo \get_the_author(); ?></span></span>
                            <p><?php echo \wp_trim_words( \get_the_content(),7, '' ); ?></p>
                        </div>
                    </li>
                <?php endwhile; ?>
                    
            </ul>                
        <?php endif; ?>
                
        </div>

        <?php wp_reset_postdata(); ?>
        <?php
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : __('', 'attorney');
        $description = !empty($instance['description']) ? $instance['description'] : __('', 'attorney');
        $PostsPerPage = !empty($instance['posts_per_page']) ? $instance['posts_per_page'] : __('', 'attorney');

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'attorney'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description:', 'attorney'); ?></label> 
            <textarea class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>"><?php echo esc_attr($description); ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('posts_per_page'); ?>"><?php _e('Posts Per Page:', 'attorney'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('posts_per_page'); ?>" name="<?php echo $this->get_field_name('posts_per_page'); ?>" type="text" value="<?php echo esc_attr($PostsPerPage); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        $instance['description'] = (!empty($new_instance['description']) ) ? strip_tags($new_instance['description']) : '';
        $instance['posts_per_page'] = (!empty($new_instance['posts_per_page']) ) ? strip_tags($new_instance['posts_per_page']) : '';
        return $instance;
    }

}