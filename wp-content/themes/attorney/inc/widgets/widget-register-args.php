<?php
return array(
    'contact-widget' => 'Contact_Widget',
    'description-widget' => 'Description_Widget',
    'contact-form-widget' => 'Contact_Form_Widget',
    'attorney-search-widget' => 'Search_Widget',
    'featured-blog' => 'Featured_Blog_Widget',
    'tags-widget' => 'Attorney_Widget_Tag_Cloud',
    'follow-us-widget' => 'Follow_Us_Widget',
    'rate-widget' => 'Rate_Widget',
    'practice-area-listing-widget' => 'Practice_Area_Listing',
);