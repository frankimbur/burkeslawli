<?php
namespace Attorney\Widget;
/**
 * Adds Contact Widget widget.
 */
class Contact_Form_Widget extends \WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
    
	function __construct() {
		parent::__construct(
			'contact_form_widget', // Base ID
			__( 'Attorney Contact Form Widget', 'attorney' ), // Name
			array( 'description' => __( 'A Contact Form Widget', 'attorney' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
                include_once ABSPATH . 'wp-admin/includes/plugin.php' ;
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
                if ( ! empty( $instance['contact_form'] ) && (0 != $instance['contact_form'])) { 
                    if ( \is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
                    echo do_shortcode("[contact-form-7 id={$instance['contact_form']} title={$instance['title']}]");
                    } else {
                        echo '<p>';
                            _e('No form found','attorney');
                        echo '</p>';
                    }
                }
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$addressSvg = ! empty( $instance['contact_form'] ) ? $instance['contact_form'] : '';
		
                if ( \is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
                    
                ?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
                
                <?php $forms = get_posts(array('post_type' => 'wpcf7_contact_form', 'posts_per_page' => '-1')); ?>
                
                <p>
		<label for="<?php echo $this->get_field_id( 'contact_form' ); ?>"><?php _e( 'Select Contact Form:', 'attorney' ); ?></label> 
                </p>
                <p>
                <select id="<?php echo $this->get_field_id( 'contact_form' ); ?>" name="<?php echo $this->get_field_name( 'contact_form' ); ?>">
                    <option <?php selected( 0, $addressSvg); ?> value="0">Select Form</option>
                    <?php foreach ($forms as $form): ?>
                    <option  <?php selected( $form->ID, $addressSvg); ?> value="<?php echo $form->ID; ?>"><?php echo $form->post_title; ?></option>
                    <?php endforeach; ?>
                </select>
                </p>
		<?php 
              }
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['contact_form'] = ( ! empty( $new_instance['contact_form'] ) ) ? $new_instance['contact_form']: '';
		return $instance;
	}

} 