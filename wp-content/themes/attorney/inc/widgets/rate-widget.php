<?php

namespace Attorney\Widget;

/**
 * Adds Footer One Description Widget widget.
 */
class Rate_Widget extends \WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
                'rate_widget', // Base ID
                __('Attorney Rage Widget', 'attorney'), // Name
                array('description' => __('A Rate Widget', 'attorney'),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance) {
        echo $args['before_widget'];
        $description = $instance['description'];
        ?>


        <div class="follow-us animate-effect anim-section">
           <?php
            if (!empty($instance['title'])) {
                echo $args['before_title'] . apply_filters('search_widget_title', $instance['title']) . $args['after_title'];
            }
            ?>
            <?php if (!empty($description)): ?>
                <span class="heading-details"><?php echo $description; ?></span>
            <?php endif; ?>
                <?php include_once ABSPATH . 'wp-admin/includes/plugin.php' ; ?>
                <?php if(is_plugin_active('social-count-plus/social-count-plus.php')): ?>
                    <?php if(function_exists('acr_show_rating')){echo acr_show_rating();} ?>
                <?php endif; ?>
        </div>
        <?php
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : __('', 'attorney');
        $description = !empty($instance['description']) ? $instance['description'] : __('', 'attorney');
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'attorney'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description:', 'attorney'); ?></label> 
            <textarea class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>"><?php echo esc_attr($description); ?></textarea>
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        $instance['description'] = (!empty($new_instance['description']) ) ? strip_tags($new_instance['description']) : '';
        return $instance;
    }

}