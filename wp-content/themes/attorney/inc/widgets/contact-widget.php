<?php
namespace Attorney\Widget;
/**
 * Adds Contact Widget widget.
 */
class Contact_Widget extends \WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'contact_widget', // Base ID
			__( 'Attorney Contact Widget', 'attorney' ), // Name
			array( 'description' => __( 'A Contact Widget', 'attorney' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
                $addresssvg = "";//$instance['addresssvg'] ? $instance['addresssvg'] : get_stylesheet_directory_uri() . "/assets/svg/pin.svg";
                 $address = $instance['address'];
                $numbersvg = "";//$instance['numbersvg'] ? $instance['numbersvg'] : get_stylesheet_directory_uri() . "/assets/svg/phone.svg";
                $number = $instance['number'];
                $mailsvg = "";$instance['mailsvg'] ? $instance['mailsvg'] : get_stylesheet_directory_uri() . "/assets/svg/mail.svg";
                $email = $instance['email'];
           
		?>
                    
                   <address class="location clearfix">
                       <a href="javascript: void(0);" class="fontlink icon-location"> address<i class="svg-shape"><img src="<?php echo esc_url($addresssvg); ?>" alt="" class="svg" /></i></a>
                            <div class="address-info">
                                <?php echo $address; ?>
                            </div>
                    </address>
                    <a href="tel:<?php echo $number; ?>" class="contact-number clearfix">
                            <?php if('' != $numbersvg): ?>
                                <i class="icon-call svg-shape">
                                   <img src="<?php echo esc_url($numbersvg); ?>" alt="" class="svg" />
                                </i>
                            <?php endif; ?>
                        <span>Phone : <?php echo $number; ?></span>
                    </a>
                    <a href="mailto:<?php echo $email;?>" class="contact-number mail-info clearfix">
                            <?php if('' != $mailsvg): ?>
                                <i class="icon-call icon-msg svg-shape">
                                    <img src="<?php echo esc_url($mailsvg); ?>" alt="" class="svg" />
                                </i>
                            <?php endif; ?>
                        <span>Email : <?php echo $email;?></span>
                    </a>
											
                <?php
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$addressSvg = ! empty( $instance['addresssvg'] ) ? $instance['addresssvg'] : '';
		$address = ! empty( $instance['address'] ) ? $instance['address'] : '';
		$numberSvg = ! empty( $instance['numbersvg'] ) ? $instance['numbersvg'] : '';
		$number = ! empty( $instance['number'] ) ? $instance['number'] : '';
		$mailSvg = ! empty( $instance['mailsvg'] ) ? $instance['mailsvg'] : '';
		$email = ! empty( $instance['email'] ) ? $instance['email'] : '';
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
                
                <p>
		<label for="<?php echo $this->get_field_id( 'addresssvg' ); ?>"><?php _e( 'Address SVG URL:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'addresssvg' ); ?>" name="<?php echo $this->get_field_name( 'addresssvg' ); ?>" type="text" value="<?php echo esc_attr( $addressSvg ); ?>">
                </p>
                
                <p>
		<label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e( 'Address:', 'attorney' ); ?></label> 
                <textarea class="widefat" id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>"><?php echo $address; ?></textarea>
                </p>
                
                <p>
		<label for="<?php echo $this->get_field_id( 'numbersvg' ); ?>"><?php _e( 'Phone Number SVG URL:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'numbersvg' ); ?>" name="<?php echo $this->get_field_name( 'numbersvg' ); ?>" type="text" value="<?php echo esc_attr( $numberSvg ); ?>">
                </p>
                
                <p>
		<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Phone Number:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>">
		</p>
                
                <p>
		<label for="<?php echo $this->get_field_id( 'mailsvg' ); ?>"><?php _e( 'E-Mail SVG URL:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'mailsvg' ); ?>" name="<?php echo $this->get_field_name( 'mailsvg' ); ?>" type="text" value="<?php echo esc_attr( $mailSvg ); ?>">
                </p>
               
                <p>
		<label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e( 'Eamil:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" type="text" value="<?php echo esc_attr( $email ); ?>">
		</p>
                
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
            
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['addresssvg'] = ( ! empty( $new_instance['addresssvg'] ) ) ? $new_instance['addresssvg']: '';
		$instance['address'] = ( ! empty( $new_instance['address'] ) ) ? $new_instance['address']: '';
		$instance['numbersvg'] = ( ! empty( $new_instance['numbersvg'] ) ) ? $new_instance['numbersvg']: '';
		$instance['number'] = ( ! empty( $new_instance['number'] ) ) ? $new_instance['number']: '';
		$instance['mailsvg'] = ( ! empty( $new_instance['mailsvg'] ) ) ? $new_instance['mailsvg']: '';
		$instance['email'] = ( ! empty( $new_instance['email'] ) ) ? $new_instance['email']: '';
 
		return $instance;
	}

} 
