<?php
namespace Attorney\Widget\Register;
/**
 * Attorney - Register Sidebars
 * 
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc.widgets
 * @since attorney 1.0
 */

Class Register_Sidebars {
    public static function register(){
        $widgetArgs = require ATTORNEY_THEME_DIR . '/inc/widgets/sidebar-register-args.php' ;
            foreach($widgetArgs as $widget) {
                \register_sidebar($widget);
            }
        
    }
}