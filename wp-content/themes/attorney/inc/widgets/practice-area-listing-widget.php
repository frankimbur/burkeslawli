<?php
namespace Attorney\Widget;
/**
 * Adds Practice List widget.
 */
class Practice_Area_Listing extends \WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'practice_area_listing_widget', // Base ID
			__( 'Practice Area Listing', 'attorney' ), // Name
			array( 'description' => __( 'A Practice Area Listing', 'attorney' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
                $description = $instance['description'];
             	?>
                    <div class="our-services">
                            <?php if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'search_widget_title', $instance['title'] ). $args['after_title'];
                    } ?>
                        <?php if(!empty($description)): ?>
                            <span class="heading-details"><?php echo $description; ?></span>
                        <?php endif; ?>
                        <?php $practices = get_posts(array('post_type' => 'practices', 'posts_per_page' => get_option('posts_per_page'))); ?>
                        <?php if(!empty($practices)): ?> 
                        <ul class="practice-listing">
                            <?php foreach($practices as $practice): ?>
                            <li>
                                <a href="<?php echo get_permalink($practice->ID); ?>"><?php echo get_the_title($practice->ID); ?><i class="fa fa-chevron-right"></i></a> 
                            </li>
                            <?php endforeach; ?>
                         </ul>   
                        <?php endif; ?>
                    </div>
                    
                <?php
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$description = ! empty( $instance['description'] ) ? $instance['description'] : '';
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
                <p>
		<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description:', 'attorney' ); ?></label> 
                <textarea class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo esc_attr( $description ); ?></textarea>
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
		return $instance;
	}

} 