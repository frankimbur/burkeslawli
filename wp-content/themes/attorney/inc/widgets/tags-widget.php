<?php
namespace Attorney\Widget;

class Attorney_Widget_Tag_Cloud extends \WP_Widget {

	public function __construct() {
		$widget_ops = array( 'description' => __( "Attorney tag cloud of your most used tags.",'attorney') );
		parent::__construct('attorney_tag_cloud', __('Attorney Tag Cloud', "attorney"), $widget_ops);
	}

	public function widget( $args, $instance ) {
		$current_taxonomy = $this->_get_current_taxonomy($instance);
		if ( !empty($instance['title']) ) {
			$title = $instance['title'];
		} else {
			if ( 'post_tag' == $current_taxonomy ) {
				$title = __('Attorney Tags', 'attorney');
			} else {
				$tax = get_taxonomy($current_taxonomy);
				$title = $tax->labels->name;
			}
		}

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = \apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo $args['before_widget']; ?>
                
                <div class="tags animate-effect anim-section">
		<?php if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} 
                $description = $instance['description'];
                echo '<span class="heading-details">'.$description.'</span>';
                echo '<div class="tag-list clearfix">'; 
                \wp_tag_cloud( \apply_filters( 'widget_tag_cloud_args', array(
			'taxonomy' => $current_taxonomy,
                        'format' => 'list',
                        'smallest' => 10, 
                        'largest' => 16,
                        'unit' => 'px', 
		) ) ); 
		echo "</div>\n";
                ?></div><?php
		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags(stripslashes($new_instance['title']));
                $instance['description'] = strip_tags(stripslashes($new_instance['description']));
		$instance['taxonomy'] = stripslashes($new_instance['taxonomy']);
		return $instance;
	}

	public function form( $instance ) {
		$current_taxonomy = $this->_get_current_taxonomy($instance);
?>
	<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'attorney') ?></label>
	<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php if (isset ( $instance['title'])) {echo esc_attr( $instance['title'] );} ?>" /></p>
        
        <p><label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description:', 'attorney') ?></label>
	<input type="text" class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" value="<?php if (isset ( $instance['description'])) {echo esc_attr( $instance['description'] );} ?>" /></p>
        
	<p><label for="<?php echo $this->get_field_id('taxonomy'); ?>"><?php _e('Taxonomy:', 'attorney') ?></label>
	<select class="widefat" id="<?php echo $this->get_field_id('taxonomy'); ?>" name="<?php echo $this->get_field_name('taxonomy'); ?>">
	<?php foreach ( get_taxonomies() as $taxonomy ) :
				$tax = get_taxonomy($taxonomy);
				if ( !$tax->show_tagcloud || empty($tax->labels->name) )
					continue;
	?>
		<option value="<?php echo esc_attr($taxonomy) ?>" <?php selected($taxonomy, $current_taxonomy) ?>><?php echo $tax->labels->name; ?></option>
	<?php endforeach; ?>
	</select></p><?php
	}

	public function _get_current_taxonomy($instance) {
		if ( !empty($instance['taxonomy']) && taxonomy_exists($instance['taxonomy']) )
			return $instance['taxonomy'];

		return 'post_tag';
	}
}