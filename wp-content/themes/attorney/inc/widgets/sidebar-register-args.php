<?php
return array(
    'DefaultSidebar' => array(
        'name' => esc_html__('Default Sidebar', 'attorney'),
        'id' => 'attorney-default-sidebar',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ),
    'FooterOneSidebarOne' => array(
        'name' => __('Footer One Sidebar:One', 'attorney'),
        'id' => 'footer-one-sidebar-one',
        'description' => 'Footer One Sidebar One',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ),
    'FooterOneSidebarTwo' => array(
        'name' => __('Footer One Sidebar:Two', 'attorney'),
        'id' => 'footer-one-sidebar-two',
        'description' => 'Footer One Sidebar Two',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ),
    'FooterOneSidebarThree' => array(
        'name' => __('Footer One Sidebar:Three', 'attorney'),
        'id' => 'footer-one-sidebar-three',
        'description' => 'Footer One Sidebar Three',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>' 
    ),
    'FooterOneSidebarFour' => array(
        'name' => __('Footer One Sidebar:Four', 'attorney'),
        'id' => 'footer-one-sidebar-four',
        'description' => 'Footer One Sidebar four',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ),
    'FooterTwoSidebarOne' => array(
        'name' => __('Footer Two Sidebar:One', 'attorney'),
        'id' => 'footer-two-sidebar-one',
        'description' => 'Footer One Sidebar One',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ),
    'FooterTwoSidebarTwo' => array(
        'name' => __('Footer Two Sidebar:Two', 'attorney'),
        'id' => 'footer-two-sidebar-two',
        'description' => 'Footer Two Sidebar Two',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ),
    'FooterTwoSidebarThree' => array(
        'name' => __('Footer Two Sidebar:Three', 'attorney'),
        'id' => 'footer-two-sidebar-three',
        'description' => 'Footer Two Sidebar Three',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>' 
    ),
    'FooterTwoSidebarFour' => array(
        'name' => __('Footer Two Sidebar:Four', 'attorney'),
        'id' => 'footer-two-sidebar-four',
        'description' => 'Footer two Sidebar four',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ),
    'FooterThreeSidebarOne' => array(
        'name' => __('Footer Three Sidebar:One', 'attorney'),
        'id' => 'footer-three-sidebar-one',
        'description' => 'Footer Three Sidebar One',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ),
    'FooterThreeSidebarTwo' => array(
        'name' => __('Footer Three Sidebar:Two', 'attorney'),
        'id' => 'footer-three-sidebar-two',
        'description' => 'Footer Three Sidebar Two',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ),
    'FooterThreeSidebarThree' => array(
        'name' => __('Footer Three Sidebar:Three', 'attorney'),
        'id' => 'footer-three-sidebar-three',
        'description' => 'Footer Three Sidebar Three',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>' 
    ),
    'FooterThreeSidebarFour' => array(
        'name' => __('Footer Three Sidebar:Four', 'attorney'),
        'id' => 'footer-three-sidebar-four',
        'description' => 'Footer Three Sidebar four',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ),
    'FooterFourSidebarOne' => array(
        'name' => __('Footer Four Sidebar:One', 'attorney'),
        'id' => 'footer-four-sidebar-one',
        'description' => 'Four Three Sidebar One',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ),
    'FooterFourSidebarTwo' => array(
        'name' => __('Footer Four Sidebar:Two', 'attorney'),
        'id' => 'footer-four-sidebar-two',
        'description' => 'Footer Three Sidebar Two',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ),
    'FooterFourSidebarThree' => array(
        'name' => __('Footer Four Sidebar:Three', 'attorney'),
        'id' => 'footer-four-sidebar-three',
        'description' => 'Footer Four Sidebar Three',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>' 
    ),
    'FooterFourSidebarFour' => array(
        'name' => __('Footer Four Sidebar:Four', 'attorney'),
        'id' => 'footer-four-sidebar-four',
        'description' => 'Footer Four Sidebar four',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ),
    'BlogSidebar' => array(
        'name' => __('Blog Sidebar', 'attorney'),
        'id' => 'blog-sidebar',
        'description' => 'Blog Sidebar.',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ),
    'AttorneySingleSidebar' => array(
        'name' => __('Attorney Detailed Page Sidebar', 'attorney'),
        'id' => 'attorney-single-sidebar',
        'description' => 'Attorney Detailed Page Sidebar.',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    )
);