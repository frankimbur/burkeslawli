<?php
namespace Attorney\Widget\Register;

Class Register_Widgets {
    public static function register(){
        $widgetArgs = require ATTORNEY_THEME_DIR . '/inc/widgets/widget-register-args.php' ;
        foreach($widgetArgs as $key => $widget) {
            require_once ATTORNEY_THEME_DIR . '/inc/widgets/'.$key.'.php' ;
            \register_widget( "Attorney\\Widget\\".$widget );
        }
    }
}
