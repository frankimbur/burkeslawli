<?php
namespace Attorney\Widget;
/**
 * Adds Footer One Description Widget widget.
 */
class Description_Widget extends \WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'description_widget', // Base ID
			__( 'Attorney Description Widget', 'attorney' ), // Name
			array( 'description' => __( 'A Description Widget', 'attorney' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
                $svg = $instance['svg'];
                $image = $instance['image'];
                $imageLink = $instance['imagelink'];
                $imageDescription = $instance['description'];
                if(empty($imageLink)){
		    $imageLink = 'javascript: void(0);';
		    $imagetext = "Footer Logo";
                }
		?>
		    <a class="fontlink footer-logo" href="<?php echo $imageLink; ?>">
		    	<?php echo $imagetext;?>
                        <?php if(!empty($svg)): ?>
                        <img src="<?php echo $svg; ?>" alt="" title="" />
                        <?php endif; ?>
                        <?php if(!empty($image)): ?>
                        <img title="" alt="" src="<?php echo $image; ?>">
                        <?php endif; ?>
                    </a>
                    <p><?php echo $imageDescription; ?></p>
                    
                <?php
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$image = ! empty( $instance['image'] ) ? $instance['image'] : '';
		$svg = ! empty( $instance['svg'] ) ? $instance['svg'] : '';
		$imageLink = ! empty( $instance['imagelink'] ) ? $instance['imagelink'] : '';
		$description = ! empty( $instance['description'] ) ? $instance['description'] : __( 'New description', 'attorney' );
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
                <p>
		<label for="<?php echo $this->get_field_id( 'svg' ); ?>"><?php _e( 'SVG URL:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'svg' ); ?>" name="<?php echo $this->get_field_name( 'svg' ); ?>" type="text" value="<?php echo esc_url( $svg ); ?>">
		</p>
                <p>
		<label for="<?php echo $this->get_field_id( 'image' ); ?>"><?php _e( 'Image URL:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" type="text" value="<?php echo esc_attr( $image ); ?>">
		</p>
                <p>
		<label for="<?php echo $this->get_field_id( 'imagelink' ); ?>"><?php _e( 'Image Link:', 'attorney' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'imagelink' ); ?>" name="<?php echo $this->get_field_name( 'imagelink' ); ?>" type="text" value="<?php echo esc_attr( $imageLink ); ?>">
		</p>
                <p>
		<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description:', 'attorney' ); ?></label>
                <textarea class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo esc_attr( $description ); ?></textarea>
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['svg'] = ( ! empty( $new_instance['svg'] ) ) ? $new_instance['svg']: '';
		$instance['image'] = ( ! empty( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';
		$instance['imagelink'] = ( ! empty( $new_instance['imagelink'] ) ) ? strip_tags( $new_instance['imagelink'] ) : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
 
		return $instance;
	}

}