<?php
/**
 * Attorney - Admin Menu
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */

if (!class_exists('Walker_Nav_Menu_Edit')) {
    require ABSPATH . 'wp-admin/includes/nav-menu.php' ;
}
attorney_inclusion( "inc/menu/nav-walker.php" );
attorney_inclusion( "inc/menu/item-field.php" );

add_action('wp_update_nav_menu_item', array( "Attorney\Admin\Menu\Item_Field", 'update'), 10, 3);
add_filter('wp_edit_nav_menu_walker', array("Attorney\Admin\Menu\Item_Field", 'walker'), 10, 2 );

add_filter( 'manage_nav-menus_columns', array("Attorney\Admin\Menu\Item_Field", 'screen'), 99 );
add_action('admin_enqueue_scripts', array( "Attorney\Admin\Menu\Item_Field", 'scripts'));
add_action('admin_footer', array( "Attorney\Admin\Menu\Item_Field", 'jsfooter'));
