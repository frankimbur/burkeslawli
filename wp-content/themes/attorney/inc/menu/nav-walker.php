<?php
/**
 * Attorney - Admin Menu
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
namespace Attorney\Admin\Menu;

class Nav_Walker extends \Walker_Nav_Menu_Edit {
    /**
     * Start the element output.
     *
     * We're injecting our custom fields after the div.submitbox
     *
     * @see Walker_Nav_Menu::start_el()
     * @since 0.1.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   Menu item args.
     * @param int    $id     Nav menu ID.
     */
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {

        $item_output = '';
        parent::start_el($item_output, $item, $depth, $args, $id);
        $output .= preg_replace('/(?=<p[^>]+class="[^"]*field-move)/', $this->get_fields($item, $depth, $args), $item_output);
        return $output;
    }

    /**
     * Get custom fields
     *
     * @access protected
     * @since 0.1.0
     * @uses add_action() Calls 'menu_item_custom_fields' hook
     *
     * @param object $item  Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args  Menu item args.
     * @param int    $id    Nav menu ID.
     *
     * @return string Form fields
     */
    protected function get_fields($item, $depth, $args = array(), $id = 0) {
        
        ob_start();
        $_key = "attorney-icon";
        $label = __("Menu Icon", "attorney");
        $key = sprintf('menu-item-%s', $_key);
        $id = sprintf('edit-%s-%s', $key, $item->ID);
        $name = sprintf('%s[%s]', $key, $item->ID);
        $value = get_post_meta($item->ID, $key, true);
        $class = sprintf('field-%s', $_key);
        ?>
        <p  class="description description-wide menu-selectpicker <?php echo esc_attr($class) ?>">
            <?php 
                $Field='<label for="%1$s">%2$s<br />
                        <select id="%1$s" class="widefat selectpicker %1$s" name="%3$s">'
                        .$this->createOption($value)
                        .'</select>
                    </label>';             
            printf($Field, esc_attr($id), esc_html($label), esc_attr($name)); //, esc_attr($value  ?>
        </p>
        <?php
        return ob_get_clean();
    }
    
    function createOption($old=0){
        $icons=  attorney_inclusion( "inc/menu/font-awesome-icons.php" );
        $str="";
        if(is_array($icons)):
            
            foreach($icons as $key=> $val):
                $var=str_replace("&#x", "",$val);
                $str.='<option value="'.$var.'" '.selected($old, $var, false).'>'.$val.'</option>';
            endforeach;
        endif;
        return $str;
    }

}