<?php
/**
 * Attorney - Admin Menu
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
namespace Attorney\Admin\Menu;

class Item_Field {
    /**
     * Replace default menu editor walker with ours
     *
     * We don't actually replace the default walker. We're still using it and
     * only injecting some HTMLs.
     *
     * @since   0.1.0
     * @access  private
     * @wp_hook filter wp_edit_nav_menu_walker
     * @param   string $walker Walker class name
     * @return  string Walker class name
     */
    public static function walker($walker, $menu_id) {
        return 'Attorney\Admin\Menu\Nav_Walker';
    }

    /**
     * Update custom field value
     *
     * @wp_hook action wp_update_nav_menu_item
     *
     * @param int   $menu_id         Nav menu ID
     * @param int   $menu_item_db_id Menu item ID
     * @param array $menu_item_args  Menu item data
     */
    public static function update($menu_id, $menu_item_db_id, $menu_item_args) {

        $_key = "attorney-icon";
        $key = sprintf('menu-item-%s', $_key);
        // Sanitize
        if (!empty($_POST[$key][$menu_item_db_id])) {
            // Do some checks here...
            $value = $_POST[$key][$menu_item_db_id];
        } else {
            $value = null;
        }
        // Update
        if (!is_null($value)) {
            update_post_meta($menu_item_db_id, $key, $value);
        } else {
            delete_post_meta($menu_item_db_id, $key);
        }
    }

    /**
     * Add our fields to the screen options toggle
     *
     * @param array $columns Menu item columns
     * @return array
     */
    public function screen($columns) {
        $columns = array_merge($columns, array("attorney-icon" => "Menu Icon"));
        return $columns;
    }

    public static function scripts() {
        global $pagenow;

        if ($pagenow == "nav-menus.php"):
            $themeURL = get_template_directory_uri();
            wp_enqueue_style('attorney.menu.font-awesome', $themeURL . '/assets//css/font-awesome.min.css', array(), null, 'all');
            wp_enqueue_style('attorney.menu.select', $themeURL . '/inc/menu/assets/bootstrap-select.css', array(), null, 'all');
            wp_enqueue_script('attorney.menu.select', $themeURL . '/inc/menu/assets/bootstrap-select.js', array('jquery'), null, true);
            //wp_enqueue_style('attorney.menu.select', $themeURL . '/inc/menu/assets/bootstrap-select.css', array(), null, 'all');
        endif;
    }

    public static function jsfooter() {
         global $pagenow;
        if ($pagenow == "nav-menus.php"):
            ?><script type='text/javascript'>/* <![CDATA[ */ 
                jQuery(document).on("click", ".item-edit", function(){
                     jQuery('.selectpicker').selectpicker();
                });
            /* ]]> */</script><?php
        endif;
    }

}
