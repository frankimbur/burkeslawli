<?php

/**
 * Attorney - Header Layout
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */

namespace Attorney\Layout;

class Header_Layout {

    public static $id, $class, $label, $template;

    public static function body_class($classes) {
        $classes[] = self::$class;
        return $classes;
    }

    public static function header() {
        get_template_part(self::$template);
    }

    public static function nav_menu() {
        $args = array(
            'menu' => '',
            'container' => '',
            'container_class' => '',
            'container_id' => '',
            'menu_class' => '',
            'menu_id' => '',
            'before' => '',
            'after' => '',
            'items_wrap' => '<ul id="%1$s" class="nav navbar-nav">%3$s</ul>',
            'link_before' => '',
            'link_after' => '',
            'walker' => new Nav_Walker
        );
        $withLocation = $args;
        $withLocation['theme_location'] = 'header_menu';

        $withOutLoaction = $args;
        $withOutLoaction['theme_location'] = '';

        //get assign loaction
        $locations = get_theme_mod('nav_menu_locations');

        if (empty($locations)) {
            wp_nav_menu($withOutLoaction);
        } else {
            wp_nav_menu($withLocation);
        }
    }

    public static function set_option() {
        $layouts = Layout_Option::get_header();
        self::$id = $layouts['id'];
        self::$class = $layouts['body_class'];
        self::$label = $layouts['label'];
        self::$template = $layouts["template_part"];
    }
    public static function social() { 
        $args=array(
            'twitter' => array("icon"=> "fa-twitter", "link" => ot_get_option('at_twitter_page_url'), "label" => "twitter" ),
            'facebook' => array("icon"=> "fa-facebook", "link" => ot_get_option('at_facebook_page_url'), "label" => "facebook" ),
            'linkedin' => array("icon"=> "fa-linkedin-square", "link" => ot_get_option('at_linkedin_page_url'), "label" => "linkedin" ),
            'pinterest' => array("icon"=> "fa-pinterest", "link" => ot_get_option('at_pinterest_page_url'), "label" => "pinterest" ),
            'instagram' => array("icon"=> "fa-instagram", "link" => ot_get_option('at_instagram_url'), "label" => "instagram" ),
            'youtube' => array("icon"=> "fa-youtube", "link" => ot_get_option('at_youtube_url'), "label" => "youtube" ),
        );
        $socails=\apply_filters('attorney_header_social_link', $args);
        
        if(is_array($socails)):
            ?>
            <ul class="media-listing clearfix attorney-header-socials">
                <?php foreach ($socails as $socail): ?> 
                    <?php if(!empty($socail['link'])): ?>
                        <li><a class="socialtag fa <?php echo esc_attr($socail['icon']); ?>" title="<?php echo esc_attr($socail['label']); ?>" target="_blank" href="<?php echo esc_url($socail['link']); ?>"><?php echo esc_attr($socail['label']); ?></a></li>
                    <?php endif; ?>
               <?php endforeach; ?>
            </ul>
            <?php
        endif;
    }
    public static function search() {  
        ?>
            <form class="header-search-box" role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                  <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" />
            </form>
        <?php
    }

}
