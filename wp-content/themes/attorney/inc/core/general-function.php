<?php
/**
 * Attorney - General Function
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */

namespace Attorney\Layout;

class General_Function {
	
	function postCategory($post_id) {
		$postCategory = wp_get_post_terms($post_id, "category", "orderby=id&order=ASC");
		if (count($postCategory) > 0):
		//current term
	            foreach ($postCategory as $types):
	          $str='<a href="%1$s" class="posted-law">'.__("Posted in ", "attorney").'<span>%2$s</span> </a>';
	              printf($str, get_term_link($types), $types->name);
		       endforeach; 
	        endif;
	    }
	    
}
\add_action("wp_enqueue_scripts", __NAMESPACE__.'\\attorney_comment_script');
function attorney_comment_script(){
	if ( \is_singular() ) \wp_enqueue_script( "comment-reply" );
}