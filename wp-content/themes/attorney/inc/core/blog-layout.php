<?php
/**
 * Attorney - Blog Layout
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */

namespace Attorney\Layout;

class Blog_Layout {

    public static function read_more_link() {
        return '<a class="more-btn"  href="' . get_permalink() . '">' . esc_html__('Read More', 'attorney') . '</a>';
    }

    public static function format_blog_meta() {
        ?>
        <div class="blog-admin-info underline-label">
            <ul class="clearfix">
                <li class="admin">
                    <span ><?php printf(__("- By : %s", "attorney"), "<span> " . '<a href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '">' . esc_html(get_the_author()) . '</a>' . " </span>"); ?></span>
                </li>
                <li class="admin">
                    <span><a href="<?php echo get_month_link(get_the_time('Y'), get_the_time('m')); ?>"><?php printf(__("Date : %s", "attorney"), '<span>' . get_the_date("d-M-y") . '</span>'); ?></a></span>
                </li>
                <?php
                if (!is_single() && !post_password_required() && ( comments_open() || get_comments_number() )):
                    ?>
                    <li class="admin">
                        <?php comments_popup_link(esc_html__('Leave a comment', 'attorney'), esc_html__('1 comment', 'attorney'), esc_html__('% comments', 'attorney')); ?>
                    </li>
                <?php endif; ?>
                <!--li class="admin">
                    <a href="#"> Like </a>
                </li>
                <li>
                    <a href="#">Share thi post</a>
                </li-->
            </ul>
        </div>
        <?php
    }
    public static function format_blog_meta_single() { ?>
        <div class="blog-admin-info underline-label">
                <span class="admin"><?php printf(__("- By : %s", "attorney"), "<span> " . '<a href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '">' . esc_html(get_the_author()) . '</a>' . " </span>"); ?></span>
                <span><a href="<?php echo esc_url(get_month_link(get_the_time('Y'), get_the_time('m'))); ?>"><?php printf(__("Date : %s", "attorney"), '<span>' . get_the_date("d-M-y") . '</span>'); ?></a></span>
        </div>
    <?php
    }
    public static function blog_single_share_meta($post_id){ ?>
        <ul class="blog-comment">
            <?php
            $comment = wp_count_comments($post_id);
            $comments_count = $comment->approved;
            ?>
            <li>
                <a href="#"> <i class="fa fa-comments-o"></i> : <?php echo esc_html($comments_count); ?> </a>
            </li>
            <?php $ViewsCount = get_post_meta(get_the_id(), 'wpb_post_views_count', true); ?>
            <?php if(!empty($ViewsCount)): ?>
            <li class="heart-status">
                <a href="<?php the_permalink(); ?>"> <i class="fa fa-heart-o"></i> : <?php echo esc_html($ViewsCount); ?></a>
            </li>
            <?php endif; ?>
            <li class="share-box">
                <a href="#"> <i class="fa fa-share-alt"></i> : <?php do_action('totalShareCount', get_permalink()); ?> </a>
                <div class="share-buttons">
                    <span class='st__large' displayText=''></span>
                    <span class='st_facebook_large' displayText='Facebook'></span>
                    <span class='st_twitter_large' displayText='Tweet'></span>
                    <span class='st_googleplus_large' displayText='Google +'></span>
                </div>
            </li>
        </ul>
    <?php }
    
    public static function blog_banner_slider($post_id){ ?>
        <section class="banner-one" id="slider">
            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>
            <?php if(!empty($url)): ?>
            <div class="about-banner" style="background: url(<?php echo esc_url($url); ?>);">
                        <div class="container banner-text">
                                <div class="row">
                                        <div class="col-xs-12">
                                                <h1><?php the_title(); ?></h1>
                                        </div>
                                </div>
                        </div>

                </div>
            <?php endif; ?>
        </section>
    <?php }

}
