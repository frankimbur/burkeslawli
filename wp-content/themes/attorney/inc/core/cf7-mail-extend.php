<?php
/**
 * Attorney - cf7 mail extande
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */
namespace Attorney\Cf7;

class Cf7_Mail_Extend {
    public static function extendEmail(){ 
        if(!empty($_POST['_wpcf7_unit_tag'])){
            $unitTags=  \explode("-", $_POST['_wpcf7_unit_tag']);
            global $attorneyCF7pid;
            if($unitTags[2]){
                $attorneyCF7pid=  \substr($unitTags[2], 1, strlen($unitTags[2])-1);
            }
        }
    }
    public function wpcf7_dynamic_email_field($args) {
        if(!empty($args['recipient'])) {
          $to = array();
          $to[]=$args['recipient'];
          global $attorneyCF7pid;
          $email = \get_post_meta($attorneyCF7pid,'attorney_email_id', true);
          if(!empty($email)){
            $to[] = $email;
            $args['recipient']=  implode(",", $to);
            return $args;
          }
        }
        return $args; 
    }
}