<?php
/**
 * Attorney - Footer Layout
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */
namespace Attorney\Layout;

class Footer_Layout {
    public static $template;

    public static function footer() {
        get_template_part(self::$template);
    }
    public static function set_option() {
        $layouts = Layout_Option::get_footer();
        self::$template = $layouts["template_part"];
    }
}
