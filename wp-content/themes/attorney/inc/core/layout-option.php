<?php

/**
 * Attorney - Core files
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */

namespace Attorney\Layout;

class Layout_Option {
    public static function get_header() {
        $layout = ot_get_option("attorney_4header");
        $part="content/header";
        switch ($layout) {
            case "header_2":
                $args = array("id" => "header_2", "body_class" => "homepage-1", "label" => "Header 2", "template_part" => $part."/two");
                break;
            case "header_3":
                $args = array("id" => "header_3", "body_class" => "homepage-1", "label" => "Header 3", "template_part" => $part."/three");
                break;
            case "header_4":
                $args = array("id" => "header_4", "body_class" => "homepage-1", "label" => "Header 4", "template_part" => $part."/four");
                break;
            default:
                $args = array("id" => "header_1", "body_class" => "homepage-1", "label" => "Header 1", "template_part" => $part."/one");
                break;
        }
        
        return $args; 
    }
    public static function get_footer() {
    	$layout = ot_get_option("attorney_4footer");
    	$part="content/footer";
    	switch ($layout) {
    		case "footer_2":
    			$args = array("template_part" => $part."/two");
    			break;
    		case "footer_3":
    			$args = array("template_part" => $part."/three");
    			break;
    		case "footer_4":
    			$args = array("template_part" => $part."/four");
    			break;
    		default:
    			$args = array("template_part" => $part."/one");
    			break;
    	}
    
    	return $args;
    }
    public static function get_blog() {
    	$layout = ot_get_option("attorney_3blog");
    	$part="blog";
    	switch ($layout) {
    		case "blog_2":
    			$args = array(
                            "template_part" => $part."/two",
                            "id" => 'grid',
                            'class' => 'blog-two-page blog-two',
                            );
    			break;
    		case "blog_3":
    			$args = array(
                            "template_part" => $part."/three",
                            'id' => '',
                            'class' => 'blog-three',
                            );
    			break;
    		default:
    			$args = array("template_part" => $part."/one",
                            'id' => '',
                            'class' => 'blog-four',
                        );
    			break;
    	}
    
    	return $args;
    }
}
