<?php

/**
 * Attorney - Login Form
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc.core
 * @since attorney 1.0
 */

namespace Attorney\Setup;

class Login_Form {

    public static function logo() {

        $site_logo = (ot_get_option('attorney_logo')) ? ot_get_option('attorney_logo') : get_stylesheet_directory_uri() . "/assets/img/logo.png";
        $color = (ot_get_option('attorney_color_main')) ? ot_get_option('attorney_color_main') : '#ffffff';
        echo "<style type='text/css'>
       body.login div#login h1 a{ background-image: url('" . esc_url($site_logo) . "') !important; }
        body.login{ background-color: " . esc_attr($color) . "!important; }
    </style>";
    }

    public static function css() {
        wp_enqueue_style('attorney.login.form', get_template_directory_uri() . '/assets/admin/css/login-style.css', array(), null, 'all');
    }

    public static function url() {
        return site_url('/');
    }

    public static function title() {
        return get_option('blogname');
    }

}
