<?php
/**
 * Attorney - Home Layout
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */

namespace Attorney\Layout;

class Home_Layout {

/**
 * Home page layout
 */
	
	public static function set_home_page(){
		$layout = ot_get_option("attorney_4home");
		$home1_select = ot_get_option("attorney_1home_layout");
		$home2_select = ot_get_option("attorney_2home_layout");
		$home3_select = ot_get_option("attorney_3home_layout");
		$home4_select = ot_get_option("attorney_4home_layout");
		switch ($layout) {
			case "home_1":
				update_option("show_on_front", "page");
				update_option('page_on_front', $home1_select);
				break;
			case "home_2":
				update_option("show_on_front", "page");
				update_option('page_on_front', $home2_select);
				break;
			case "home_3":
				update_option("show_on_front", "page");
				update_option('page_on_front', $home3_select);
				break;
			case "home_4":
				update_option("show_on_front", "page");
				update_option('page_on_front', $home4_select);
				break;
			default :
				$front_page_id = get_option('page_on_front');
				$template = get_post_meta($front_page_id, "_wp_page_template", true);
				 
				if ($template == "attorney-home-one.php" || $template == "attorney-home-two.php" || $template == "attorney-home-three.php" || $template == "attorney-home-four.php") {
					update_option("show_on_front", "posts");
					update_option('page_on_front', "");
				}
				break;
		}
		
	}


}