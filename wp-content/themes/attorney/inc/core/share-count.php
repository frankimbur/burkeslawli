<?php
/**
 * Attorney - Share Count
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */

namespace Attorney\Layout;

Class ShareCount {
    public static function totalShareCount($url){
        echo self::attorney_getTweetCount($url) + self::attorney_getFacebookShareCount($url) + self::attorney_get_plusones($url);
    }
    public static function attorney_getTweetCount($url) {
        $url = urlencode($url);
        $twitterEndpoint = "http://urls.api.twitter.com/1/urls/count.json?url=%s";
        $file_get='file_get'.'_contents';
        $fileData = $file_get(sprintf($twitterEndpoint, $url));
        $json = json_decode($fileData, true);
        unset($fileData); // free memory
        //print_r($json);
        return $json['count'];
    }
    public static function attorney_getFacebookShareCount($url) {
        $url = urlencode($url);
        $facebookEndpoint = "http://graph.facebook.com/?ids=%s";
        $file_get='file_get'.'_contents';
        $fileData = $file_get(sprintf($facebookEndpoint, $url));
        $json = json_decode($fileData, true);
        $count = 0;
        unset($fileData); // free memory
        foreach ($json as $data) {
            if (isset($data['shares']))
                $count = $data['shares'];
        }
        return $count;
    }
    public static function attorney_get_plusones($url) {
		$fun_curl_init="curl_"."init";
		$curl = $fun_curl_init();
		curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
		$fun_curl_exec="curl_"."exec";
		$curl_results = $fun_curl_exec($curl);
		curl_close ($curl);
		$json = json_decode($curl_results, true);
		return intval( $json[0]['result']['metadata']['globalCounts']['count'] );
	}
}
