<?php

/**
 * Attorney - setup
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */

namespace Attorney\Setup;

class Theme_Setup {

    public static function after_setup() {
        //Make theme available for translation.
        load_theme_textdomain('attorney', get_template_directory() . '/languages');
        // Add RSS feed links to <head> for posts and comments.
        add_theme_support('automatic-feed-links');
        // Enable support for Title Tag.
        add_theme_support("title-tag");
        // Enable support for Post Thumbnails, and declare two sizes.
        add_theme_support('post-thumbnails');
        add_theme_support('buddypress');
        // Enable support for Post Formats.
        add_theme_support('post-formats', array('image', 'video', 'quote', 'audio', 'gallery',
            'link', 'aside', 'chat', 'status'));

        add_image_size("attorney_testimonial_user", 96, 96, true);
        add_image_size("attorney_client", 182, 38, true);
        add_image_size("attorney_h1_news", 262, 198, true);
        add_image_size("attorney_blog_two", 243, 171, true);
        add_image_size("attorney_blog_three", 387, 363, true);

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'header_menu' => esc_html__('Header Menu', "attorney"),
            'footer_menu' => esc_html__('Footer Menu', "attorney"),
            'footer_menu_one' => esc_html__('Footer Menu One', "attorney"),
        ));
        //content width
        $GLOBALS['content_width'] = apply_filters('attorney_content_width', 640);
    }

    public static function remove_after_setup() {
        //remove default gallery style
        add_filter('use_default_gallery_style', '__return_false');
        //custom header
        add_theme_support('custom-header');
        remove_theme_support('custom-header');
        //custom background
        add_theme_support('custom-background');
        remove_theme_support('custom-background');
        //editor style
        add_editor_style();
        remove_editor_styles();
        if (!isset($content_width)) {
            $content_width = 474;
        }
        if (version_compare($GLOBALS['wp_version'], '4.3', '<=')) {
            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action('wp_print_styles', 'print_emoji_styles');
        }
    }

    public static function switch_theme() {
        switch_theme(WP_DEFAULT_THEME, WP_DEFAULT_THEME);
        unset($_GET['activated']);
        add_action('admin_notices', array(&$this, 'upgrade_notice'));
    }

    /**
     * Add message for unsuccessful theme switch.
     */
    public static function upgrade_notice() {
        $message = sprintf(__('Attorney requires at least WordPress version 3.9 You are running version %s. Please upgrade and try again.', 'attorney'), $GLOBALS['wp_version']);
        printf('<div class="error"><p>%s</p></div>', $message);
    }

    /**
     * Prevent the Theme Preview from being loaded on WordPress versions prior to 3.9
     */
    public static function theme_preview() {
        if (isset($_GET['preview'])) {
            wp_die(sprintf(__('Attorney requires at least WordPress version 3.9 You are running version %s. Please upgrade and try again.', 'attorney'), $GLOBALS['wp_version']));
        }
    }

    //reder title
    public static function render_title() {
        /* ?>
          <title><?php wp_title('|', true, 'right'); echo get_bloginfo('name', 'display'); ?></title>
          <?php */
    }

    //filter wp title
    public static function wp_title($title, $sep) {
        global $paged, $page;

        if (is_feed()) {
            return $title;
        }

        // Add the site name.
        $title .= get_bloginfo('name', 'display');

        // Add the site description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && ( is_home() || is_front_page() )) {
            $title = "$title $sep $site_description";
        }

        // Add a page number if necessary.
        if (( $paged >= 2 || $page >= 2 ) && !is_404()) {
            $title = "$title $sep " . sprintf(__('Page %s', 'attorney'), max($paged, $page));
        }
        return $title;
    }

    public static function favicon() {
        $favicon = (ot_get_option('attorney_favicon')) ? ot_get_option('attorney_favicon') : get_stylesheet_directory_uri() . '/favicon.ico';

        echo '<link rel="shortcut icon" type="image/x-icon" href="' . esc_url($favicon) . '"  />';
    }

    // Uploade svg

    public static function attorneyUploadMimes($files) {
        $files["svg"] = "image/svg+xml";
        return $files;
    }

    // theme activate
    public static function theme_activate() {
        if (is_admin() && isset($_GET['activated'])) {
            wp_redirect(admin_url("themes.php?page=theemon-importer"));
            die();
        }
    }

}
