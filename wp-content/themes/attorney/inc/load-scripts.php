<?php

/* Attorney - Load scripts
 * @version 1.0.0
 */

namespace Attorney\Setup;

class Load_Scripts {

    public static function admin() {
        self::addCSS("attorney.option.ui", array(ATTORNEY_THEME_URL . '/assets/admin/css/option-tree-ui.css', array(), '', "all"));
        self::addCSS("attorney.font.awesome", array(ATTORNEY_THEME_URL . '/assets/css/font-awesome.min.css', array(), '', "all"));
        self::addJS("attorney.tooltip", array(ATTORNEY_THEME_URL . '/assets/admin/js/attorney.tooltip.js', array('jquery'), null, true));
        self::addJS("attorney.metabox", array(ATTORNEY_THEME_URL . '/assets/admin/js/attorney.metabox.js', array('jquery'), null, true));

        //wp_localize_script
    }

    public static function front() {
        //css
        self::addCSS("attorney.style", array(get_stylesheet_uri(), array(), null, "all"));
        self::addCSS("bootstrap.min", array(ATTORNEY_THEME_URL . '/assets/css/bootstrap.min.css', array(), null, "all"));
        self::addCSS("font-awesome.min", array(ATTORNEY_THEME_URL . '/assets/css/font-awesome.min.css', array(), null, "all"));
        self::addCSS("attorney.global", array(ATTORNEY_THEME_URL . '/assets/css/global.css', array(), null, "all"));
        self::addCSS("attorney.custom.style", array(ATTORNEY_THEME_URL . '/assets/css/attroney.style.css', array(), null, "all"));
        self::addCSS("attorney-visual-composer", array(ATTORNEY_THEME_URL . '/assets/css/visual-composer.css', array(), null, "all"));
        self::addCSS("attorney.responsive", array(ATTORNEY_THEME_URL . '/assets/css/responsive.css', array(), null, "all"));
        self::addCSS("attorney.support", array(ATTORNEY_THEME_URL . '/assets/css/support.css', array(), null, "all"));
        
        //js
        self::addJS("bootstrap.min", array(ATTORNEY_THEME_URL . '/assets/js/bootstrap.min.js', array('jquery'), null, true));
        self::addJS("attorney.option", array(ATTORNEY_THEME_URL . '/assets/js/attorney.option.js', array('jquery'), null, true));
        self::addJS("modernizr", array(ATTORNEY_THEME_URL . '/assets/js/modernizr.js', array('jquery'), null, true));
        self::addJS("jquery.shuffle", array(ATTORNEY_THEME_URL . '/assets/js/jquery.shuffle.js', array('jquery'), null, true));
        self::addJS("attorney.vailidation", array(ATTORNEY_THEME_URL . '/assets/js/vailidation.js', array('jquery'), null, true));
         $googleapisArgs = array('sensor' => false);
    $google_map_api_key = ot_get_option('attorney_google_map_api_key');
    if(!empty($google_map_api_key)) {
	$googleapisArgs['key'] = $google_map_api_key;
    }
    
        self::addJS("maps.google", array(add_query_arg($googleapisArgs,'//maps.googleapis.com/maps/api/js'), array('jquery'), null, true), false); //register
        self::addJS("gmap", array(ATTORNEY_THEME_URL . '/assets/js/gmap.js', array('jquery'), null, true), false); //register
        self::addJS("attorney.gmap", array(ATTORNEY_THEME_URL . '/assets/js/attorney.gmap.js', array('jquery', 'maps.google' ,'gmap'), null, true), false); //register
        
        self::addJS("owl.carousel.min", array(ATTORNEY_THEME_URL . '/assets/js/owl.carousel.min.js', array('jquery'), null, true));
        self::addJS("attorney.site", array(ATTORNEY_THEME_URL . '/assets/js/site.js', array('jquery'), null, true));
        
        $global_fonts = (ot_get_option('attorney_theme_font') != "")? ot_get_option('attorney_theme_font') : "'Exo', sans-serif";
        $attorneyColorMain = (ot_get_option('attorney_color_main') != "") ? ot_get_option('attorney_color_main') : '#ee3135';

        //wp_localize_script
        $attorneySwitcher=array(
                "themeURL" => ATTORNEY_THEME_URL, 
                "color" => $attorneyColorMain,
                'sticky_header' => \ot_get_option('attorney_stickey_header'),
                'layout' => \ot_get_option('attorney_theme_layout'), 
                'global_font' => $global_fonts, 
                'effect' => \ot_get_option('attorney_theme_effect') 
            );
        wp_localize_script('attorney.option', 'ATTORNEY_OPTION',$attorneySwitcher ); //attorney switcher
        
        $latLon=array(
                "latitude" => \get_post_meta(get_the_id(),'latitude', true), 
                'longitude' => \get_post_meta(get_the_id(),'longitude', true)
            );
        wp_localize_script('attorney.gmap', 'ATTORNEY_GMAP', $latLon);//attorney gmap  

        $site=array(
                "ajax_url" => admin_url("admin-ajax.php"), 
            );
        wp_localize_script('attorney.site', 'ATTORNEY_SITE', $site);//attorney site  
        
    }

    private static function addCSS($handler = null, $args, $enqueue = true) {

        list($url, $deps, $vers, $media) = $args;
        wp_register_style($handler, $url, $deps, $vers, $media);
        if ($enqueue): wp_enqueue_style($handler);
        endif;
    }

    private static function addJS($handler = null, $args, $enqueue = true) {

        list($url, $deps, $vers, $footer) = $args;
        wp_register_script($handler, $url, $deps, $vers, $footer);
        if ($enqueue): wp_enqueue_script($handler);
        endif;
    }

}
