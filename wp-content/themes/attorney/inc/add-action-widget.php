<?php
/**
 * Attorney - Widget Hooks
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */

attorney_inclusion( "inc/widgets/register-widgets.php" );
add_action( 'widgets_init', array("Attorney\Widget\Register\Register_Widgets", "register"));

attorney_inclusion( "inc/widgets/register-sidebars.php" );
add_action("widgets_init",  array("Attorney\Widget\Register\Register_Sidebars", "register"));
