<?php
/**
 * Attorney - Format Hooks
 *
 * @package AttorneyTheme
 * @subpackage AttorneyTheme.inc
 * @since attorney 1.0
 */

attorney_inclusion( "inc/core/theme-setup.php" );
if (version_compare($GLOBALS['wp_version'], '3.9', '<=')) {
    add_action('after_switch_theme', array("Attorney\Setup\Theme_Setup", 'switch_theme'));
    add_action('template_redirect', array("Attorney\Setup\Theme_Setup", 'theme_preview'));
}
add_action('after_setup_theme', array("Attorney\Setup\Theme_Setup", "after_setup"));
add_action('after_setup_theme', array("Attorney\Setup\Theme_Setup", "remove_after_setup"));
add_filter('upload_mimes', array("Attorney\Setup\Theme_Setup","attorneyUploadMimes"));
add_action('admin_init', array("Attorney\Setup\Theme_Setup","theme_activate"));

if (!function_exists('_wp_render_title_tag')) {
    add_action('wp_head', array("Attorney\Setup\Theme_Setup", "render_title"));
}
add_action("wp_head", array("Attorney\Setup\Theme_Setup", "favicon"));

attorney_inclusion( "inc/core/login-form.php" );

add_action('login_head', array("Attorney\Setup\Login_Form", "logo"));
add_action('login_enqueue_scripts', array("Attorney\Setup\Login_Form", "css"));
add_filter('login_headerurl', array("Attorney\Setup\Login_Form", "url"));
add_filter('login_headertitle', array("Attorney\Setup\Login_Form", "title"));

//require_once ATTORNEY_THEME_DIR . "/inc/core/load-scripts.php";
attorney_inclusion( "inc/load-scripts.php" );

add_action('admin_enqueue_scripts', array("Attorney\Setup\Load_Scripts", "admin"));
add_action('wp_enqueue_scripts', array("Attorney\Setup\Load_Scripts", "front"));

attorney_inclusion( "inc/core/cf7-mail-extend.php" );
Attorney\Cf7\Cf7_Mail_Extend:: extendEmail();
add_filter('wpcf7_mail_components', array("Attorney\Cf7\Cf7_Mail_Extend", "wpcf7_dynamic_email_field"));

add_action('wp_head', 'attorneyShairThis');
function attorneyShairThis(){
    //echo '<script type="text/javascript">var stLight.options({publisher: "f0849ee0-3b27-496a-a10e-aced996d11fa", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>';
}
