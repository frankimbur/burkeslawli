<?php
if (!function_exists('attorney_comment')) :

    /**
     * Template for comments and pingbacks.
     *
     * To override this walker in a child theme without modifying the comments template
     * simply create your own attorney_comment(), and that function will be used instead.
     *
     * Used as a callback by wp_list_comments() for displaying the comments.
     *
     * @since attorney 1.0
     */
    function attorney_comment($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        switch ($comment->comment_type) :
            case 'pingback' :
            case 'trackback' :
// Display trackbacks differently than normal comments.
                ?>
                <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
                    <p><?php _e('Pingback:', 'attorney'); ?> <?php comment_author_link(); ?> <?php edit_comment_link(__('(Edit)', 'attorney'), '<span class="edit-link">', '</span>'); ?></p>
                    <?php
                    break;
                default :
                    // Proceed with normal comments.
                    global $post;
                    ?>
                <li <?php comment_class('clearfix'); ?> id="li-comment-<?php comment_ID(); ?>">
                    <article id="comment-<?php comment_ID(); ?>" class="comment">
                        <header class="comment-meta comment-author vcard">
                            <figure class="animate-effect">
                                <?php echo get_avatar($comment, 97); ?>
                            </figure>
                        </header><!-- .comment-meta -->
                        <div class="user-comment animate-effect">
                            <h5><?php
                                printf('<cite><b class="fn">%1$s</b> %2$s</cite>', get_comment_author_link(),
                                        // If current post author is also comment author, make it known visually.
                                        ( $comment->user_id === $post->post_author ) ? '<span>' . __('', 'attorney') . '</span>' : '' );
                                ?></h5>
                            <div style="display: none">
                                <?php
                                $secs=  get_comment_date('Y-m-d g:i:s');
                                ?>
                            </div>
                            <ul class="comment-status">
                                <li class="line">
                                    <?php
                                    echo attorney_comment_time_slot($secs);
                                    //echo round(abs($to_time - $from_time) / 60, 2) . " minute ago";
                                    ?>
                                </li>
                                <li>
                                    <div class="reply">
                                        <?php comment_reply_link(array_merge($args, array('reply_text' => __('Reply', 'attorney'), 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                                    </div><!-- .reply -->

                                </li>
                            </ul>
                            <section class="comment-content comment">
                                <?php comment_text(); ?>
                                <?php if ('0' == $comment->comment_approved) : ?>
                                    <p class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'attorney'); ?></p>
                                <?php endif; ?>
                                <?php edit_comment_link(__('Edit', 'attorney'), '<p class="edit-link">', '</p>'); ?>
                            </section><!-- .comment-content -->

                        </div>
                    </article><!-- #comment-## -->
                    <?php
                    break;
            endswitch; // end comment_type check
        }

	/**
	 * Time Elapsed
	 * 6 days ago.
	 * @param type $secs
	 * @return type string
	 */
	function attorney_comment_time_slot($secs) {
		$secs = time() - strtotime($secs);
		$bit = array(
				' year' => $secs / 31556926 % 12,
				' week' => $secs / 604800 % 52,
				' day' => $secs / 86400 % 7,
				' hour' => $secs / 3600 % 24,
				' minute' => $secs / 60 % 60,
				' second' => $secs % 60
		);
	
		foreach ($bit as $k => $v) {
			if ($v > 0) {
				$ret =  $v . $k ;
				break;
			}
		}
		return $ret . ' ago.';
	}
    endif;
    
