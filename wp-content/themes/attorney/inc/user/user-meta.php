<?php

/**
 * Attorney - User meta ui 
 *
 * @package     attorney.inc.user
 * @version     v.1.0
 */
namespace Attorney\Admin\User;

class User_Meta {

    /*/public function __construct() {
     $this->action();   
    }

    public function action() { 
		//user_new_form
        add_action('show_user_profile', array(&$this, 'userProfile'));
        add_action('edit_user_profile', array(&$this, 'userProfile'));
        add_action( 'personal_options_update', array(&$this, 'userSaveProfile'));
        add_action( 'edit_user_profile_update', array(&$this, 'userSaveProfile'));
    }
    */
    public function userProfile($user){
        \attorney_inclusion( 'inc/user/meta-ui.php' );
    }
    
    public function userSaveProfile($user_id){
        if ( !isset( $_POST['attorney_author_nonce'] ) ||  !wp_verify_nonce( $_POST['attorney_author_nonce'], 'attorney_author_action' ) ) {
            \wp_die("Wrong Profile");
            return "";
        }
        
        
        if(isset($_POST['attorney_author_prifile'])):
            foreach($_POST['attorney_author_prifile'] as $key=>$val):
                if($key=="image"){
                    self::setThumb($user_id, $val);
                }
                \update_user_meta( $user_id,$key, $val );
            endforeach;    
        endif;
    }
    
    private static function setThumb($user_id, $url){
       \update_user_meta( $user_id,"image_thumb", $url );
       \update_user_meta( $user_id,"image_medium", $url );
    }
    
    public static function loadAssets(){
        \wp_enqueue_media();
        \wp_enqueue_style( 'admin-user-css', ATTORNEY_THEME_URL.'/inc/user/assets/userCss/user.css', array(), '1.0.0');
        \wp_enqueue_script( 'admin-user-js', ATTORNEY_THEME_URL.'/inc/user/assets/userJs/user.js', array(), '1.0.0', true); 
    }
}