<?php

/**
 * Attorney - User meta ui 
 *
 * @package     attorney.inc.user
 * @version     v.1.0
 */
require_once ATTORNEY_THEME_DIR . "/inc/user/user-meta.php" ;

add_action('show_user_profile', array("Attorney\Admin\User\User_Meta", 'userProfile'));
add_action('edit_user_profile', array("Attorney\Admin\User\User_Meta", 'userProfile'));
add_action('personal_options_update', array("Attorney\Admin\User\User_Meta", 'userSaveProfile'));
add_action('edit_user_profile_update', array("Attorney\Admin\User\User_Meta", 'userSaveProfile'));
add_action('admin_enqueue_scripts', array("Attorney\Admin\User\User_Meta", 'loadAssets'));
