<?php

/**
 * Attorney - Comment Extra Fields
 *
 * @package     attorney/inc
 * @version     v.1.0
 */
class ATExtraFields {

    public function __construct() {
        $this->action();
    }

    function action() {
        //form view
        add_filter('comment_form_default_fields', array(&$this, 'add_extra_comment_form_field')); // Displaying field
        //add_filter('preprocess_comment', array(&$this, 'verify_comment_meta_data')); // Verifying the data
        add_action('comment_post', array(&$this, 'save_comment_meta_data'));
        //add_filter('get_comment_author_link', array(&$this, 'attach_phone_to_author')); // Retrieving and displaying the data
        //meta box
        add_action('add_meta_boxes_comment', array(&$this, "addMetaBox"));
        add_action('edit_comment', array(&$this, 'editFields'));
    }

// Displaying field
    function add_extra_comment_form_field($default) {
        unset($default['url']);
        $default['subject'] = '<div class="row"><div class="col-xs-12 col-sm-12 form-block animate-effect">'
                . '<input type="text" placeholder="subject*" id="sub" /></div></div>';
        return $default;
    }

// Verifying the data
    function verify_comment_meta_data($commentdata) {
        if (!isset($_POST['subject']))
            wp_die(__('Error: please fill the required field (phone).', 'attorney'));
        return $commentdata;
    }

//Saving in database
    function save_comment_meta_data($comment_id) {
        if (!empty($_POST['subject'])) {
            add_comment_meta($comment_id, 'subject', $_POST['subject']);
        }
    }

//Retrieving and displaying the data
    function attach_phone_to_author($author) {
        $subject = get_comment_meta(get_comment_ID(), 'subject', true);
        if ($subject)
            $author .= " ($subject)";
        return $author;
    }

    function addMetaBox() {
        add_meta_box('title', __('Comment Meta', "attorney"), array(&$this, "showMetaBox"), 'comment', 'normal', 'high');
    }

    function showMetaBox($comment) {

        $subject = get_comment_meta($comment->comment_ID, 'subject', true);

        wp_nonce_field('nonce_subject', 'nonce_subject', false);
        ?>
        <p>
            <input type="text" placeholder="subject*" id="sub" value="<?php echo esc_attr($phone); ?>" />
        </p>
        <?php
    }

    function editFields($comment_id) {
        if (!isset($_POST['nonce_subject']) || !wp_verify_nonce($_POST['nonce_subject'], 'nonce_subject'))
            return;

        if (( isset($_POST['subject']) ) && ( $_POST['subject'] != '')) :
            $comment_image = wp_filter_nohtml_kses($_POST['subject']);
            update_comment_meta($comment_id, 'subject', $comment_image);
        else :
            delete_comment_meta($comment_id, 'subject');
        endif;
    }

}

new ATExtraFields();




