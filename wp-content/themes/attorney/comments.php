<?php
/**
 * The template for displaying Comments
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to attorney_comment() which is
 * located in the functions.php file.
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 * 
 */
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if (post_password_required())
    return;
?>

<div id="comments" class="comments-area clearfix">

    <?php // You can start editing here -- including this comment!  ?>

    <?php if (have_comments()) : ?>
        <div class="comment-blog-listing">

            <h2 class="comments-title">
                <?php
                printf(_n('Comments %2$s', 'Comments %2$s', get_comments_number(), 'attorney'), number_format_i18n(get_comments_number()), '<span class="heading-details underline-label">- ' . get_the_title() . '</span>');
                ?>
            </h2>

            <ol class="commentlist user-comment-list">
                <?php wp_list_comments(array('callback' => 'attorney_comment', 'style' => 'ol')); ?>
            </ol><!-- .commentlist -->
            <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : // are there comments to navigate through  ?>
                <nav id="comment-nav-below" class="navigation blog-details-page" role="navigation">
                    <ul class="pagination-list clearfix animate-effect anim-section animate">
                        <li>
                            <?php previous_comments_link(__('<i class="fa fa-chevron-left"></i> Older Comments', 'attorney')); ?> 
                        </li>
                        <li>
                            <?php next_comments_link(__('Newer Comments <i class="fa fa-chevron-right"></i>', 'attorney')); ?>
                        </li>
                    </ul>


                </nav>
            <?php endif; // check for comment navigation  ?>

            <?php
            /* If there are no comments and comments are closed, let's leave a note.
             * But we only want the note on posts and pages that had comments in the first place.
             */
            if (!comments_open() && get_comments_number()) :
                ?>
                <p class="nocomments"><?php _e('Comments are closed.', 'attorney'); ?></p>
            <?php endif; ?>
        </div>
    <?php endif; // have_comments()  ?>

    <!-- Comment Form  -->
    <section class="leave-comment">
        <div class="container-form">
            <!--<div class="row">-->
            <div class="reply-form comment-form-wrapper">

                <?php
                global $aria_req;
                $args = array(
                    'id_form' => 'commentform',
                    'id_submit' => 'submit-comment',
                    'title_reply' => esc_html__('Leave comment', 'attorney'),
                    'title_reply_to' => esc_html__('Leave a Reply to %s', 'attorney'),
                    'cancel_reply_link' => esc_html__('Cancel Reply', 'attorney'),
                    'label_submit' => esc_html__('SUBMIT', 'attorney'),
                    'comment_field' => '' .
                    '<div class="row"><div class="col-xs-12 col-sm-12 form-block animate-effect"><textarea id="comment" name="comment" placeholder="Comment*" rows="3" cols="1" aria-required="true"></textarea></div></div>',
                    'must_log_in' => '<p class="must-log-in">' .
                    sprintf(
                            __('You must be <a href="%s">logged in</a> to post a comment.', 'attorney'), wp_login_url(apply_filters('the_permalink', get_permalink()))
                    ) . '</p>',
                    'logged_in_as' => '<p class="logged-in-as">' .
                    sprintf(
                            __('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'attorney'), admin_url('profile.php'), $user_identity, wp_logout_url(apply_filters('the_permalink', get_permalink()))
                    ) . '</p>',
                    'comment_notes_before' => '',
                    'comment_notes_after' => '',
                    'fields' => apply_filters('comment_form_default_fields', array(
                        'author' =>
                        ' <div class="row"><div class="col-xs-12 col-sm-6 form-block animate-effect"><input id="author" name="author" placeholder="name*" type="text"  value="' . (esc_attr($commenter['comment_author'] ? $commenter['comment_author'] : '' )) . '" size="30"' . $aria_req . ' /></div>',
                        'email' =>
                        '<div class = "col-xs-12 col-sm-6 form-block animate-effect"><input id = "email" name = "email" placeholder="email*" type = "text" value = "' . esc_attr($commenter['comment_author_email'] ? $commenter['comment_author_email'] : '' ) . '" size = "30"' . $aria_req . ' /></div></div>',
                            )
                    ),
                );
                comment_form($args);
                ?>

            </div>
            <!--</div>-->
        </div>

    </section>

</div><!-- #comments .comments-area -->
