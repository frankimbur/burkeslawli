<?php
/**
 * Attorney - single
 * The template for displaying all single posts.
 * 
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>

<section id="slider" class="banner-one">
    <div style="background: url(<?php echo get_post_meta(get_the_id(), 'practices_banner_image', true); ?>);" class="about-banner">
        <div class="container banner-text">
            <div class="row">
                <div class="col-xs-12">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<!--content Section Start Here -->
<div id="content">
    <!--blog-content Section Start Here -->
    <section class="practice-detail-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) : the_post();
                            ?>
                            <h2><?php the_title(); ?></h2>
                            <span class="heading-details"><?php echo get_post_meta(get_the_id(), 'practices_sub_title', true); ?></span>
                            <div class="practice-detail-box clearfix">
                                <?php //if (has_post_thumbnail() && !post_password_required() && !is_attachment()) : ?>
                                <!--<figure class="detail-caption">
                                    <a href="<?php //the_permalink();               ?>"><?php //the_post_thumbnail("full");               ?></a>
                                </figure>-->
                                <?php //endif; ?>
                                <div class="detail-one-description animate-effect">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <!-- <div class="animate-effect">
                                    <div class="user-quote">
                                            <p>
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                                            </p>
                                            <span class="by animate-effect">By <span> Sunil Joshi </span> </span>
                                    </div>
                                    <p>
                                            etraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.  Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and oing hrough the cites .
                                    </p>
                            </div> -->
                            <?php
                            // If comments are open or we have at least one comment, load up the comment template.
                            if (comments_open() || get_comments_number()) :
                                comments_template();
                            endif;
                        endwhile;
                    else :
                        get_template_part('content/none');
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </section>
    <!--blog-content Section End Here -->
</div>
<!--content Section End Here -->
<?php
get_footer();
