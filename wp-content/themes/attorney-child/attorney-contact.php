<?php
/**
 * Template Name: Attorney Contact
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<script>
    (function($) { 
        $(function() {
          //  alert('hi');// more code using $ as alias to jQuery
          $('#map').find('img:not([alt])').attr('alt', 'Alt text');

        });
    })(jQuery);
</script>
<?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); ?>
        <?php do_action('attorney_format_blog_banner', get_the_id()); ?>
        <!--content Section Start Here -->
        <div id="content" class="contact-page"> 
            <!--contact-form-form Section Start Here -->
            <section class="contact-form-form">
                <div class="container">
                    <div class="row">

                        <div class="col-xs-12 col-sm-8 col-md-9">
                            <h2><?php echo get_post_meta(get_the_id(), 'page_title', true); ?></h2>
                            <h5><?php echo get_post_meta(get_the_id(), 'title_description', true); ?></h5>
                            <?php $formId = get_post_meta(get_the_id(), 'contact_form', true); ?>
                            <?php if (!empty($formId)): ?>
                                <?php echo do_shortcode("[contact-form-7 id={$formId} title='Contact Form two']"); ?>
                            <?php endif; ?>
                        </div>

                        <div class="col-xs-12 col-sm-4 col-md-3 contact-details">
                            <h2><?php _e('Contact Details', 'attorney') ?></h2>
                            <span class="heading-details underline-label"><?php echo get_post_meta(get_the_id(), 'contact_details', true); ?></span>

                            <address class="address">
                                <strong><?php _e('Office address', 'attorney') ?></strong>
                                <span class="about-us-paragraph"><?php echo get_post_meta(get_the_id(), 'office_address', true); ?></span>

                            </address>
                            <div class="address phone animate-effect ">
                                <h3><?php _e('phone', 'attorney') ?></h3>
                                <?php echo get_post_meta(get_the_id(), 'phone', true); ?>
                            </div>
                            <div class="address phone animate-effect ">
                                <h3><?php _e('fax', 'attorney') ?></h3>
                                <?php echo "631.249.3994"?>
                            </div>
                            <div class="address phone email animate-effect">
                                <h3><?php _e('email', 'attorney') ?></h3>
                                <a href="mailto:<?php echo get_post_meta(get_the_id(), 'email', true); ?>"><?php echo get_post_meta(get_the_id(), 'email', true); ?></a>
                            </div>
                        </div>

                    </div>

                </div>

            </section>

            <!--contact-form-form Section End Here -->
            <?php  wp_enqueue_script('attorney.gmap'); ?>
            <section class="map embed-responsive embed-responsive-16by9">
                <div id="map"></div>
            </section>

            <!-- consulation Section Start Here -->
            <?php $legal_consulation_section_background = get_post_meta(get_the_id(), 'legal_consulation_section_background', true); ?>
            <?php $legal_consulation_section_style = ($legal_consulation_section_background) ? 'style="background-image: url(' . $legal_consulation_section_background . ');"' : ''; ?>
            <section <?php echo ($legal_consulation_section_style) ? $legal_consulation_section_style : ''; ?> class="consulation">
                <div class="container">
                    <div class="row">

                        <div class="col-xs-12 col-sm-2 col-md-2 col-md-offset-1 animate-effect">
                            <span class="consult-info"><?php echo get_post_meta(get_the_id(), 'free_legal_consultation', true); ?></span>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-6 animate-effect">
                            <a href="tel:<?php echo get_post_meta(get_the_id(), 'consultation_number', true); ?>" class="contact-consult"><i class="icon-consult fa fa-mobile"></i><?php echo get_post_meta(get_the_id(), 'consultation_number', true); ?></a>
                        </div>

                        <div class="col-xs-12 col-sm-3 col-md-3 animate-effect">
                            <span class="consult-info week-time"><?php echo get_post_meta(get_the_id(), 'consultation_time', true); ?></span>
                        </div>

                    </div>
                </div>
            </section>

            <!-- consulation Section Start Here -->
        </div><!--content Section ends Here -->
    <?php endwhile; ?>
<?php endif; ?>
<?php
get_footer();
