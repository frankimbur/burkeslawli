<?php
/**
 * Template Name: Attorney About Us
 *
 * @package AttorneyTheme
 *  
 * @since attorney 1.0
 */
get_header();
?>
<?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); ?>
        <?php do_action('attorney_format_blog_banner', get_the_id()); ?>
    <?php endwhile; ?>
<?php endif; ?>
<!--content Section Start Here -->
<div id="content">
    <!--our-attorney Section Start Here -->
    <section class="our-attorney">
        <div class="container">
            <div class="row">
                <?php $aboutImage = get_post_meta(get_the_id(), 'about_us_image', true); ?>
                <?php if(!empty($aboutImage)): ?>
                <div class="col-xs-12 col-sm-5 col-md-5 our-attorney-pics">
                    <img src="<?php echo esc_url($aboutImage); ?>" alt="" title=""/>
                </div>
                <?php endif; ?>
                <div class="col-xs-12 col-sm-12 col-md-12 attorney-mob">
                    <?php $aboutPageTitle = get_post_meta(get_the_id(),'page_title', true); ?>
                    <?php if(!empty($aboutPageTitle)): ?>
                    <h2><?php echo esc_html($aboutPageTitle); ?></h2>
                    <?php endif; ?>
                    <?php $aboutTitleDescription = get_post_meta(get_the_id(),'title_description', true); ?>
                    <?php if(!empty($aboutTitleDescription)): ?>
                        <span class="heading-details"><?php echo esc_html($aboutTitleDescription); ?></span>
                    <?php endif; ?>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>

    <!--our-attorney Section End Here -->

    <!--our-principles Section Start Here -->
    <section class="our-principles">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php $principlesTitle = get_post_meta(get_the_id(),'principles_title', true); ?>
                    <?php if(!empty($principlesTitle)): ?>
                    <h2><?php echo esc_html($principlesTitle); ?></h2>
                    <?php endif; ?>
                    <?php $principlesTitleDescription = get_post_meta(get_the_id(),'principles_title_description', true); ?>
                    <?php if(!empty($principlesTitleDescription)): ?>
                    <span class="heading-details"><?php echo esc_html($principlesTitleDescription); ?></span>
                    <?php endif; ?>
                </div>

            </div>
            <div class="row">
                <?php $principles = get_post_meta(get_the_id(),'principles_list', true); ?>
                <?php $count=1; foreach ($principles as $principle): ?>
                    <div class="col-xs-12 col-sm-4 col-md-4 clearfix principles-box animate-effect">
                        <div class="index-box animate-effect">
                            <span><?php echo esc_html($count); ?></span>
                        </div>
                        <div class="principles-detail animate-effect">
                            <h3 class="underline-label"><?php echo esc_html($principle['title']); ?></h3>
                            <p><?php echo esc_html($principle['principles_description']); ?></p>
                        </div>

                    </div>
                <?php $count++; endforeach; ?>
            </div>
        </div>
    </section>
    <!--our-principles Section End  Here -->

    <!--client Section start  Here -->

    <section class="our-practice-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    <?php $practiceTitle = get_post_meta(get_the_id(),'practice_title', true); ?>
                    <?php if(!empty($practiceTitle)): ?>
                    <h2><?php echo esc_html($practiceTitle); ?></h2>
                    <?php endif; ?>
                    
                    <?php $practiceTitleDescription = get_post_meta(get_the_id(),'practice_title_description', true); ?>
                    <?php if(!empty($practiceTitleDescription)): ?>
                    <span class="heading-details underline-label"><?php echo esc_html($practiceTitleDescription); ?></span>
                    <?php endif; ?>
                    <!-- 	accordian code start  here -->
                    <?php $practice_area_list = get_post_meta(get_the_id(),'practice_area_list', true); ?>
                    
                    <div class="panel-group animate-effect" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php if(!empty($practice_area_list)): $count = 1; foreach ($practice_area_list as $practice_area): ?>
                            <div class="panel panel-default animate-effect">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h3 class="panel-title"><a class="<?php print($count != 1)? 'collapsed': ''; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo esc_attr($count); ?>" aria-expanded="<?php print($count == 1)? 'true': 'false'; ?>" aria-controls="collapseOne"><?php echo esc_html($practice_area['title']); ?><i class="fa fa-angle-up"></i> <i class="fa fa-angle-down"></i></a></h3>
                                </div>
                                <div id="collapse<?php echo esc_attr($count); ?>" class="panel-collapse collapse <?php print($count == 1)? 'in': ''; ?> clearfix" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body about-us-paragraph animate-effect">
                                        <p><?php echo esc_html($practice_area['practice_description']); ?></p>
                                    </div>
                                    <div class="panel-pics animate-effect">
                                        <i class="family-law-svg"> <img src="<?php echo esc_url($practice_area['practice_image']); ?>" alt=""  title="" class="svg"/> </i>
                                    </div>
                                </div>
                            </div>
                        <?php $count++; endforeach; 
                        endif; ?>
                    </div>

                </div>

                <div class="col-xs-12 col-md-3 our-services">
                    <?php $ourServiceTitle = get_post_meta(get_the_id(),'our_service_title', true); ?>
                    <?php if(!empty($ourServiceTitle)): ?>
                    <h2><?php echo esc_html($ourServiceTitle); ?></h2>
                    <?php endif; ?>
                    <?php $ourServiceTitleDescription = get_post_meta(get_the_id(),'our_service_title_description', true); ?>
                    <?php if(!empty($ourServiceTitleDescription)): ?>
                    <span class="heading-details underline-label"><?php echo esc_html($ourServiceTitleDescription); ?></span>
                    <?php endif; ?>
                    <?php $our_service_list = get_post_meta(get_the_id(), 'our_service_list', true); ?>
                    <?php if(!empty($our_service_list)): ?>
                    <ul class="practice-listing animate-effect">
                        <?php foreach ($our_service_list as $our_service): ?>
                        <li>
                            <a href="<?php echo esc_attr($our_service['service_page_link']); ?>"><?php echo esc_html($our_service['title']); ?> <i class="fa fa-chevron-right"></i></a>
                        </li>
                        <?php endforeach;; ?>
                    </ul>
                    <?php endif; ?>
                </div>

            </div>

        </div>

    </section>
    <!--client Section End  Here -->

</div>
<!--content Section ends Here --> 
<?php
get_footer();
