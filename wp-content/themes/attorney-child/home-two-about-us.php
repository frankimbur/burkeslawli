<?php
/**
 * Attorney -  Home Two About Us
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="about-us animate-effect">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 about-us-detail ">

                <a href="#" class="fontlink icon-civil"> civil<i class="civilean-law-svg"> <img src="<?php echo (ot_get_option('civil_litigation_image'))? ot_get_option('civil_litigation_image'): get_template_directory_uri().'/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i> </a>
                <?php $aboutTitle = get_post_meta(get_the_id(), 'about_title2', true); ?>
                <?php if(!empty($aboutTitle)): ?>
                <h2><?php echo esc_html($aboutTitle); ?></h2>
                <?php endif; ?>
                
                <?php $AboutTitleDescription = get_post_meta(get_the_id(), 'about_title_description2', true); ?>
                <?php if(!empty($AboutTitleDescription)): ?>
                <span class="about-tag"><?php echo esc_html($AboutTitleDescription); ?></span>
                <?php endif; ?>
                
                <?php $AboutSectionLink = get_post_meta(get_the_id(), 'about_section_link2', true); ?>
                <?php if(!empty($AboutSectionLink)): ?>
                <a href="<?php echo get_permalink($AboutSectionLink); ?>" class="more-btn"><?php _e('READ MORE', 'attorney'); ?></a>
                <?php endif; ?>

            </div>

            <div class="col-xs-12 col-sm-9 col-md-9 ">
                <div class="about-us-details">
                    <?php $AboutContentTitle = get_post_meta(get_the_id(), 'about_content_title2', true); ?>
                    <?php if(!empty($AboutContentTitle)): ?>
                    <strong class="about-us-heading"><?php echo esc_html($AboutContentTitle); ?></strong>
                    <?php endif; ?>
                    
                    <?php $AboutContentDescription = get_post_meta(get_the_id(), 'about_content_description2', true); ?>
                    <?php if(!empty($AboutContentDescription)): ?>
                    <p class="content-description"><?php echo esc_html($AboutContentDescription); ?></p>
                    <?php endif; ?>
                </div>
                <div class="contact-details clearfix">
                    <div class="phone-detail clearfix">
                        <?php $AboutContactNoTitle = get_post_meta(get_the_id(), 'about_contact_no_title2' ,true); ?>
                        <?php $AboutContactNo = get_post_meta(get_the_id(), 'about_contact_no2' ,true); ?>
                        <?php if(!empty($AboutContactNo)): ?>
                        <a href="javascript: void(0);" class="fa fa-mobile"> &nbsp;</a>
                        <div class="phone-detail-inner">
                            <span><?php echo esc_html($AboutContactNoTitle); ?></span>
                            <a href="callto:<?php echo esc_attr($AboutContactNo); ?>" class="phone-number"><?php echo esc_html($AboutContactNo); ?></a>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="mail-detail">
                        <?php $AboutEmailTile = get_post_meta(get_the_id(), 'about_email_title2' ,true); ?>
                        <?php $AaboutEmail = get_post_meta(get_the_id(), 'about_email2' ,true); ?>
                        <?php if(!empty($AboutContactNo)): ?>
                        <a href="javascript: void(0);" class="fa fa-envelope-o"> &nbsp;</a>
                        <div class="phone-detail-inner">
                            <span><?php echo esc_html($AboutEmailTile); ?></span>
                            <a href="mailto:<?php echo esc_attr($AaboutEmail); ?>" class="phone-number"><?php echo esc_html($AaboutEmail); ?></a>

                        </div>
                        <?php endif; ?>
                    </div>

                </div>

            </div>
        </div>
    </div>

</section>
