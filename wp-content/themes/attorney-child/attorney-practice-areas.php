<?php
/**
 * Template Name: Attorney Practice Areas
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); ?>
        <?php do_action('attorney_format_blog_banner', get_the_id()); ?>

<!--content Section Start Here -->
<div id="content">
    <!--practice-listings-container Section Start Here -->
    <section class="practice-listings-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php $PageTitle = get_post_meta(get_the_id(),'page_title', true); ?>
                    <?php if(!empty($PageTitle)): ?>
                    <h2><?php echo esc_html($PageTitle); ?></h2>
                    <?php endif; ?>
                    <?php $TitleDescription = get_post_meta(get_the_id(),'title_description', true); ?>
                    <?php if(!empty($TitleDescription)): ?>
                       <h5><?php echo esc_html($TitleDescription); ?></h5>
                    <?php endif; ?>
                </div>
            </div>
            <div class="practice-area-lists">
                <div class="row">
                <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
                <?php $practices = new WP_Query(array('post_type' => 'practices', 'posts_per_page' => get_option('posts_per_page'), 'paged' => $paged)); ?>
                <?php if ($practices->have_posts()): ?>
                    <?php while ($practices->have_posts()): $practices->the_post(); ?>
                    <div class="col-xs-12 col-sm-3 area-list-collection zoom">
                        <?php if (has_post_thumbnail()) { ?>
                                <figure><?php the_post_thumbnail(); ?> </figure>
                            <?php } ?>
                        <h3><?php the_title(); ?></h3>
                                <p><?php echo wp_trim_words( get_the_excerpt(), 22, '' ); ?></p>
                        <a class="more-btn more" href="<?php the_permalink(); ?>"><?php _e('Read more', 'attorney'); ?></a>
                    </div>
                    <?php endwhile;  
                    attorney_pagenavi($practices);
            else :
                get_template_part('content/none');
            endif;
            wp_reset_postdata(); ?>
                </div>
            </div>

        </div>

    </section>

    <!--practice-detail-container Section End Here -->

</div>
<!--content Section ends Here -->
    <?php endwhile; ?>
<?php endif; ?>
<?php
get_footer();
