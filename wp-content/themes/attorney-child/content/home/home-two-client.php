<?php
/**
 * Attorney -  Home Two client
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
?>
<section class="client animate-effect">
    <div class="container">
        <div class="row">
            <header class="col-xs-12 section-header">
                <a href="javascript:void(0);" class="fontlink icon-civil"> civil<i class="civilean-law-svg"> <img src="<?php echo (ot_get_option('civil_litigation_image'))? ot_get_option('civil_litigation_image'): get_template_directory_uri().'/assets/svg/civil-litigation.svg'; ?>" alt=""  title="" class="svg"/> </i> </a>
                <?php $ClientSectionTitle = get_post_meta(get_the_id(), 'client_section_title2', true); ?>
                <?php $ClientsSectionTitleDescription = get_post_meta(get_the_id(), 'clients_section_title_description2', true); ?>
                <?php if (!empty($ClientSectionTitle)): ?>
                    <h2><?php echo esc_html($ClientSectionTitle); ?></h2>
                <?php endif; ?>
                <?php if (!empty($ClientsSectionTitleDescription)): ?>
                    <span class="about-tag"><?php echo esc_html($ClientsSectionTitleDescription); ?></span>
                <?php endif; ?>
            </header>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?php $attorney_client = new WP_Query(array('post_type' => 'attorney_client', 'posts_per_page' => '6')); ?>
                <?php if ($attorney_client->have_posts()): ?>
                <ul class="client-listing clearfix">
                    <?php while ($attorney_client->have_posts()): $attorney_client->the_post(); ?>
                            <?php if (has_post_thumbnail()) { ?>
                                <li><?php the_post_thumbnail(); ?></li>
                    <?php } ?>
                        <?php endwhile; ?>
                </ul>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>

        </div>

    </div>

</section>	
