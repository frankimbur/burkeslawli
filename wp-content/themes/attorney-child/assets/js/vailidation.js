/*!
 *Attorney 
 *
 *Form validation 
 *
 *@package: attorney
 *@subpackage: attorney.assest.js
 *@scince: attorney 1.0.0
 **/
(function($) {
    "use strict";

    var AttorneyFormValidation={
        selAuthor: $('#commentform #author'),
        selEmail: $('#commentform #email'),
        selSubject: $('#commentform #sub'),
        selComment: $('#commentform #comment'),
        submitFlag: 1,
        
        init: function(){
            this.event();
        },
        event: function(){
            var self=this;
            $(document).on("click", "#submit-comment", function(){
                return self.formSumbit();
            } ); 
            $(document).on("blur", "#commentform #author, #commentform #sub, #commentform #comment", function(){
                return self.check($(this));
            } ); 
            $(document).on("blur", "#commentform #email", function(){
                return self.blurEmail($(this));
            } ); 
     
        },
        blurEmail: function(obj){
            this.check(obj);
            this.isEmail(obj);
        },
        formSumbit: function(){
          
            if(this.selAuthor.length> 0){
                this.check(this.selAuthor);
            }
            if(this.selSubject.length> 0){
                this.check(this.selSubject);
            }
            if(this.selEmail.length> 0){
                this.check(this.selEmail);
                this.isEmail(this.selEmail);
            }
           
            this.check(this.selComment);
            return (this.submitFlag> 1) ? false : true;
        },
        check: function(obj){
            if(this.isEmpty(obj)){
                this.addError(obj)
                this.submitFlag++;
            }
            else{
                this.removeError(obj);
            }
        },
        isEmpty: function(obj){
            return (obj.val() === "") ?  true : false;
        },
        isEmail: function(obj){
            var emailRegex = /^[a-zA-Z0-9._]+[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,4}$/;
            
            if(!emailRegex.test(obj.val())) {
                this.submitFlag++;
                this.addError(obj);
            }
            
        },
        addError: function(obj){
            obj.addClass('error');
            
        },
        removeError: function(obj){
            obj.removeClass('error');
        }    
    };

    if($("#commentform").length > 0){
        AttorneyFormValidation.init();
    }
})(jQuery);

