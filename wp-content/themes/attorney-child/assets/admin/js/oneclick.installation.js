(function($) {
    //"use strict";
    
    if($("#dialogInstallationStart").length>0){

        var AttorneyOneClickInstall={
            threadInterval: null,
            init: function(){
                this.initInstallDialogAlert();
                this.initInstallDialog();
                this.showLogDialog();
                this.events();
            },
            initInstallDialogAlert: function(){
                
                $("#dialogInstallationAlert").dialog({
                    resizable: true,
                    autoOpen:false,
                    modal: false,
                    open: function() {
                        $(this).dialog("widget").find(".ui-dialog-titlebar").css("visibility", "hidden");
                        $(this).dialog("widget").find(".ui-dialog-buttonpane").css("border", "0px");
                    },
                    buttons: {
                        OK: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });
                
            },
            
            initInstallDialog: function(){
                var $this=this;
                $("#dialogInstallationStart").dialog({
                    resizable: true,
                    autoOpen:false,
                    modal: false,
                    open: function() {
                        $(this).dialog("widget").find(".ui-dialog-titlebar").css("visibility", "hidden");
                        $(this).dialog("widget").find(".ui-dialog-buttonpane").css("border", "0px");
                    },
                    buttons: {
                        No: function() {
                            $( this ).dialog( "close" );
                        },
                        Yes: function() {
                            $( this ).dialog( "close" );
                            $this.isYes();
                        }
                    }
                });
                
            },
            isYes: function() {
                this.ajax();
            },
            ajax: function() {
                var $this = this;
                //var $flag=1;
                this.ajaxInit();
                
                $.ajax({
                    type: 'post',
                    data: {
                        action: 'attorney_oneclick_install'
                    },
                    url: attorneyOneclick.url,
                    cache: false, 
                    dataType: "text",
                    xhr: function() {
                        return $this.xhrInit();
                    },
                    complete: function() {
                        return clearInterval($this.threadInterval);
                    },
                    success: function($html) {
                        $html = $html.replace(/<<(\d+)\>>/gi, "");
                        
                        $html2='<p>All done. Have fun!</p><p>Remember to update the passwords and roles of imported users.</p>';
                        $("#dialogInstallationLog").html($html2);
                       
                        $("#installationLog").removeClass("tm-btn-hide");
                        $this.showProgress("100");
                        window.location.href=attorneyOneclick.importSucess;
                    }
                });
            },
            xhrInit: function() {
                var $this = this;
                var xhr;
                xhr = jQuery.ajaxSettings.xhr();
                $this.threadInterval = setInterval(function() {
                    var response, status;
                    if (xhr.readyState > 2) {
                        response = xhr.responseText;
                        status = $this.getStatus(response);
                        $this.showProgress(status);
                        
                        
                    }
                }, 100);
                return xhr;
            },
            
            showLogDialog: function(){
                $("#dialogInstallationLog").dialog({
                    resizable: true,
                    autoOpen:false,
                    modal: false,
                    width: 700,
                    open: function() {
                        $(this).dialog("widget").find(".ui-dialog-titlebar").css("visibility", "hidden");
                        $(this).dialog("widget").find(".ui-dialog-buttonpane").css("border", "0px");
                    },
                    buttons: {
                        Close: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });
               
            },
            showProgress: function($point) {
                // console.log($point);
                $(".attorney-progressbar-value").css("width", $point+"%");
                $(".attorney-progress-label").html($point+"%");
                
                
                $(".tm-s-"+$point).removeClass("tm-hide");
                if($(".tm-e-"+$point).length > 0){
                    $(".tm-e-"+$point).find("i.icon").addClass('ot-icon-check').removeClass('ot-icon-spinner');
                    
                    if($point == "100"){
                        $(".tm-finish").find("i.icon").addClass('ot-icon-check').removeClass('ot-icon-spinner');
                    }
                }
                
            },
            getStatus: function($html) {
                var $matches, $status;
                $matches = $html.match(/<<(\d+)\>>/gi);
                $status = $matches[$matches.length - 1];
                $status = $status.replace('<<', '').replace('>>', '');
                return $status;
            },
            //<i class="fa fa-check text-success"></i>
            
            ajaxInit: function() {
                $(document).ajaxStart(function() {
                    $("#installationStart").addClass("tm-btn-hide");
                    $("#attorney-progressbar").removeClass("tm-btn-hide");
                });
            },

            events: function() {
                var $this = this;
                $("#installationStart").on("click", function() {
                    if($("#setting-error-tgmpa").length> 0){
                       $("#dialogInstallationAlert").dialog("open");
                    }
                    else{
                        $("#dialogInstallationStart").dialog("open");
                    }
                });
                $("#installationLog").on("click", function() {
                    $("#dialogInstallationLog").dialog("open");
                });
                
                
            }        
        };
        
        AttorneyOneClickInstall.init();
    }
})(jQuery);
