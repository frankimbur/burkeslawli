<?php
/**
 * Template Name: Attorney Blog
 *
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<!--content Section Start Here -->
<?php do_action('attorney_format_blog_banner', get_the_id()); ?>
<div id="content">
    <!--blog-content Section Start Here -->
    <section class="blog-content">
        <div class="container">
            <div class="row">
                <?php $template_part = Attorney\Layout\Layout_Option::get_blog(); ?>
                <div class="col-xs-12 col-sm-12 col-md-12 <?php echo esc_attr($template_part['class']); ?>">
                    <div id="<?php echo esc_attr($template_part['id']); ?>">
                    <?php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $Blogposts = new WP_Query(array('post_type' => 'post', 'posts_per_page' => get_option('posts_per_page'), 'paged' => $paged));
                    if ($Blogposts->have_posts()) :
                        while ($Blogposts->have_posts()) : $Blogposts->the_post();
                            //$postFormat = (get_post_format()) ? get_post_format() : "standard";
                            $postFormat = "standard";
                            get_template_part('content/format/' . $postFormat);
                        endwhile;
                    else :
                        get_template_part('content/none');
                    endif;
                    ?>
                    </div>
                    <?php attorney_pagenavi($Blogposts); ?>
                </div>
            </div>
        </div>
    </section>
    <!--blog-content Section End Here -->
</div>
<!--content Section End Here -->
<?php
get_footer();
