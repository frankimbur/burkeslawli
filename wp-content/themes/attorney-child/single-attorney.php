<?php
/**
 * Attorney - single
 * The template for displaying all single posts.
 * 
 * @package AttorneyTheme
 * @since attorney 1.0
 */
get_header();
?>
<!--content Section Start Here -->
<?php if (have_posts()): ?>
<?php while (have_posts()): the_post(); ?>
<div id="content">
    <!--attorney-detail-container Section Start Here -->
    <section class="attorney-detail-container aprilfool">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
                   <h2><?php the_title(); ?></h2>
                    
                    <?php $PageSubHeading = ot_get_option('attorney_single_sub_heading'); ?>
                    <?php if(!empty($PageSubHeading)): ?>
                    <span class="heading-details"><?php echo esc_html($PageSubHeading); ?></span>
                    <?php endif; ?>
                    <div class="detail-info-block">
                        <div class="contact-education ">
                            <div class="clearfix">
                                <div class="contact-him ">
                                    <?php $attorney_single_contact_him_heading = ot_get_option('attorney_single_contact_him_heading'); ?>
                                    <h3 class="underline-label"><?php echo ($attorney_single_contact_him_heading)? $attorney_single_contact_him_heading: __('Contact her', 'attorney');  ?></h3>
                                    <?php $attorney_contact_no = get_post_meta(get_the_id(), 'attorney_contact_no', true); ?>
                                    <?php if(!empty($attorney_contact_no)): ?>
                                    <span class="call"><?php _e('Call :', 'attorney'); ?> <a href="tel:<?php echo $attorney_contact_no; ?>"><?php echo $attorney_contact_no; ?></a> </span>
                                    <?php endif; ?>
                                    <?php $attorney_email_id = get_post_meta(get_the_id(),'attorney_email_id',true); ?>
                                    <?php if(!empty($attorney_email_id)): ?>
                                    <span class="call mail"><?php _e('Email :', 'attorney'); ?> <a href="mailto:<?php echo $attorney_email_id; ?>"><?php echo $attorney_email_id; ?></a> </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="attorney-detail-para animate-effect">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 our-services">
                    <?php if(is_active_sidebar('attorney-single-sidebar')): ?>
                        <?php dynamic_sidebar('attorney-single-sidebar'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <!--attorney-detail-container Section End Here -->

    <!-- contact-him Section Start Here -->
   
    <section class="contact-form">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php $attorney_single_contact_him_title = ot_get_option('attorney_single_contact_him_heading'); ?>
                    <h2><?php echo ($attorney_single_contact_him_title)? $attorney_single_contact_him_title: __('Contact her', 'attorney');  ?></h2>
                    
                    <?php $attorney_single_contact_him_sub_heading = ot_get_option('attorney_single_contact_him_sub_heading'); ?>
                    <?php if(!empty($attorney_single_contact_him_sub_heading)): ?>
                    <span class="heading-details underline-label"><?php echo esc_html($attorney_single_contact_him_sub_heading); ?> </span>
                    <?php endif; ?>
                </div>
                 <?php echo do_shortcode(get_post_meta(get_the_id(), 'attorney_contact_form', true)); ?>
            </div>
        </div>
    </section>
    <!-- contact-him Section End  Here -->
    <!-- consulation Section Start Here  -->
    <?php $legal_consulation_background = ot_get_option('attorney_single_legal_consulation_background'); ?>
    <?php $legal_consulation_style = ($legal_consulation_background)? 'style="background-image: url('.$legal_consulation_background.');"':''; ?>
    <section <?php echo ($legal_consulation_style)? $legal_consulation_style: ''; ?> class="consulation">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-2 col-md-2 col-md-offset-1 animate-effect">
                    <span class="consult-info"><?php echo get_post_meta(get_the_id(), 'free_legal_consulation_title', true); ?></span>

                </div>
                <div class="col-xs-12 col-md-6 col-sm-6 animate-effect">
                    <a href="tel:<?php echo get_post_meta(get_the_id(), 'attorney_contact_no', true); ?>" class="contact-consult"><i class="icon-consult fa fa-mobile"></i><?php echo get_post_meta(get_the_id(), 'attorney_contact_no', true); ?></a>

                </div>

                <div class="col-xs-12 col-sm-3 col-md-3 animate-effect">
                    <span class="consult-info week-time">
                        <?php echo get_post_meta(get_the_id(), 'free_legal_consulation_description', true); ?>
                    </span>
                </div>

            </div>
        </div>
    </section>
    <!-- consulation Section Start Here -->
</div>
<?php endwhile; 
    else :
    get_template_part('content/none');
endif; ?>
<!--content Section End Here -->
<?php
get_footer();
